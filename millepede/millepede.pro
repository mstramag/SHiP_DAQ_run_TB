TEMPLATE = lib
TARGET = millepede
QT -= core gui
#CONFIG += debug

F90_SOURCES += millepede.f
HEADERS += millepede.h

DEPENDPATH +=
INCLUDEPATH +=
macx {
LIBS += -L/opt/local/lib -lf95
} else {
LIBS += -lgfortran
}

F90 = gfortran
F90_FLAGS = -fPIC

f90.output = ${QMAKE_FILE_BASE}.o
f90.commands = $$F90 $$F90_FLAGS -c ${QMAKE_FILE_NAME} -o ${QMAKE_FILE_OUT}
f90.input = F90_SOURCES
QMAKE_EXTRA_COMPILERS += f90
