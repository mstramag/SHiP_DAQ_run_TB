 # Export work and data directory
export ANALYSIS_PATH=$PWD/

if [ -z "$1" ] ; then
    # Run in SERVER
    echo "Data path is set in rawdata"
    #export DATAPATH=${HOME}/DESY_testbeam/rawdata/Calibration/
    export DATAPATH=${HOME}/DESY_testbeam/rawdata/DESY_2019_tb_data/
elif [ $1 == "local" ]; then
    # Run locally 
    echo "Data path is set in the current data directory"
    export DATAPATH=${PWD}/data/
fi

echo "Set DATAPATH="${DATAPATH}
export SHIFTPATH=${PWD}/AlignedShift/
export OUTPUTPATH=${DATAPATH}
export DEADPATH=/srv/nfs/rootfs/root/fpga_app/config_current/
