# Configuration file for testbeam data analysis
# Intentionally, this text file is to be read with Plotter->Init("Configuration.txt")
# Date 13 Nov 2018 - Init - by S. Ek-In - If you have doubts, please contact me.
#      20 Nov 2018 - The program works as it should - wait for validation 
#      17 Jan 2019 - ReConfiguration -> Read only number
# Features 
# 1.) Board Configuration  
#       - NumberBoard = amount of boards used in the dataset - this number should correspond to BoardConfiguration defined on the following line
#       - NumberLayer = amount of Layer used in the detector - should correspond to dimension of zPositionOffset 
# 2.) Information 
#       - (Trig - Hit) time/ window
#       - delays offset
# 3.) Constant 
#       - BeamSpot Coordinate 
#       - Gap_SiPM
# Convention : - read only lines without # 
#              - Comment should begin with #
#              - All space will be deleted with >> std::ws  // does not work
#              - if n_value > 1, a comma is expected to separate the value
#Line 20 #######################Limited comments to 20 Lines#######################################
# Configuration ( LayerType: 0 = X, 1 = Y, 2 = U, 3 = V => Applied only in ROOT files and the first table)
# IP ==999 ; not existed
# BoardID, IP  , BoardTimeOffset, IsEnable, LayerType , LayerNumber , Layer    
NumberBoard = 12
NumberLayer = 12
    01   ,  01  ,  -172        ,    1     ,   1   ,     1   ,   1  
    02   ,  02 	,  -212        ,    1     ,   0   ,     1   ,   2  
    03   ,  03  ,  -204        ,    1     ,   1   ,     2   ,   3  
    04   ,  04  ,  -243        ,    1     ,   0   ,     2   ,   4  
    05   ,  05 	,  -168        ,    1     ,   1   ,     3   ,   5  
    06   ,  06  ,  -207        ,    1     ,   0   ,     3   ,   6  
    07   ,  07 	,  -198        ,    1     ,   1   ,     4   ,   7  
    08   ,  08 	,  -239        ,    1     ,   0   ,     4   ,   8  
    09   ,  09 	,  -167        ,    1     ,   1   ,     5   ,   9  
    10   ,  10 	,  -207        ,    1     ,   0   ,     5   ,  10  
    11   ,  11 	,  -199        ,    1     ,   1   ,     6   ,  11  
    12   ,  12 	,  -238        ,    1     ,   0   ,     6   ,  12  
# Beamspot position
# LayerType, LayerNumber, beam_low, beam_high
    0  ,  1     ,  0       ,  0 
    0  ,  2     ,  0       ,  0
    0  ,  3     ,  0       ,  0
    0  ,  4     ,  0       ,  0
    0  ,  5     ,  0       ,  0
    0  ,  6     ,  0       ,  0
    1  ,  1     ,  0       ,  0
    1  ,  2     ,  0       ,  0 
    1  ,  3     ,  0       ,  0
    1  ,  4     ,  0       ,  0
    1  ,  5     ,  0       ,  0
    1  ,  6     ,  0       ,  0
# should correspond to No. layers -> not yet implemented
# find lib for reading and write Configuration file -> in [mm]
zPositionOffset = 0., 10. , 40., 50., 155., 165., 195, 205, 310., 320., 350., 360. 
#zPositionOffset = 0., 10., 40., 50., 120., 130., 160., 170., 240., 250., 280., 290. 
# End of Configuration - begin Constants
# Constants should have a format -> name = value **Please don't ignore the spaces. They are for visibility... [mm or ns]
MasterBoardID = 01
gap_die = 0.220
gap_SiPM = 0.380
ch_width = 0.250
fibre_thickness = 0.250
z_offset = 0.
FPGABoard_period = 7.353 
T_Course = 1.84 
T_Fine = 57.5 
fine_bits = 32  
radiants = 2.5  
overlap = 72 
enabletiming = 0 
time_window = 9 
