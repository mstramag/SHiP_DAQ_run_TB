/* Track Program
 * Class for track reconstruction 
 * Optimise from SHiP testbeam software Oct2019
 * by S. Ek-In - surapat.ek-in@epfl.ch
 */

#ifndef Track_hpp
#define Track_hpp

#include "TF1.h"

class Track 
{
    private: 
        
    public:

        // =-=-=-=-= Global variables =-=-=-=-=
        std::vector<double> Daughter; // Hits to build track
        std::vector<double> Daughter_zpos;
        std::vector<double> Daughter_Layer; // Layer
        std::vector<double> OtherHits;
        std::vector<double> OtherHits_zpos;
        std::vector<double> OtherHits_Layer; // 
        double ORIVTX;
        double ENDVTX;
        double Slope; // ChPosition = Slope*(zpos) + Offset
        double SlopeError;
        double Offset;
        double OffsetError; 
        TF1* LinearFunc; 

        
        /////// Function 
        Track();
        ~Track();
        void InitTrack(); 
        TF1 * FitLinearTrack(double *data_x, double *data_y, double *error_x=0, double *error_y=0, int data_size = 0, bool silence = true);

        void FitLinear();

        void AddHit(double hit, double hit_zpos, int Layer = 0) { OtherHits.push_back(hit); OtherHits_zpos.push_back(hit_zpos);   OtherHits_Layer.push_back(Layer); }
        void AddDaughter(double hit, double hit_zpos, int Layer = 0) { Daughter.push_back(hit); Daughter_zpos.push_back(hit_zpos); Daughter_Layer.push_back(Layer); }

        // SetGet
        double GetSlope() {return Slope;}
        double GetOffset() {return Offset;}
        double GetORIVTX();
        double GetENDVTX();
        std::vector<double> GetDaughter() {return Daughter;}
        std::vector<double> GetDaughter_zpos() {return Daughter_zpos;}
        std::vector<double> GetOtherHits() {return OtherHits;}
        std::vector<double> GetOtherHits_zpos() {return OtherHits_zpos;}
        double GetYPoint(double x);
        bool GoodQuality(); 
        double Quality(); 


        bool CheckHitsOutofRadius(double radius); 
        // 

        // Alignment
        unsigned int m_nGlobal;
        unsigned int m_nLocal;
        float* m_globalDerivatives;
        float* m_localDerivatives;

};


#endif /* Track_hpp */
