/* Decoder Program
 * Class for decoding STiC output
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 */

#ifndef Decoder_hpp
#define Decoder_hpp

class Decoder 
{
    private: 
        uint64_t hex_line;
        
    public:

        // =-=-=-=-= Global variables =-=-=-=-=

        // =-=-=-=-= Function =-=-=-=-=

        //// Init -> Input must be uint64_t
        Decoder(uint64_t input=0x000000000);
        void set_Input(uint64_t input);
        uint64_t get_Line();
        
        //// Check HexLine
        bool is_Amp_Saturated();
        bool is_Trigger();
        bool is_Header();
        string TriggerCodeLast;  
        string TriggerCodeFirst;

        //// Conversion
        // =-= Trigger
        uint64_t Trigger_Counter();
        uint64_t Trigger_Time();

        // =-= Configuration
        uint32_t Board_IP();
        uint32_t STiC_ID();
        uint32_t Ch_ID();

        // =-= Hit
        uint64_t Amp(); // Time over Threshold (TOT)
        uint64_t Hit_Time();
        uint64_t Fine_Time();

        // =-= Real Time in ns
        double HitRealTime(uint64_t hit_time, uint64_t fine_time, double board_frequency, double fine_bits); 
        double TriggerRealTime(uint64_t trigger_time , double board_frequency); 

};


#endif /* Decoder_hpp */
