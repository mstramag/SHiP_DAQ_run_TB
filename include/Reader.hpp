/* Reader Program
 * Class for decoding STiC output
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 */

#ifndef Reader_hpp
#define Reader_hpp

#include <unordered_map>
#include "../src/Event.cpp"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TTreeReader.h"

class Reader 
{
    private: 
    public:

        // =-=-=-=-= Global variables =-=-=-=-=
        // Detector Configuration
        // Manual Configuration - Changeable - use map STL - python-dict equivalent
        std::unordered_map<int, std::array<int, 6> > BoardID_config; // Call BoardID[PhysicalID][CollumnInConfig.txt]
        std::unordered_map<int, int>                 IP_GetID; // Call IP_GetID[IP] = PhysicalID -> Used when read Encoded data -> Trust IP decode from data  
        
    
        // Constant configuration
        int beamlowerlimit[4][20];//Don't use the second index 0 
        int beamupperlimit[4][20];//Don't use the second index 0  


        // Constant 
        std::unordered_map<int, string> MapLayerType = {  
                                                      {0, "X"},
                                                      {1, "Y"},
                                                      {2, "U"},
                                                      {3, "V"}
                                                        };
        
    
        std::vector<std::vector<TString>> layer_name ; 
        std::vector<std::vector<int>> layer_BoardID ;
        std::vector<std::vector<int>> layer_Position ; 
        std::vector<std::vector<float>> layer_Shift ; 
 
        //Generic vector for dead channels
        std::vector<std::vector<std::vector<bool>>> dead_channels;


        double T_Fine;
        double T_Course;
        double FPGABoard_period;
        double board_frequency; // = 1000./FPGABoard_period = 136 MHz ; Used in decoder class and reader
        double fine_bits;
    
        double time_window;
        bool enabletiming;
    
        unsigned int n_board_max;
        unsigned int n_layer_max;

        double fibre_thickness;
        int MasterBoardID;
        double gap_die;
        double gap_SiPM;
        double ch_width;

        // These variables will be used in Track reconstruction
        std::vector<double> zpos; 
        std::vector<double> display_zPosition_BinEdge; // For event display 
        std::vector<double> display_zLayerCenter; // For event display 
        double z_offset;
        double radiants;
        int overlap; //overlap between two fibres (when stereo angle) in channels
    
        // Def struct for EventROOTBuilder
        typedef struct HITDATA_STRUCT
        {
            uint64_t TriggerID;
            uint64_t TriggerCounter;
            bool collect_flag;
            std::vector<uint32_t> PlaneCode;
            std::vector<uint32_t> PlaneNumber;
            std::vector<uint32_t> Board_IP; 
            std::vector<uint32_t> Board_ID; 
            std::vector<uint32_t> STiC_ID;  
            std::vector<uint32_t> Ch_ID;    
            std::vector<uint32_t> Ch_Position; // Position on a layer
        
            std::vector<uint64_t> Amp;  
            std::vector<uint64_t> Hit_Time; 
            std::vector<uint64_t> Fine_Time;
            std::vector<uint64_t> Trig_Time;
        
            std::vector<double> Trig_RealTime; 
            std::vector<double> Hit_RealTime; 
        
        	unsigned int entry;
        
        }HitData; 

        typedef struct BUFFDATA_STRUCT {
            uint64_t TriggerID;
            uint64_t TriggerCounter;
            bool collect_flag;
            uint32_t PlaneCode;
            uint32_t PlaneNumber;
            uint32_t Board_IP; 
            uint32_t Board_ID; 
            uint32_t STiC_ID;  
            uint32_t Ch_ID;    
            uint32_t Ch_Position; // Position on a layer
        
            uint64_t Amp;  
            uint64_t Hit_Time; 
            uint64_t Fine_Time;
            uint64_t Trigger_Time;
        
            double Trig_RealTime; 
            double Hit_RealTime; 
        } BuffData ;

        // Def event reconstruction
        // Access through Reader.Events
        std::vector<Event> Events;

        // =-=-=-=-= Function =-=-=-=-=
        //// Init
        Reader();
        
        //// Checking the data
        bool is_Header(string hex_line);
        bool is_Trigger(string hex_line);
        bool is_Valid_Time(double trigger_time, double hit_time, double board_offset, double threshold);  
        bool is_Blacklisted(string hex_line);
        bool StartWith(string main_text, string pre_text); 

        //// File Reader
        void ReadConfiguration(string CfgFilename = "Configuration.txt");
        void GenLayerName();
        void ImportShift(float *shiftparam, unsigned int dim);
        void ReadShiftFromFile(TString filein);

        //// ROOT type Creator
        void CreateHitRoot(string BoardFilename, int NEvent = -1, short verbose_trigger = 0); 
        void CreateHitAllBoard(string data_dir, int NEntry = -1);
        void CreateTree(string Run_number, bool time_window_flag = 0, bool Reject_Saturated_flag = 0 );
        void CreateEventTree(string Run_number);
        void CreateEventTrigger(string Run_number);

        ////// Get Data
        uint64_t GetMaxTriggerID(string Run_number, string Board_name, bool Actual_triggerID = 1, string TreeName = "data");
        void GetHitData(HitData &hitdata_buffer, TChain* chain , unsigned int entry, unsigned int TriggerID_main, BuffData &buff_data);
        void GetTriggerData(HitData &hitdata_buffer, TChain* chain , unsigned int entry, unsigned int TriggerID_main);

        /////// Build Event for plotter 
        void LoadtheEntry(uint64_t entry, string Run_number, bool Build_cluster = 1);

        ///////Fill the dead channels list
        void SetDeadChannels();

};


#endif /* Reader_hpp */
