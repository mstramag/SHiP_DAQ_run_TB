/* Plotter Program
 * Plotting program
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 */

#ifndef Plotter_hpp
#define Plotter_hpp

#include "../src/Reader.cpp"
#include "TH2.h"
#include "TLine.h"
#include "../include/Alignment.hpp"

class Plotter 
{
    private: 
        
    public:

        
        // Load Configuration every time
        Reader reader;
    
        // Global Reader to TTreeReader
        TCanvas *c;
        TH1 *t[4][10];
        TH2 *t2[4][10];
        TLine *b0[4][10];
        TLine *b1[4][10];

        TH1 *hist_general[20];
        TH2 *hist2D_general[20];
        TH1 *multiplicity[12];
        TH2 *h2d_allboard[2];
        TH1 *angular_distribution[2];


        // Event paramaters 
        TTree* data_tree;
        double* z_BinEdge;
        double* z_Center;
        
        string run_number;
        int nentries;

        // ChOffsetfrom VCSEL - structure [BoardID.][ChNumber]
        std::vector<std::vector<double>> ChTimeOffset;  
        std::vector<std::vector<bool>> ChTimeOffsetIsInvalid;  
        void ReadChOffset(UInt_t board = 0);
        

        // =-=-=-=-= Function =-=-=-=-=
        //// Init
        Plotter();
        
        /////// Build Event for plotter 
		void SetFileIn(string Run_number, int N_entries = -1, bool time_window_flag = 0, bool Reject_Saturated_flag = 0, bool Force_create = 0, bool Simulation = 0, UInt_t ChOffsetBoard = 0, TString ChFile = "", TString shift_run = ""); 
        void LoadtheEntry(uint64_t entry, TTree* chain, bool Build_cluster = 1);
        std::vector<Event> Events;
        std::vector<int> selected_events;
        std::vector<std::vector<double>> selected_events_angCut;
		std::map<int, double> Efficiency;
		std::map<int, double> EfficiencyError;
        std::map<int, double> ResidualMean;
		std::map<int, double> ResidualSigma;

        // Buffer variables

        // Detector Configuration variables
        bool m_Simulation ;
        ULong64_t sim_TriggerID;
        Int_t Eventnum;                   //ID of the current event
        std::vector<uint64_t> *TriggerID;
        std::vector<uint64_t> *TriggerCounter;
        std::vector<uint32_t> *PlaneCode;
        std::vector<uint32_t> *PlaneNumber;
        
        std::vector<uint32_t> *Board_ID;     //physical id of the boards (1-24)
        std::vector<uint32_t> *Board_IP; 
        std::vector<uint32_t> *STiC_ID;      //identification number of the stic position inside one board (0-3)
        std::vector<uint32_t> *Ch_ID;        //channel number in the stic (0-63)
        std::vector<uint32_t> *Ch_Position; // Position on a layer
        std::vector<uint32_t> *Amp;          //time over threshold of the hit

        std::vector<uint64_t> *Hit_Time;     //coarse arrival time of the hit
        std::vector<uint32_t> *Fine_Time;    //fine arrival time of the hit
        std::vector<uint64_t> *Trig_Time;     //trigger time per very board
        
        std::vector<double> *Trig_RealTime; 
        std::vector<double> *Hit_RealTime; 


        //// Plot function
        void    PlotVariable(string Variable, TChain* chain);
        TCanvas * PlotExample(unsigned int cluster_min_size, TString lay);
        ////// layer.GetAmplitudes == TOT 
        ////// ev.Gettimestamp == TriggerTime ? 
        ////// Event == 1 TriggerID == Trigger Time
        void      Position(int ch_layer,int layer,double &x,double &y,double &z); // Get position depending on detector configuration // Not yet implemented
        void      PrintTrigTime(uint64_t board); // ev.Gettimestamp // Put to the Reader Not yet implemented   
        void      FindDeadChannels();
        void      DumpEvent(uint64_t evnum);   
        // No clustering needed
        TCanvas * PlotBeamSpot(); // 1D Plot => All Trigger ID => Draw("ChPosition") for each layer
        TCanvas * PlotEvent(uint64_t evnum); // 1D Plot => 1 TriggerID => 1D plot of position on layer weighted with ChPosition
        TCanvas * PlotInnerTimeDiffSingleBoard(UInt_t board, bool LongMinusPlotRange, bool selected_tracks_flag);
        TCanvas * PlotInnerTimeDiffAll(bool LongMinusPlotRange, bool BoardCalibration, bool ChCalibration, bool selected_tracks_flag);
        TCanvas * PlotFineTime(UInt_t board); //  2D Plot => All Trigger ID => FineTime
        TCanvas * PlotBoardsTimeDiff(UInt_t board); // 1D Plot => All Trigger ID => Trigger Time Diff between each trigger time of MasterBoard and Board UI 

        // Need Clustering algorithm => Class Cluster
        // Finish
        TCanvas * PlotInClusterTOT(unsigned int cluster_min_size = 1); // clusterLength()>0
        TCanvas * PlotClusterBeamSpot(); // clusterLength()>1 && clusterLength()<5 // Depending on the beam condition
        TCanvas * PlotClusterMultiplicity(TString calibrationRun); // Need clustering -> Hit Multiplicity in a cluster
        TCanvas * PlotClusterTOT(unsigned int cluster_min_size = 1); // clusterLength()>1 && Total TOT  =  sum_i{TOT_i}

        TCanvas * PlotTOT(TString lay);
        TCanvas * PlotTOTAll();
        TCanvas * PlotClusterSize();
        TCanvas * PlotHitEfficiency(TString lay); //  clusterLength() >1 - Need checking

        //// May define Cluster::GetMinTimeCh() 
        TCanvas * PlotChHitTime(TString lay); // 2DPlot => ChPosition vs Difference between Hits in a cluster and the first hit
        TCanvas * PlotTOTHitTime(TString lay); // 2D Plot => Diff between position of the first arrival ch. and the rest vs Difference between Hits in a cluster and the first hit


        //// Need programming
        TCanvas * PlotTrigTrigTime(UInt_t board); // Time diff between Trig_i and Trig_i+1
        TCanvas * PlotHitHitTime(TString lay);// Time diff between  
        /* clusterLength()>1
         * 0 == All Hit to hit time
         * 1 == Hit to hit time if the first channel is on the right
         * 2 == Hit to hit time if the first channel is on the left
         * 3 == Hit to hit time same stic
         * 4 == Hit to hit time same board
         * 5 == Hit to hit time diff board
         */
        
        //Residual
        TCanvas * PlotResidualSingle(TString lay, bool with_aligned = 0);
        TCanvas * PlotResidualAll(bool with_aligned);

        // Control alignment - Class alignment
        Alignment * AlignmentTracks(bool * fixed_plane, unsigned int n_fixed_plane, float * shift = 0);
        void AlignmentRun(Alignment * alignment, bool * fixed_plane, unsigned int n_fixed_plane);
        void AlignmentStart();

        // Time Functions
        TCanvas * PlotCTRCluster(TString lay, TString runNbr, double Amplitude_cut, bool offset);
        TCanvas * PlotCTRdiffLayers(TString lay1, TString lay2, TString calibrationRun, double theta_max = 0.9, bool correction_travelTime = 0, bool injection_corr = 1);
        void CalcChannelOffsetsSingleBoard(UInt_t board);
        void CalcChannelOffsetsAll();
        TCanvas * PlotInnerTimeDiffCorrectedSingleBoard(UInt_t board, bool LongMinusPlotRange, bool injection_corr, bool BoardCalibration, bool ChCalibration, bool Avg_ChOffset, bool selected_tracks_flag);
        TCanvas * PlotInnerTimeDiffCorrectedAll(bool LongMinusPlotRange, bool injection_corr, bool BoardCalibration, bool ChCalibration, bool Avg_ChOffset, bool selected_tracks_flag);
        TCanvas * PlotChannelOffsetComparison(string run1, string run2);
        TCanvas * PlotHitMinusTriggerTimeSingleBoard(UInt_t board, bool calibration, bool select_tracks);
        TCanvas * PlotHitMinusTriggerTimeAll(bool calinration, bool select_tracks);

        TCanvas * PlotSTICsCoincidenceResolution(TString lay, string mode = "exp", double limit=80000., unsigned int amp=80000,UInt_t board_position=2);

        // Display funcions
        void PlotAngularDistribution(int nbrEvents, bool DisplayEvents);
        //Efficiency
        TCanvas * PlotLASERInnerTimeDiffCorrected(UInt_t board);
        void CalcTelescopeEfficiency(TString calibrationRun);
        void CalcTelescopeEfficiencyBeamSingleBoard(TString lay, bool with_aligned);
        void CalcTelescopeEfficiencyBeamAll(bool with_aligned);

        TCanvas * DisplayEvents();
        //void DisplaySingleEventContinuous(int nbrEvents);
        TCanvas * DisplayEventsPosition(int eventnum);
        TCanvas * DisplaySingleEvent(int eventnum, int nevents, bool continous);
        void DisplaySelectedTracks(int nbrEvents, bool eventDisplay);
        void SelectTracksAngularCut(int nbrEvents, double theta_max);
        void PrintSelectedEvents();


        // Additional functions
        double Channel2AcPosition(double Channel, double max_cha_plane = 512);
        std::vector<Cluster> SelectClusters(std::vector<Cluster> clusters ,double maxTimeSpread, unsigned int minLength, unsigned int maxLength, unsigned int maxAmp);



};


#endif /* Plotter_hpp */
