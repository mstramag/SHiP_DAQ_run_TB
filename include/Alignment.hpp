/* Alignment Program
 * Class for alignment
 * Optimise from SHiP testbeam software Oct2019
 * by S. Ek-In - surapat.ek-in@epfl.ch
 */

#ifndef Alignment_hpp
#define Alignment_hpp

#include "TString.h"
#include "TF1.h"
#include "../src/Track.cpp"

class Alignment 
{
    private: 
        
    public:

        // =-=-=-=-= Global variables =-=-=-=-=

        // Alignment Millepede params
        // m_nGlobal = m_nPar
        unsigned int m_nGlobal, m_nLocal, m_nStdDev, m_nIter;
        int m_verbose;
        float m_cutValue; 

        // Set of parameters == change in each iteration  , I suppose
        float*       m_parameters; // Shift parameters for each global iteration 
        float*       m_parameterSigma;
        float*       m_angles;
        unsigned int m_nAngles; 

        float* m_globalDerivatives;
        float* m_localDerivatives;

        std::vector<Track*> m_tracks_x; // Get All tracks suitable for the alignment
        std::vector<Track*> m_tracks_y; // Get All tracks suitable for the alignment

        float* m_ShiftParams; // will be final shift (accumulative)
        
        /////// Function 
        Alignment();
        ~Alignment();

        void InitTrack(); 

        // SetGet
        void AddTracks(Track* track_x, Track* track_y) {m_tracks_x.push_back(track_x); m_tracks_y.push_back(track_y);}

        TF1 * SetFixedLayer(unsigned int *layer); // May exclude

        void InitMethod(); 
        void InitMatrix(); 

        void SetMethod(); // m_strategy->DoInit()
        void SetMatrix(); // m_matrix->DoInit()
		void SetParameterSigma(unsigned int i, float sigma);

        void SetAlignedParameter(float *setparam) {m_parameters = setparam;} //  GetParameterArray()                     {return m_parameters;}
        float * GetAlignedParameter() {return m_parameters;} //  GetParameterArray()                     {return m_parameters;}
        float * GetFinalAlignedParameter() {return m_ShiftParams;} //  GetParameterArray()                     {return m_parameters;}

        void KeepShiftParams();

        void FillAlignedMatrixFromTrack(unsigned int ind_track); // FillMatrixFromTrack

        // Write/Get to File 
        void WriteShiftToFile(TString fileshift);
        void GetShiftFromFile(TString fileshift);

        void PrintParameters();
        void PrintKeptParameters();
        void PrintParameterSigma();



};


#endif /* Alignment_hpp */
