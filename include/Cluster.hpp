/* Cluster Program
 * Class for d
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 * copy directly from Maria's code
 */
#ifndef Cluster_hpp
#define Cluster_hpp

#include <iostream>

// Header file for the classes stored in the TTree if any.

class Cluster {
    
    private:
        // m - used for the global variables
        short int m_startChannel;
        std::vector<unsigned int> m_amplitudes;
        std::vector<unsigned int> m_STiCIDs;
        std::vector<unsigned int> m_BoardIDs;
        std::vector<double> m_times;
        std::vector<uint64_t> m_coarsetimes;
        std::vector<uint32_t> m_finetimes;
    public :
    
        Cluster();
        Cluster(short int startChannel, unsigned int startAmplitude, double startTime,unsigned int SticID,unsigned int BoardID, uint64_t startCoarse, uint32_t startFine);
        ~Cluster();
        double Getposition();
        void Settimestamp(double time);
        void Settriggercounter(uint64_t trigger);
        void addChannelToTheLeft(unsigned int amplitude,double time,unsigned int SticID, unsigned int BoardID,  uint64_t Coarse, uint32_t Fine);
        void addChannelToTheRight(unsigned int amplitude,double time,unsigned int SticID, unsigned int BoardID, uint64_t Coarse, uint32_t Fine);
        void clear();
        short int startChannel() const {return m_startChannel;}
        unsigned int clusterLength() const;
        unsigned int totalAmplitude() const;
        unsigned int amplitudeAtChannel(unsigned int channel) const {return m_amplitudes.at(channel - m_startChannel);} //TODO sanity check!!!
        double timeAtChannel(unsigned int channel) const {return m_times.at(channel - m_startChannel);} //TODO sanity check!!!
        uint64_t coarseAtChannel(unsigned int channel) const {return m_coarsetimes.at(channel - m_startChannel);} 
        uint32_t fineAtChannel(unsigned int channel)   const {return m_finetimes.at(channel - m_startChannel);}   
        unsigned int GetSTiCID(unsigned int channel)const {return m_STiCIDs.at(channel - m_startChannel);}
        unsigned int GetBoardID(unsigned int channel)const {return m_BoardIDs.at(channel - m_startChannel);}
        float channelPosition() const;
        float channelPosition_weightedMean() const;
        void dump() const;
    
    
};

#endif

