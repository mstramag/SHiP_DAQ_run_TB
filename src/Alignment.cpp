#include "../include/Alignment.hpp"
#include<iostream>
#include "../millepede/millepede.h"

Alignment::Alignment() :
        m_nGlobal(0), 
		m_nLocal(0), 
		m_nStdDev(0), 
		m_nIter(0),
        m_verbose(0),
        m_cutValue(0.), 

        m_parameters(0), 
        m_parameterSigma(0), 
        m_angles(0), 
        m_nAngles(0), 

        m_globalDerivatives(0),
        m_localDerivatives(0),

        m_ShiftParams(0)
{
}

Alignment::~Alignment()
{
    return;
}

void Alignment::SetParameterSigma(unsigned int i, float sigma) {
	m_parameterSigma[i] = sigma;
}

void Alignment::InitMethod() {

    // INIT global variables
    INITGL(m_nGlobal, m_nLocal, m_nStdDev, m_verbose);
    PARGLO(m_parameters); 
    INITUN(m_nIter, m_cutValue);
}



void Alignment::InitMatrix(){

    // Step 1 BTSoftware_AlignmentMatrix::DoInit()

    m_nGlobal = 24; // 12 planes + 12 angles
    m_nLocal  = 4 ; // {x,y}*{Offset, Slope}
	m_nAngles = 12; // angular params
	m_nIter   = 2 ; // Local iteration
	m_cutValue= 100.; 
	m_nStdDev = 0;
    m_verbose = 0; // should be 1

	// Step 2 Init variables
	delete[] m_globalDerivatives;
	delete[] m_localDerivatives;
	m_globalDerivatives = new float[m_nGlobal];
	m_localDerivatives = new float[m_nLocal];

	for (unsigned int i = 0; i < m_nGlobal; i++) {
		// Setting relying on btsoftware_2017/BTSoftware_MatrixStraightLine.cpp
    	m_globalDerivatives[i] = 0.;
	}
	for (unsigned int i = 0; i < m_nLocal; i++) {
    	m_localDerivatives[i] = 0.;
    }


	// Step 3 Init shift param and angle
	delete[] m_parameters    ;
	delete[] m_parameterSigma;
    delete[] m_angles        ;
	m_parameters     = new float[m_nGlobal];
	m_parameterSigma = new float[m_nGlobal];
	m_angles         = new float[m_nAngles];

	for (unsigned int i = 0; i < m_nGlobal; i++)
		m_parameters[i] = 0.;

	// -1 means that this sigma is not used.
	for (unsigned int i = 0; i < m_nGlobal; i++)
		m_parameterSigma[i] = -1.;

	for (unsigned int i = 0; i < m_nAngles; i++)
		m_angles[i] = 0.;	

}


void Alignment::SetMethod(){

    // INIT global variables
    INITGL(m_nGlobal, m_nLocal, m_nStdDev, m_verbose);
    PARGLO(m_parameters); 

	// Set SD for each parameter
	// Shift params
	for (unsigned int i = 0; i < m_nGlobal; i++) {
	    if (m_parameterSigma[i] >= 0.) {
	    	unsigned int iPar = i+1;
	    	PARSIG(iPar, m_parameterSigma[i]);
	    }
	}

	// Angular params 
	for (unsigned int i = 0; i < m_nAngles; i++) {
	    unsigned int iPar = m_nGlobal - m_nAngles + i + 1;
	    float sigma = .0f;
	    PARSIG(iPar, sigma);
	}

    INITUN(m_nIter, m_cutValue);

}


void Alignment::FillAlignedMatrixFromTrack(unsigned int ind_track) {

	// Step 1 rearrange hit data
	// X
	Track* track_x = m_tracks_x.at(ind_track);
	std::vector<double> hit_x = track_x->GetDaughter();
	std::vector<double> otherhits_x = track_x->GetOtherHits();
	hit_x.insert(hit_x.end(), otherhits_x.begin(), otherhits_x.end()); // Conc track hit and notrack hit

	std::vector<double> hit_x_zpos = track_x->GetDaughter_zpos();
	std::vector<double> otherhits_x_zpos = track_x->GetOtherHits_zpos();
	hit_x_zpos.insert(hit_x_zpos.end(), otherhits_x_zpos.begin(), otherhits_x_zpos.end()); // Conc track hit and notrack hit

	// Y
	Track* track_y = m_tracks_y.at(ind_track);
	std::vector<double> hit_y = track_y->GetDaughter();
	std::vector<double> otherhits_y = track_y->GetOtherHits();
	hit_y.insert(hit_y.end(), otherhits_y.begin(), otherhits_y.end()); // Conc track hit and notrack hit

	std::vector<double> hit_y_zpos = track_y->GetDaughter_zpos();
	std::vector<double> otherhits_y_zpos = track_y->GetOtherHits_zpos();
	hit_y_zpos.insert(hit_y_zpos.end(), otherhits_y_zpos.begin(), otherhits_y_zpos.end()); // Conc track hit and notrack hit

	if (hit_x.size() != hit_y.size() ) {
		std::cout << "Something is wrong ..." << std::endl;
		return;
	}

    // Merge X Y ==> This way is to complicated -> should change the behavior of Class Track - > add only hits and reconstruct later
    // This is a bit stupid and slow -_-
    std::vector<double> hit(hit_x);
    hit.insert(hit.end(), hit_y.begin(), hit_y.end());

    std::vector<double> hit_zpos(hit_x_zpos);
    hit_zpos.insert(hit_zpos.end(), hit_y_zpos.begin(), hit_y_zpos.end());

    std::vector<std::pair<double, double>> hit_pair; 
    hit_pair.reserve(hit.size()); 
    std::transform(hit_zpos.begin(), hit_zpos.end(), hit.begin(), std::back_inserter(hit_pair),
                   [](double zp, double h) { return std::make_pair(zp, h); });

    sort(hit_pair.begin(), hit_pair.end());


	// Step 2 Set local_Derivative
	for (unsigned int ind_hit = 0; ind_hit < hit_pair.size() ; ind_hit++) {
		float sigma = 0.072; // in mm -  should be the resolution of SiPM WIP - hardcoded

        // if those planes are shifted
        // after version v2.7.1 the accumulated shifts is done in alignment_buffer in Plotter.cpp
        float shift = 0.;
        if (m_ShiftParams != nullptr){
            shift = m_ShiftParams[ind_hit];
        }
        float hit_pos = hit_pair.at(ind_hit).second - shift;

        // Match layer position
        m_globalDerivatives[ind_hit] = 1.;
        m_globalDerivatives[ind_hit + (m_nGlobal - m_nAngles)] = 0;

        //  Def local vars
        if (ind_hit%2) {
    	    // For Track X
    	    m_localDerivatives[0] = 1.;
    	    m_localDerivatives[1] = hit_pair.at(ind_hit).first; // Zpos
    	    m_localDerivatives[2] = 0;
    	    m_localDerivatives[3] = 0;
        } else {
		    // For Track Y
		    m_localDerivatives[0] = 0;
    	    m_localDerivatives[1] = 0;
    	    m_localDerivatives[2] = 1.;
    	    m_localDerivatives[3] = hit_pair.at(ind_hit).first;
        }
		EQULOC(m_globalDerivatives, m_localDerivatives, hit_pos, sigma); 
	}
}


void Alignment::KeepShiftParams() {
    
    // Init if not yet init
    if (m_ShiftParams == nullptr){
        delete[] m_ShiftParams;
	    m_ShiftParams  = new float[m_nGlobal]{}; // Init with 0
    } 
       
    // accu shift params
    for (unsigned int i = 0; i < m_nGlobal ; i++) {
        m_ShiftParams[i] += m_parameters[i];
    } 
}

	
void Alignment::PrintKeptParameters(){
    if (m_ShiftParams == nullptr) return ;
    for (unsigned int ind = 0; ind < m_nGlobal ; ind++ ){
        if (ind < (m_nGlobal - m_nAngles)){
            std::cout << "Shift param " << ind << " : " << m_ShiftParams[ind] << std::endl;
        } else {
            std::cout << "Angle param " << ind << " : " << m_ShiftParams[ind] << std::endl;
        }
    }
}

void Alignment::PrintParameters(){
    for (unsigned int ind = 0; ind < m_nGlobal ; ind++ ){
        if (ind < (m_nGlobal - m_nAngles)){
            std::cout << "Shift param " << ind << " : " << m_parameters[ind] << std::endl;
        } else {
            std::cout << "Angle param " << ind << " : " << m_parameters[ind] << std::endl;
        }
    }
}


void Alignment::PrintParameterSigma(){
    for (unsigned int ind = 0; ind < m_nGlobal ; ind++ ){
        if (ind < (m_nGlobal - m_nAngles)){
            std::cout << "Shift paramSigma " << ind << " : " << m_parameterSigma[ind] << std::endl;
        } else {
            std::cout << "Angle paramSigma " << ind << " : " << m_parameterSigma[ind] << std::endl;
        }
    }
}



void Alignment::WriteShiftToFile(TString fileshift) {

    // Def
    ofstream fileout;
    fileout.open(fileshift);

    // Loop params
    for (unsigned int ind = 0; ind < m_nGlobal ; ind++ ){
        if (ind < (m_nGlobal - m_nAngles)){
            fileout << "Shift " << ind << " " << m_ShiftParams[ind] << std::endl;
        } else {
            fileout << "Angle " << ind - (m_nGlobal - m_nAngles) << " " << m_ShiftParams[ind] << std::endl;
        }
    }
    
    // end with close
    fileout.close();

}


