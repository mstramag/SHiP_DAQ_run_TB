/* Reader Program
 * Class for reading configuration file and STiC data
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 * 08.01.2019
 */


#include <iostream>
#include <assert.h>
#include <algorithm>
#include "UtilFunc.cpp"
#include "Decoder.cpp"
#include "../include/Reader.hpp"
#include "string.h"
#include "dirent.h"
#include <climits>


/////////////////////////////////////////////////////////////
///////////////////// Init && GetSet ////////////////////////
/////////////////////////////////////////////////////////////

Reader::Reader()
{
    ReadConfiguration(); 

    GenLayerName();
    // Convert Board offset 
    for(unsigned int board=1; board<=n_board_max ;board++){
        BoardID_config[board][1] = ((double)BoardID_config[board][1]);
    }
}

/////////////////////////////////////////////////////////////
////////////////// Generate layer name //////////////////////
/////////////////////////////////////////////////////////////

void Reader::GenLayerName(){

    //Def var 
    int n_LayerType = 0;
    std::vector<int> buff_layer_type;// "X", "Y"
    std::vector<int> buff_layer_pos;// 1 2 3 4 5 6

    // Check in BoardID_config
    for (std::pair<int, std::array<int, 6>> element : BoardID_config) {
        // Check distinct elements 
        if (std::find(buff_layer_type.begin(), buff_layer_type.end(), element.second[3]) != buff_layer_type.end()  )  {
            // if buff already contains layer  -> increment position
            for (unsigned int ind = 0 ; ind < buff_layer_type.size(); ind++){
                if (buff_layer_type[ind] == element.second[3]) {
                    buff_layer_pos[ind]++;
                    break;
                }
            }
        } else { // buff_layer_type does not contain layer_type
            buff_layer_type.push_back(element.second[3]);
            buff_layer_pos.push_back(1);
        }
    }

    // Gen layer_name from buff information
    /// sort vector of pairs
    std::vector<std::pair <int,int>> pair_buffer;
    for (unsigned int ind = 0 ; ind < buff_layer_type.size(); ind++) {
        pair_buffer.push_back(std::make_pair(buff_layer_type[ind], buff_layer_pos[ind]));
    }
    std::sort(pair_buffer.begin(), pair_buffer.end());

    /// Gen layer_name based on buffer --> "X1", "X2", "Y1" ,...
    for (unsigned int ind = 0 ; ind < pair_buffer.size() ; ind++) {
        std::vector<TString> buff_layer_name; 

        for (int n_layer = 1; n_layer <=pair_buffer[ind].second; n_layer++) {
            buff_layer_name.push_back((TString) Form("%s%d", MapLayerType[pair_buffer[ind].first].c_str(), n_layer) );
        }

        layer_name.push_back(buff_layer_name); 
    
    }

    // Gen Layer Position layer_Position[k][j] = index_LayerPosition --> 1, 3, 2
    // layer_Position = 1  => zpos[0]     >>>> Meaning The 1st position corresponding to the first element of zpos array 
    for (unsigned int i = 0 ; i < layer_name.size() ; i++) {
        std::vector<int> sub_vec(layer_name[0].size(), 0);
        std::vector<float> sub_vec2(layer_name[0].size(), 0.);
        std::vector<int> sub_vecID(layer_name[0].size(), 0);
        layer_Position.push_back(sub_vec); 
        layer_Shift.push_back(sub_vec2); 
        layer_BoardID.push_back(sub_vecID); 
    }
    
    // Set values
    for (unsigned int i = 1; i <= n_board_max; i++) {
        layer_Position[BoardID_config[i][3]][BoardID_config[i][4]-1] = BoardID_config[i][5]; 
        layer_Shift[BoardID_config[i][3]][BoardID_config[i][4]-1] = 0.; 
        layer_BoardID[BoardID_config[i][3]][BoardID_config[i][4]-1] = i;
    }

    // Set BinEdge for event display
    for (unsigned int i = 0; i < n_board_max; i++ ) {
        if (i%2 == 0) {
            display_zPosition_BinEdge.push_back(zpos[i]-5);
            display_zLayerCenter.push_back((zpos[i] + zpos[i+1])/2.);  
        } else {
            display_zPosition_BinEdge.push_back(zpos[i]+5);
        }
    }

    // Modify bins' edges - Add space front and back
    display_zPosition_BinEdge.push_back(zpos.back()+20); // add the last space 

    // Use rotate algorithm since insert function is less efficient
    display_zPosition_BinEdge.push_back(zpos[0]-20); // add the first space and rotate below
    std::rotate(display_zPosition_BinEdge.rbegin(), display_zPosition_BinEdge.rbegin() + 1, display_zPosition_BinEdge.rend());

}

/// Import Shift param

void Reader::ImportShift(float *shiftparam, unsigned int dim){

    std::cout << "Dimiension: " << dim << " Layer_max in Config.: " << n_layer_max << std::endl;
    if (dim != n_layer_max ) {std::cout << "Include angular parameter: NOT IMPLEMENTED\nERROR Expecetd" << std::endl;}

    for (unsigned int i =  0; i < dim; i++) {
        for (unsigned int lay_type = 0 ; lay_type < layer_Position.size() ; lay_type++) {
            for (unsigned int lay_pos = 0 ; lay_pos < layer_Position[lay_type].size(); lay_pos++){
                if (layer_Position[lay_type][lay_pos] == (int) i + 1){ // i + 1 = layer position
                    layer_Shift[lay_type][lay_pos] = shiftparam[i];
                }
            }
        } 
    }
}

void Reader::ReadShiftFromFile(TString filein) {

    // Def Variable
    std::ifstream file_shift(filein);
    if (!file_shift) std::cout << "File is not opened" << std::endl;
    std::string line; 
    std::istringstream buffer;

    // Shift param 
    std::vector<std::pair<unsigned int, float>> shift_params;

    // Loop over file
    while (std::getline(file_shift >> std::ws, line)){
        // End loop when find angular parameters
        if (line.find("Angle") != std::string::npos) break;
    
        // Step 0. buffer a line to a string stream (ss) - For holding the line value 
        std::stringstream ss(line);
        std::string buffer_line; 

        // Step 1. read shift params 
        if (line.find("Shift") != std::string::npos) {
            unsigned int layer_pos;
            float shift;

            ss.str(line.substr(line.find("t")+2));
            std::getline(ss, buffer_line, ' ');
            buffer.str(buffer_line);
            buffer >> layer_pos;
            buffer.clear();

            std::getline(ss, buffer_line, ' ');
            buffer.str(buffer_line);
            buffer >> shift;
            buffer.clear(); 

            shift_params.push_back(std::make_pair(layer_pos, shift));

            continue;
        }
    }

    sort(shift_params.begin(),shift_params.end());

    float only_shift[shift_params.size()];

    unsigned int ind_pair = 0 ;
    for (const std::pair<unsigned int, float> &shift_param : shift_params) {
        only_shift[ind_pair] =  shift_param.second;
        ind_pair++;
    }

    ImportShift(only_shift, shift_params.size());

}
/// Fills boolean matrix with the dead channels information

void Reader::SetDeadChannels(){
    dead_channels.clear();
    for(unsigned int ind_layType=0;ind_layType<layer_name.size();ind_layType++){
        std::vector<std::vector<bool>> layern;
        layern.clear();
        for (unsigned int ind_layNumber=0; ind_layNumber<layer_name.at(ind_layType).size();ind_layNumber++){
            std::vector<bool> channeln;
            channeln.clear();
            for(int ch=0;ch<512;ch++){
                channeln.push_back(0);
            }
            layern.push_back(channeln);
        }
        dead_channels.push_back(layern);
    }

    for(unsigned int bo=0;bo<n_board_max;bo++){
        std::stringstream boardID;
        boardID<<std::setw(2)<<setfill('0')<<BoardID_config[bo+1][0];
        
        string Dead_Pathdir = GetEnv("DEADPATH")+"Board"+boardID.str()+"/";  
        DIR* dirp = opendir(Dead_Pathdir.c_str());
        struct dirent * dp; //pointer to the directory
        for(unsigned int STiC_ID=0;STiC_ID<8;STiC_ID++){
            
            string Dead_Path=Dead_Pathdir+"dead_ch/stic_"+std::to_string(STiC_ID)+".txt";
            
            std::ifstream file_stic(Dead_Path);
            assert(file_stic.is_open()); // Will raise an error if the file is not opened
            for(unsigned int i=0;i<64;i++){
                unsigned int en=2;
                std::string line=""; 
                std::istringstream buffer;

                getline(file_stic, line);
                
                if (line.find("en= ") != std::string::npos){ // LOOK FOR THE CHANNEL STATUS
                    buffer.str(line.substr(line.find("en= ")+4));
                    buffer >> en;
                    buffer.clear();
                                   
                    unsigned int xy  = BoardID_config[bo+1][3];
                    unsigned int lay = BoardID_config[bo+1][4]-1;
                    unsigned int ch_position = ((int)(STiC_ID/2))*128+(i*2)+(STiC_ID%2);
                    
                    if(en==0)dead_channels[xy][lay][ch_position]=0;
                    else if(en==1){dead_channels[xy][lay][ch_position]=1;}
                    else {std::cout<<"hey, something is wrong with your board config file..."<<std::endl;
                            std::cout<<"I can not find information on dead channels"<<std::endl;}
                } 
            }
        }
    }

}


/////////////////////////////////////////////////////////////
///////////////////// String manager ////////////////////////
/////////////////////////////////////////////////////////////

// Please be careful that string::strncmp is case-sensitive
// and input is string not HEX 

bool Reader::StartWith(string main_text, string pre_text) 
{
    // strncmp return 0 when main_text contains pre_text
    // add ! for conversion
    return !strncmp(main_text.c_str(), pre_text.c_str(), pre_text.size());
}

bool Reader::is_Header(string hex_line)
{
    return StartWith(hex_line, "00CDEF");
}

bool Reader::is_Trigger(string hex_line)
{
    return StartWith(hex_line, "F"); 
}

bool Reader::is_Blacklisted(string hex_line)
{
    // Do not decode this line
    //else if(hitdata_string.find("#")!=std::string::npos || hitdata_string.find("*")!=std::string::npos || hitdata_string == " " || hitdata_string.find("ZEROs")!=std::string::npos){
    return StartWith(hex_line, "***") || StartWith(hex_line, "No data received");
}

bool Reader::is_Valid_Time(double trigger_time, double hit_time, double board_offset, double time_window) 
{
    /*
    Return True if the time of this hit is within the time_window (ns).
    Put it when building the event.
    */
    double diff = abs(trigger_time-hit_time);
    return abs(diff-board_offset) < time_window;
}


/////////////////////////////////////////////////////////////
/////////////////// Read and Create ROOT ////////////////////
/////////////////////////////////////////////////////////////

// Create Root file directly from STiC Data -> 1 Entry  = 1 Hit
void Reader::CreateHitRoot(string BoardFilename, int NEntry, short verbose_trigger) 
{
    // Step 1 Get information from filename for checking when decode hit data
    std::cout<<"reading file: "<< BoardFilename <<std::endl;
    
    //// Read board ID from file name
    Int_t board_id; // the physical board id from file name
    string Run_number    = BoardFilename.substr(BoardFilename.length()-26, 8);
    string sub           = BoardFilename.substr(BoardFilename.length()-17, 17);
    string rootfile_tree = GetEnv("PWD")+"/rootfiles/"+Run_number+"/"+sub+"_HitData.root" ; 
    sub                  = sub.substr(4,6);
    board_id             = stoi(sub);
    std::cout << "Board Physical number: " << board_id << std::endl;
    
    // Step 2 Declare variables to read the data file
    // Note that type int has a limit at 2147483647
    // Trigger variables
    uint64_t TrigCounter_dec     = 0; //Trigger counter coming from the board counter
    uint64_t TrigCounter         = 0; //Actual trigger counter in data
    uint64_t TrigMismatch         = 0xFFFFFFFFFFFFFFFF; // For checking a consistancy between TrigCounter_dec and TrigCounter
    uint32_t Number_TrigOverroll = 0; // For Oct2018 data -> limit no. of trig counter is 0xFFFF = 65535
    uint64_t Trig_Time;
    uint64_t TrigLimit        = 0xFFFF; // May put to config later
    string   TrigBefore       = "FFFC"; // Init with FFFC with an assumption that the first trigger should begin with FFFC 

    // Hit variables 
    uint64_t Amp;  // Time over Threshold
    uint64_t Hit_Time; // Hit Time
    uint64_t Fine_Time; // Fine Time

    // RealTime -> for hit and trigger
    double Trig_RealTime; 
    double Hit_RealTime; 

    // Detector Configuration variables
    uint32_t PlaneCode;// X = 0, Y = 1, U = 2, V = 3 // Get directly from configuration 
    uint32_t PlaneNumber;// 1 2 3 4

    uint32_t Board_IP; // Board IP
    uint32_t Board_ID; // Board ID
    uint32_t STiC_ID;  // STiC ID
    uint32_t Ch_ID;    // Ch ID on SiPM

    uint32_t Ch_Position; // Position on a layer

    // Document variables
    Decoder decoder;
    std::ifstream file_data(BoardFilename);
    std::string line; 

    // Set ROOT file  
    // HitData
    TFile *OutputFile = new TFile(rootfile_tree.c_str(),"RECREATE");
    TTree *tree = new TTree("data","data tree");

    tree->Branch("TriggerID"      ,&TrigCounter_dec);
    tree->Branch("TriggerCounter" ,&TrigCounter);

    tree->Branch("PlaneCode"  ,&PlaneCode);
    tree->Branch("PlaneNumber",&PlaneNumber);
    tree->Branch("BoardIP"    ,&Board_IP);
    tree->Branch("BoardID"    ,&Board_ID); 
    tree->Branch("STiCID"     ,&STiC_ID);
    tree->Branch("ChID"       ,&Ch_ID);
    tree->Branch("ChPosition" ,&Ch_Position);  

    tree->Branch("TOT",&Amp);
    tree->Branch("HitTime",&Hit_Time);
    tree->Branch("FineTime",&Fine_Time);
    tree->Branch("TriggerTime",&Trig_Time);

    tree->Branch("HitRealTime",&Hit_RealTime);
    tree->Branch("TriggerRealTime",&Trig_RealTime);
    
    // TriggerData
    while (rootfile_tree.find("HitData") != string::npos)
    {
        rootfile_tree.replace(rootfile_tree.find("HitData"), 7, "Trigger");
    }
    TFile *OutputFile_trigger = new TFile(rootfile_tree.c_str(),"RECREATE");
    TTree *tree_trigger = new TTree("trigger","trigger tree");

    tree_trigger->Branch("TriggerID"      ,&TrigCounter_dec);
    tree_trigger->Branch("TriggerCounter" ,&TrigCounter);
    tree_trigger->Branch("TriggerTime",&Trig_Time);
    tree_trigger->Branch("TriggerRealTime",&Trig_RealTime);
    tree_trigger->Branch("PlaneCode"  ,&PlaneCode);
    tree_trigger->Branch("PlaneNumber",&PlaneNumber);
    tree_trigger->Branch("BoardIP"    ,&Board_IP);
    tree_trigger->Branch("BoardID"    ,&Board_ID); 


    // Step 3 Read the file and fill root file

    assert(file_data.is_open()); // Will raise an error if the file is not opened
    while (std::getline(file_data >> std::ws, line)){
        // ****Problem in this version is there will be no data if there is no hit*****

        // Step 3.0 Clean data
        if (is_Blacklisted(line) || is_Header(line) ) continue; // Ignore these header and blacklisted lines

        // Step 3.1^(in while) Openning this file and read data to line
        // convert line to uint64
        uint64_t line_int; //used to convert the string into hex number
        std::stringstream strm(line);
        strm >>std::hex>>line_int;

        // Step 3.2 Put to class Decoder
        // For debugging 
        if (TrigCounter == (unsigned int)NEntry) break; 
        decoder.set_Input(line_int);

        // Step 3.3 Read the first Start Reading!
        // In the updated version, there are only Trigger and Hit lines
        if (is_Trigger(line)) {
            TrigCounter++;
            // Check trigger reset  
            if (TrigBefore == decoder.TriggerCodeLast  && line.substr(0,4) == decoder.TriggerCodeFirst) {
                Number_TrigOverroll++;
            }

            // Assign trigger values
            // Reset push the trigger counter to 0 -> need to shift by Number_TrigOverroll
            TrigCounter_dec = decoder.Trigger_Counter() + Number_TrigOverroll*TrigLimit + Number_TrigOverroll;
            Trig_Time = decoder.Trigger_Time();
            Trig_RealTime  = decoder.TriggerRealTime(Trig_Time, board_frequency)  ; 
            TrigBefore = line.substr(0,4);
            if (TrigCounter != TrigCounter_dec && TrigMismatch == 0xFFFFFFFFFFFFFFFF) {
                TrigMismatch = TrigCounter; 
            }

            // Fill Tree_trigger
            Board_IP = BoardID_config[board_id][0];
            Board_ID = board_id;   
            PlaneCode   = BoardID_config[board_id][3]; 
            PlaneNumber = BoardID_config[board_id][4]; 

            tree->Fill();
            tree_trigger->Fill();

            // make a possibility to shift the starting trigger -> not yet implemented 
        } else 
        {
            if (TrigCounter == 0){
                // Exclude everything before the first trigger line
                continue;
            } else if (is_Trigger(line) != 1 && is_Header(line) != 1) 
            {   // Hit data
                // Check for valid hits
                // -> Amp not saturated and TOT > 0
                if (decoder.is_Amp_Saturated() || decoder.Amp() == 0 ) continue; 

                // Board Configuration
                Board_IP = decoder.Board_IP();
                Board_ID = board_id;   
                STiC_ID  = decoder.STiC_ID(); 
                Ch_ID    = decoder.Ch_ID(); 
                //// Get Plane from Configuration
                PlaneCode   = BoardID_config[board_id][3]; 
                PlaneNumber = BoardID_config[board_id][4]; 

                // Check consistancy 
                assert(BoardID_config[board_id][0] == Board_IP);

                // 2 Steps computation -> for readiability
                Ch_Position = (Board_ID - 1)%3 ;
                Ch_Position = (Ch_Position * 512) + ((int)(STiC_ID/2))*128 + (Ch_ID*2) + (STiC_ID%2); // NEED Checking

                Amp      = decoder.Amp(); // TOT
                Hit_Time = decoder.Hit_Time();
                Fine_Time= decoder.Fine_Time();
                
                // Convert Hit and Trigger time to real time
                Hit_RealTime   = decoder.HitRealTime(Hit_Time, Fine_Time,  board_frequency, fine_bits); 

                tree->Fill();
            }  else 
            {
                std::cout << "Unimplemented line: " << line << std::endl;
                break;
            }
        } //end else in [if is_trigger}
        

        if (verbose_trigger == 2)
        {   // Print all lines with trigger counter, decoded and trigger_time 
            std::cout << line << " " << is_Trigger(line) << " Counter "  << TrigCounter << " DecTrig " << TrigCounter_dec << " Time " << Trig_Time << " Overroll " << Number_TrigOverroll << std::endl; 
        }
        
    } // End while loop

    // Step 4 Report, save and close files
    // Report TrigCounter and TrigCounter_dec
    std::cout << "Last decoded Trigger Counter: " << TrigCounter_dec << std::endl;
    if (verbose_trigger > 0 )
    {
        std::cout << "Trigger Counter: " << TrigCounter << std::endl;
        if (TrigMismatch != 0xFFFFFFFFFFFFFFFF)
        {
            std::cout << "The first mismatch is at TrigCounter: " << TrigMismatch 
                      << " \nRun the program until this trigger number and turn debug mode on" << std::endl;
        } else
        {
            std::cout << "Every triggers is snychronised! " << std::endl;
        }
        std::cout << "=============================================" << std::endl;
    } else 
    {
        if (TrigMismatch != 0xFFFFFFFFFFFFFFFF)
        {
            std::cout << "******Trigger mismatch found! Run with verbose_trigger = 1 to find out******" << std::endl;
        } 
    }

    // Save and close
    file_data.close();

    // Save Hit
    OutputFile->cd();
    tree->Write();
    OutputFile->Close();

    OutputFile_trigger->cd();
    tree_trigger->Write();
    OutputFile_trigger->Close();

}

// Call CreateHitRoot for all data in data_dir directory -> 1 Entry = 1 Hit
void Reader::CreateHitAllBoard(string Run_number, int NEntry)
{
    // Step 1 Report all directory
    string Data_Path = GetEnv("DATAPATH")+"RUN_"+Run_number+"/";
    string Output_Path = GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/";
    std::cout << "Loop over " << Data_Path << std::endl;
    std::cout << "Output in " << Output_Path << std::endl;

    // Must have a function to check and create directory => Not yet implemented 
    //DIR* out_path = opendir(Output_Path);

    // Step 2 Open directory and loop all arm_* txt files

    DIR* dirp = opendir(Data_Path.c_str());
    struct dirent * dp; //pointer to the directory
    string Full_Path;

    while((dp = readdir(dirp)) != NULL)   
    {
        Full_Path=Data_Path+dp->d_name;
        if(Full_Path.find("arm_")!=std::string::npos)
        {
            CreateHitRoot(Full_Path, NEntry); 
        }
    }
}


/**********************************************************************************************************************************/
//CreateTree()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CreateTree is the Event builder. From the data files coming from the n_board_max (or less) boards, hits belonging ////////////////
//to the same trigger count are merged. The assumption is that boards never miss to receive a trigger //////////////////////////////
/**********************************************************************************************************************************/

void Reader::CreateTree(string Run_number, bool time_window_flag, bool Reject_Saturated_flag)
{

    //the first trigger that the board receives is the Reset, this flag is used to discard all hits coming before the second trigger
    std::vector<bool> flagcntone(n_board_max, 0); 
    
    //define the root output file
    TString rootfile_tree = (GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/");
    TString root_name = "Run_"+Run_number;
    if (time_window_flag) root_name += "_withtime";
    if (!Reject_Saturated_flag) root_name += "_withSatHit";
    rootfile_tree += root_name+".root" ;
    TFile *outputfile=new TFile(rootfile_tree,"RECREATE");
    TTree *tree= new TTree("data","data tree");
    
    //infos to be stored in the tree
    
    Int_t Eventnum=0;                   //ID of the current event

    std::vector<uint64_t> TriggerID;
    std::vector<uint64_t> TriggerCounter;
    std::vector<uint32_t> PlaneCode;
    std::vector<uint32_t> PlaneNumber;
    
    std::vector<uint32_t> Board_ID;     //physical id of the boards (1-n_board_max)
    std::vector<uint32_t> Board_IP; 
    std::vector<uint32_t> STiC_ID;      //identification number of the stic position inside one board (0-3)
    std::vector<uint32_t> Ch_ID;        //channel number in the stic (0-63)
    std::vector<uint32_t> Ch_Position; // Position on a layer
    std::vector<uint32_t> Amp;          //time over threshold of the hit

    std::vector<uint64_t> Hit_Time;     //coarse arrival time of the hit
    std::vector<uint32_t> Fine_Time;    //fine arrival time of the hit
    std::vector<uint64_t> Trig_Time;     //trigger time per very board
    
    std::vector<double> Trig_RealTime; 
    std::vector<double> Hit_RealTime; 
    
    //Definition of the Branches that we want to store inside the tree
    tree->Branch("Eventnum", &Eventnum);
    tree->Branch("TriggerID"      ,&TriggerID);
    tree->Branch("PlaneCode"  ,&PlaneCode);
    tree->Branch("PlaneNumber",&PlaneNumber);
    tree->Branch("BoardIP"    ,&Board_IP);
    tree->Branch("BoardID"    ,&Board_ID); 
    tree->Branch("STiCID"     ,&STiC_ID);
    tree->Branch("ChID"       ,&Ch_ID);
    tree->Branch("ChPosition" ,&Ch_Position);  
    tree->Branch("TOT",&Amp);
    tree->Branch("HitTime",&Hit_Time);
    tree->Branch("FineTime",&Fine_Time);
    tree->Branch("TriggerTime",&Trig_Time);
    tree->Branch("HitRealTime",&Hit_RealTime);
    tree->Branch("TriggerRealTime",&Trig_RealTime);
    
    //flags that will be useful to align the triggers and the hits in all the boards
    std::vector<UInt_t> TriggerCheck(n_board_max, 0);
    std::vector<UInt_t> Overroll(n_board_max, 0);
    std::vector<UInt_t> TriggerCnt(n_board_max, 0);
    std::vector<UInt_t> TriggerCntLast(n_board_max, 0);
    std::vector<UInt_t> TriggersAccepted(n_board_max, 0); 
    //path where to find the data files
    TString tmpfile=getenv("DATAPATH");
    
    cout << "     " << tmpfile << endl;
    tmpfile+="RUN_"+Run_number+"/";
    cout << "     " << tmpfile << endl;
    ifstream raw;
    
    //Array of data converted from the lines for the boards.
    std::vector<std::vector<uint64_t>> filevector;
    for(UInt_t i=0;i<n_board_max;i++) {filevector.push_back(std::vector<uint64_t> ()); ;filevector[i].clear();} // Clean the filevector - this should contain data
    
    UInt_t tot_number_events=0;
    
    DIR* dirp = opendir(tmpfile);
    struct dirent * dp; //pointer to the directory
    
    // tmpfile - data/
    // RUN - dirp - loop over directory in tmpfile
    // list of files in RUN - dp - list of files in dirp
    // log file - tmpfilefile - data log file
    // load all the files infos in vectors - filevectors - (one for every board)
    UInt_t Trigcounter = 0;
    while((dp = readdir(dirp)) != NULL){   
        
        TString tmpfilefile=tmpfile+dp->d_name;
        string che(tmpfilefile);
        
        if(che.find("raw_")!=std::string::npos){
            //std::cout<<"reading file: "<<dp->d_name<<std::endl;
            std::cout<<"reading raw data file: "<<tmpfilefile<<std::endl;
            
            // Read the id of the board
            Int_t b=-1; //b is the physical board id
            // UInt_t c=0;
            std::string sub = dp->d_name;
            sub = sub.substr(4,6);
            b=stoi(sub);
            std::cout<<"board number: "<<b<<std::endl;
            
            // Declare variables to read the data file
            raw.open(tmpfilefile);
            string hitdata_string; //this string is used as temporary container of each line in the file
            Trigcounter = 0; //Trigger counter coming from the board counter
            
            // Read the file
            while (!raw.eof()) {
                
                // Openning this file and read data
                // Get line
                getline(raw,hitdata_string);
                if( raw.eof() ) break;  

                if (is_Blacklisted(hitdata_string) || is_Header(hitdata_string) ) continue; // Ignore these header and blacklisted lines
                uint64_t hitdata_stringint; //used to convert the string into number
                std::stringstream strm(hitdata_string);
                strm >>std::hex>>hitdata_stringint;
                

                // Trigger
                // The trigger word is flagged in the data through the identifier 
                if (is_Trigger(hitdata_string)) {
                    TriggersAccepted[b-1]++;
                    if(b!=-1 && flagcntone[b-1]){filevector[b-1].push_back(hitdata_stringint);}
                    
                    // Extract the counter from the trigger line
                    // Def Decoder
                    Decoder decoder;
                    decoder.set_Input(hitdata_stringint);
                    Trigcounter=decoder.Trigger_Counter();

                    if(b!=-1 && (Trigcounter>0 || filevector[b-1].size()>0))flagcntone[b-1]=1; 
                    if(b!=-1){
                        if(Trigcounter>TriggerCntLast[b-1] && (Trigcounter-TriggerCntLast[b-1])>1) TriggerCnt[b-1]+=Trigcounter-TriggerCntLast[b-1];
                        else TriggerCnt[b-1]++;
                        TriggerCntLast[b-1]=Trigcounter;
                        if(flagcntone[b-1])filevector[b-1].push_back(Trigcounter);
                        if(Trigcounter>TriggerCheck[b-1])TriggerCheck[b-1]++;
                        else{Overroll[b-1]++; TriggerCheck[b-1]=Trigcounter;}
                    }
                }
                //the last case is the hits information
                else{
                    uint64_t hitdata_stringint; //used to convert the string into number
                    std::stringstream strm(hitdata_string);
                    strm >>std::hex>>hitdata_stringint;
                    if(b!=-1 && flagcntone[b-1]){filevector[b-1].push_back(hitdata_stringint);}
                }
                if (!raw.good()) break;
                // =*=*=*=*=*=*=*=*=*=**=*= End of reading file =*=*=*=*=*=*=*=*=*=*=*=*
            }
            std::cout << "BOARD: " << b << " Last (Decoded) Trigcounter: " << Trigcounter << std::endl;
            raw.close();//close board file
            
        }//if arm
    }//while inside the directory
    closedir(dirp); //close the directory
    
    // At this point, n_board_max vectors are filled with trigger time, trigger count and hit information for all the boards
    bool save=true; //flag to choose if one event will be saved

    // total number of events registered, taken as the maximum number of events registered by one board
    for(unsigned int j=0;j<n_board_max;j++)if(TriggerCnt[j]>tot_number_events)tot_number_events=TriggerCnt[j];        

    // maximum number of times that one counter overrolled
    UInt_t max_Overrolls=0;                                                                         
    for(unsigned int j=0;j<n_board_max;j++)if(Overroll[j]>max_Overrolls)max_Overrolls=Overroll[j];
    
    // sanity check printouts
    for(unsigned int j=0;j<n_board_max;j++){std::cout<<"Triggers for board: "<<j+1<<" "<<TriggersAccepted[j]<<std::endl;}
    std::cout<<"Events collected are: "<<tot_number_events<<std::endl;
    std::cout<<"Overrolls are: "<<max_Overrolls<<std::endl;
    
    //second step is -
    //read the vectors to build the events
    
    //integer used to "remember" the vector element with the last hit in one event
    std::vector<UInt_t> stop_point(n_board_max, 0);                                                      

    //trigger counter
    std::vector<UInt_t> Triggerc(n_board_max, 0);                                                        

    //trigger time in a given event, trigger time in the last event, difference between the two
    std::vector<uint64_t> trig_time_now(n_board_max, 0);//,trig_time_last(n_board_max, 0),trig_time_diff(n_board_max, 0); 

    //counter of overrols per board until all the boards employed overrolled
    std::vector<UInt_t> Overrolls_board(n_board_max, 0);                                                 
    std::vector<int> counter_board_error(n_board_max, 0); 
    
    for(UInt_t l=0;l<tot_number_events;l++){
        int global_Overroll=0;      //flag to check if all boards got to the overroll
        for(unsigned int b=0;b<n_board_max;b++){global_Overroll+=Overrolls_board[b];}
        if(global_Overroll>=7)for(unsigned int b=0;b<n_board_max;b++){
                                Overrolls_board[b]=0;
                                /*save=false; std::cout<<"save at false for Overroll"<<std::endl;*/
                                } //in case the run is split into spills, the overroll is the new reset
        
        // cleaning the vectors at the beginning of every event
        
        Trig_Time.clear();
        Trig_RealTime.clear();
        TriggerID.clear();
        for(unsigned int i=0;i<n_board_max;i++){
            Trig_Time.push_back(ULONG_MAX);
            Trig_RealTime.push_back(ULONG_MAX);
            TriggerID.push_back(ULONG_MAX);
        }
        
        Board_ID.clear();
        Board_IP.clear();
        PlaneCode.clear();
        PlaneNumber.clear();
        STiC_ID.clear();
        Ch_ID.clear();
        Amp.clear();
        Hit_Time.clear();
        Fine_Time.clear();
        Ch_Position.clear();
        Hit_RealTime.clear();

        
        for(UInt_t bo=0;bo<n_board_max;bo++){ //for loop on the boards
            int eventseparator=0;
            
            Int_t next=100000;
            
            if(Overrolls_board[bo])continue;
            
            for(UInt_t le=stop_point[bo]; le<filevector[bo].size();le++){
                uint64_t hitdata=filevector[bo].at(le);
                bool foundtrigger=false;
                // Def Decoder
                Decoder decoder_hitdata;
                decoder_hitdata.set_Input(hitdata);


                //header
                if(decoder_hitdata.is_Header()) {
                    continue;
                }
                //trigger
                else if (decoder_hitdata.is_Trigger()) {
                    eventseparator++;
                    //case in which one board missed to send a trigger
                    if(eventseparator==1 && le!=filevector[bo].size()-1 && filevector[bo].at(le+1) >Triggerc[bo]+1){
                        eventseparator++;
                        Triggerc[bo]++;
                        }
                    //case in which a Reset signal was sent or the counter reached its end and started again
                    if(eventseparator==1 && filevector[bo].at(le+1) <Triggerc[bo]+1 && Triggerc[bo]!=0){eventseparator++; Triggerc[bo]=filevector[bo].at(le+1);
                        }
                    //normal case (the trigger count is increasing monotone)
                    if(eventseparator==1)
                    {
                        foundtrigger=true;
                        Triggerc[bo]=filevector[bo].at(le+1);
                        trig_time_now[bo] = decoder_hitdata.Trigger_Time();  // convert to 170MHz clock cycles or equivalent
                        
                        Trig_Time.at(bo)= trig_time_now[bo]; // 680MHz or equivalent
                        Trig_RealTime.at(bo) = decoder_hitdata.TriggerRealTime(Trig_Time.at(bo), board_frequency) ;
                        TriggerID.at(bo)=(int)filevector[bo].at(le+1);
                        le++;
                    }
                    if(eventseparator>1){
                        if(filevector[bo].at(le+1) <Triggerc[bo]+1){
                            Overrolls_board[bo]++;
                            Triggerc[bo]=-1;
                            }
                        stop_point[bo]=le-1;
                        le=filevector[bo].size();
                        continue;
                    }

                }

                //std::cout << "Trigger: " << foundtrigger << std::endl;
                if(foundtrigger){

                    //look for data between one trigger and the following
                    for(int k = le+1 ; k < next+(int)le+1 ; k++){
                        
                        if(k<0 || ((unsigned int)(k))>=filevector[bo].size())continue;

                        uint64_t hitdatad=filevector[bo].at(k);
                        // Def Decoder
                        Decoder decoder_hitdatad, decoder_hitdatad_before;
                        decoder_hitdatad.set_Input(hitdatad);
                        decoder_hitdatad_before.set_Input(filevector[bo].at(k-1));

                        //header
                        if(decoder_hitdatad.is_Header()) {
                            continue;
                        }

                        //trigger or trigger counter
                        else if (decoder_hitdatad.is_Trigger() || (k>1 && decoder_hitdatad_before.is_Trigger())) {
                            next=k-(le+1);
                            continue;
                        }
                        //hit data
                        else  {
                            // reject time out events if the flag is enable
                            if (decoder_hitdatad.is_Amp_Saturated() && Reject_Saturated_flag) continue;  

                            if(eventseparator==1) 
                            {
                                // Select time window between Trigger and Hit
                                if (time_window_flag && !is_Valid_Time((double) trig_time_now[bo], 
                                            (double) decoder_hitdatad.Hit_Time(), (double)BoardID_config[bo+1][1], time_window)) continue;

                                // Fill values to vectors
                                // Check consistancy 
                                if (BoardID_config[bo+1][0] != (int)(decoder_hitdatad.Board_IP()) && !counter_board_error[bo] ) {
                                    std::cout << "\033[0;41mDecoded IP: " << decoder_hitdatad.Board_IP() << " In Configuration.txt, IP of Board " << bo+1 << " is " << BoardID_config[bo+1][0] << "\033[0m" << std::endl;
                                    std::cout << "\033[0;31mSetting in Configuration.txt does not match with a decoded board IP.\nMake sure that Configuration is set correctly.\nAbort this function.\033[0m" << std::endl; 
                                    counter_board_error[bo]++;
                                }
                                Board_ID.push_back(bo+1);
                                STiC_ID.push_back((int)(decoder_hitdatad.STiC_ID()));  //3b
                                Ch_ID.push_back((int)(decoder_hitdatad.Ch_ID()));  //6b
                                Amp.push_back((int)(decoder_hitdatad.Amp()));  //8b
                                Hit_Time.push_back(decoder_hitdatad.Hit_Time());   //36b
                                Fine_Time.push_back((int)(decoder_hitdatad.Fine_Time()));   //5b
                                Board_IP.push_back((int)(decoder_hitdatad.Board_IP()));  //3b
                                PlaneCode.push_back(BoardID_config[bo+1][3]);
                                PlaneNumber.push_back(BoardID_config[bo+1][4]);
                                //Ch_Position.push_back(((bo%3)*512) + ((int)(decoder_hitdatad.STiC_ID()/2))*128 + 
                                //        (decoder_hitdatad.Ch_ID()*2) + (decoder_hitdatad.STiC_ID()%2));
                                Ch_Position.push_back(((int)(decoder_hitdatad.STiC_ID()/2))*128 + 
                                        (decoder_hitdatad.Ch_ID()*2) + (decoder_hitdatad.STiC_ID()%2));
                                Hit_RealTime.push_back(decoder_hitdatad.HitRealTime(decoder_hitdatad.Hit_Time(), 
                                            decoder_hitdatad.Fine_Time(),  board_frequency, fine_bits));
                            }
                            else 
                            {
                                continue;
                            }
                        } //end else hitdata
                    }//for between two triggers
                }//if foundtrigger
            }//for on the size of the vector
        }//for on the boards

        unsigned int testT=0;
        for(unsigned int b=0;b<n_board_max;b++){if(Trig_Time[b]==ULONG_MAX)testT++;}// check how many boards did not receive a trigger
        if(testT>n_board_max-1){save=false;} // if only one board did receive the trigger, so not save the event
        
        if(save){
            tree->Fill();
            Eventnum++;
        }
        save =true;
        
    }//for on the events
    
    //write the tree in the file
    tree->Write();
    outputfile->Close();
   
    std::cout<<"file "<<rootfile_tree<<" created"<<std::endl;
    std::cout<<"the .root file exists, plot the variables!"<<std::endl;
}


// Build Event
// Should include TriggerCounter - Not yet implemented
// Create slow version
void Reader::CreateEventTree(string Run_number)
{   std::cout<<"create rootfile and tree"<<std::endl;
    // Step 1 Define an output ROOT file
    TFile *event_file = new TFile((GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/Event_"+Run_number+".root").c_str(), "RECREATE");
    TTree *event_tree = new TTree("Event","Event tree");

    // Step 2 Define variable in EventROOT
    // Detector Configuration variables
    HitData event_data;

    event_tree->Branch("TriggerID"      ,&event_data.TriggerID);
    event_tree->Branch("TriggerCounter"      ,&event_data.TriggerCounter);
    event_tree->Branch("PlaneCode"  ,&event_data.PlaneCode);
    event_tree->Branch("PlaneNumber",&event_data.PlaneNumber);
    event_tree->Branch("BoardIP"    ,&event_data.Board_IP);
    event_tree->Branch("BoardID"    ,&event_data.Board_ID); 
    event_tree->Branch("STiCID"     ,&event_data.STiC_ID);
    event_tree->Branch("ChID"       ,&event_data.Ch_ID);
    event_tree->Branch("ChPosition" ,&event_data.Ch_Position);  
    event_tree->Branch("TOT",&event_data.Amp);
    event_tree->Branch("HitTime",&event_data.Hit_Time);
    event_tree->Branch("FineTime",&event_data.Fine_Time);
    event_tree->Branch("TriggerTime",&event_data.Trig_Time);
    event_tree->Branch("HitRealTime",&event_data.Hit_RealTime);
    event_tree->Branch("TriggerRealTime",&event_data.Trig_RealTime);
    std::cout<<"define identifier"<<std::endl;    
    // Def EventNo. Identifier -> Faster way to read ROOT file 
    unsigned int entry_identifier[25] = {}; // In C++ Init. array this way will result in array[i] = 0 for all i

    char buff[31]; 
    string Board_name;
    std::cout<<"get max trigger"<<std::endl;
    // Get MaxTriggerID from Master board
    snprintf(buff, sizeof(buff), "arm_%02d_phys_%02d_ip_HitData.root", MasterBoardID, BoardID_config[MasterBoardID][0] ); 
    Board_name = buff; 
    uint64_t MaxTriggerID= GetMaxTriggerID(Run_number, Board_name);
    std::cout << "MaxTrig: " << MaxTriggerID << std::endl;

    // Load Chain 
    TChain* chain[25];  
    BuffData buff_data[25];

    // Set the chain for every boards
    for (unsigned int loop_BoardID = 1; loop_BoardID <= n_board_max ; loop_BoardID++ )
    {
        if (BoardID_config[loop_BoardID][0] > 100 ) continue; // Skip deactivated boards
        snprintf(buff, sizeof(buff), "arm_%02d_phys_%02d_ip_HitData.root", loop_BoardID, BoardID_config[loop_BoardID][0] ); 
        Board_name = buff ;
        string rootfile_tree = GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/"+Board_name ;
        chain[loop_BoardID] = new TChain("data");
        chain[loop_BoardID]->Add(rootfile_tree.c_str());
        std::cout<<"defining branches for board: "<<loop_BoardID<<std::endl;
        chain[loop_BoardID]->SetBranchAddress("TriggerID"      , &buff_data[loop_BoardID].TriggerID     );
        chain[loop_BoardID]->SetBranchAddress("TriggerCounter" , &buff_data[loop_BoardID].TriggerCounter  );
        chain[loop_BoardID]->SetBranchAddress(  "PlaneCode"    , &buff_data[loop_BoardID].PlaneCode     );
        chain[loop_BoardID]->SetBranchAddress(  "PlaneNumber"  , &buff_data[loop_BoardID].PlaneNumber   );
        chain[loop_BoardID]->SetBranchAddress(   "BoardIP"     , &buff_data[loop_BoardID].Board_IP      );
        chain[loop_BoardID]->SetBranchAddress(   "BoardID"     , &buff_data[loop_BoardID].Board_ID      );
        chain[loop_BoardID]->SetBranchAddress(   "STiCID"      , &buff_data[loop_BoardID].STiC_ID       );
        chain[loop_BoardID]->SetBranchAddress(   "ChID"        , &buff_data[loop_BoardID].Ch_ID         );
        chain[loop_BoardID]->SetBranchAddress(   "ChPosition"  , &buff_data[loop_BoardID].Ch_Position   );
        chain[loop_BoardID]->SetBranchAddress(  "TOT"          , &buff_data[loop_BoardID].Amp           );
        chain[loop_BoardID]->SetBranchAddress(   "HitTime"     , &buff_data[loop_BoardID].Hit_Time      );
        chain[loop_BoardID]->SetBranchAddress(   "FineTime"    , &buff_data[loop_BoardID].Fine_Time     );
        chain[loop_BoardID]->SetBranchAddress(   "TriggerTime" , &buff_data[loop_BoardID].Trigger_Time  );
        chain[loop_BoardID]->SetBranchAddress(   "HitRealTime" , &buff_data[loop_BoardID].Hit_RealTime  );
        chain[loop_BoardID]->SetBranchAddress("TriggerRealTime", &buff_data[loop_BoardID].Trig_RealTime );



    }
    uint64_t TriggerID = 1; 

    while (TriggerID <= MaxTriggerID)
    {
        //std::cout << "event_data.TriggerID: " << event_data.TriggerID << std::endl;
        // Step 3.0 Clear vector and struct
        event_data = {}; //Reset values
        // Try with .clear()

        event_data.TriggerID = TriggerID; 
        // Step 3.1 Loop over other boards with boardID ->GetHitData(Identifier, event_data.TriggerID)
        for (unsigned int loop_BoardID = 1; loop_BoardID <= n_board_max ; loop_BoardID++ )
        {
            if (BoardID_config[loop_BoardID][0] > 100 ) continue; // Skip deactivated boards
            GetHitData(event_data, chain[loop_BoardID], entry_identifier[loop_BoardID], event_data.TriggerID, buff_data[loop_BoardID]);
            entry_identifier[loop_BoardID] = event_data.entry ; 

        }
        
        // Step 3.4 Fill the tree 
        event_tree->Fill();
        TriggerID++;
        if (TriggerID%1000  == 0 ) std::cout << "============================ Finish Event " << event_data.TriggerID << "===================" << std::endl;
    }

    // Step 4 save
    event_file->cd();
    event_tree->Write();
    event_file->Close();

}

// Build Event trigger -> Need optimisation
void Reader::CreateEventTrigger(string Run_number)
{
    // Step 1 Define an output ROOT file
    TFile *trigger_file = new TFile((GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/Trigger_"+Run_number+".root").c_str(), "RECREATE");
    TTree *trigger_tree = new TTree("Trigger","Trigger tree");

    // Step 2 Define variable in EventROOT
    // Detector Configuration variables
    uint64_t TriggerID;
    uint64_t TriggerCounter ; // Trigger Counter
    std::vector<uint32_t> PlaneCode;// X = 0, Y = 1, U = 2, V = 3 // Get directly from configuration 
    std::vector<uint32_t> PlaneNumber;// 1 2 3 4
    std::vector<uint32_t> Board_IP; // Board IP
    std::vector<uint32_t> Board_ID; // Board ID

    std::vector<uint64_t> Trig_Time; // Fine Time
    std::vector<double> Trig_RealTime; 

    trigger_tree->Branch("TriggerID"      ,&TriggerID);
    trigger_tree->Branch("TriggerCounter"      ,&TriggerCounter);
    trigger_tree->Branch("TriggerRealTime",&Trig_RealTime);
    trigger_tree->Branch("PlaneCode"  ,&PlaneCode);
    trigger_tree->Branch("PlaneNumber",&PlaneNumber);
    trigger_tree->Branch("BoardIP"    ,&Board_IP);
    trigger_tree->Branch("BoardID"    ,&Board_ID); 
    trigger_tree->Branch("TriggerTime",&Trig_Time);
    
    // Def EventNo. Identifier -> Faster way to read ROOT file 
    unsigned int entry_identifier[25] = {}; // In C++ Init. array this way will result in array[i] = 0 for all i

    char buff[31]; 
    string Board_name;
    HitData hitdata_buffer; 
    
    // Get MaxTriggerID from Master board
    snprintf(buff, sizeof(buff), "arm_%02d_phys_%02d_ip_Trigger.root", MasterBoardID, BoardID_config[MasterBoardID][0] ); 
    Board_name = buff; 
    uint64_t MaxTriggerID= GetMaxTriggerID(Run_number, Board_name, 1, "trigger");
    std::cout << "MaxTrig: " << MaxTriggerID << std::endl;

    // Load Chain 
    TChain* chain[25];  

    for (unsigned int loop_BoardID = 1; loop_BoardID <= n_board_max ; loop_BoardID++ )
    {
        if (BoardID_config[loop_BoardID][0] > 100 ) continue; // Skip deactivated boards
        snprintf(buff, sizeof(buff), "arm_%02d_phys_%02d_ip_Trigger.root", loop_BoardID, BoardID_config[loop_BoardID][0] ); 
        Board_name = buff ;
        string rootfile_tree = GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/"+Board_name ;
        chain[loop_BoardID] = new TChain("trigger");
        chain[loop_BoardID]->Add(rootfile_tree.c_str());
    }
    TriggerID = 1; 

    while (TriggerID <= MaxTriggerID)
    {
        //std::cout << "TriggerID: " << TriggerID << std::endl;
        // Step 3.0 Clear vector and struct
        PlaneCode.clear();
        PlaneNumber.clear();
        Board_IP.clear();
        Board_ID.clear();
        Trig_Time.clear();
        Trig_RealTime.clear();

        // Step 3.1 Loop over other boards with boardID ->GetHitData(Identifier, TriggerID)
        for (unsigned int loop_BoardID = 1; loop_BoardID <= n_board_max ; loop_BoardID++ )
        {
            if (BoardID_config[loop_BoardID][0] > 100 ) continue; // Skip deactivated boards
            hitdata_buffer = {}; //Reset values

            GetTriggerData(hitdata_buffer, chain[loop_BoardID], entry_identifier[loop_BoardID], TriggerID);
            entry_identifier[loop_BoardID] = hitdata_buffer.entry ; 

            // Merge into vector
            if (hitdata_buffer.collect_flag == 1)
            {
                assert(TriggerID == hitdata_buffer.TriggerID);
                TriggerCounter = hitdata_buffer.TriggerCounter;
                PlaneCode.insert( PlaneCode.end(), hitdata_buffer.PlaneCode.begin(), hitdata_buffer.PlaneCode.end());
                PlaneNumber.insert( PlaneNumber.end(), hitdata_buffer.PlaneNumber.begin(), hitdata_buffer.PlaneNumber.end());
                Board_IP.insert( Board_IP.end(), hitdata_buffer.Board_IP.begin(), hitdata_buffer.Board_IP.end());
                Board_ID.insert( Board_ID.end(), hitdata_buffer.Board_ID.begin(), hitdata_buffer.Board_ID.end());
                Trig_Time.insert( Trig_Time.end(), hitdata_buffer.Trig_Time.begin(), hitdata_buffer.Trig_Time.end());
                Trig_RealTime.insert( Trig_RealTime.end(), hitdata_buffer.Trig_RealTime.begin(), hitdata_buffer.Trig_RealTime.end());
            }
        }
        
        // Step 3.4 Fill the tree 
        trigger_tree->Fill();
        if (TriggerID % 1000 == 0 ) std::cout << "============================ Finish Event " << TriggerID << "===================" << std::endl;
        TriggerID++;
    }

    // Step 4 save
    trigger_file->cd();
    trigger_tree->Write();
    trigger_file->Close();

}

// Get Maximum trigger
uint64_t Reader::GetMaxTriggerID(string Run_number, string Board_name, bool Actual_triggerID, string TreeName)
{
    // Step 1 Read file
    string rootfile_tree = GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/"+Board_name ; 
    TFile *file_hitdata = TFile::Open(rootfile_tree.c_str());  

    // Step 2 Point to the last entry and return
	TTreeReader HitReader(TreeName.c_str(), file_hitdata);
	TTreeReaderValue<uint64_t> *TriggerID = new TTreeReaderValue<uint64_t>(HitReader,      "TriggerID"      );

    // the file need to be closed
    // if closed *TriggerID will disappear 
    if (Actual_triggerID) 
    {
	    HitReader.SetEntry(HitReader.GetEntries(0)-1);
        uint64_t triggerID = **TriggerID;
        std::cout << "Entries: " << triggerID << std::endl;
        file_hitdata->Close();
        delete file_hitdata;
        return triggerID; 
    } else
    {
        std::cout << rootfile_tree << std::endl;
        uint64_t MaxEntries = HitReader.GetEntries(0)-1;
        std::cout << "Entries: " << MaxEntries << std::endl;
        file_hitdata->Close();
        delete file_hitdata;
        return MaxEntries;
    }
}

// Get Hit data with TriggerID_main
void Reader::GetHitData(Reader::HitData &event_data, TChain* chain , unsigned int entry, unsigned int TriggerID_main, BuffData &buff_data)
{   

    // Step 1 Set read variables
    if (entry+1 >= chain->GetEntries()) 
    {
        event_data.entry = entry;
    }

    // Step 3 Prepare reading buffer
	chain->GetEntry(entry);    

	// Step 4 Loop TriggerID <= TriggerID_main
    //        Loop and fill struct HitData with the same TriggerID 
	while (buff_data.TriggerID <= TriggerID_main)
	{
		//if (TriggerID < TriggerID_main || !is_Valid_Time((double)Trigger_Time, (double)Hit_Time, (double)BoardID_config[Board_ID][1], time_window) )  
		if (buff_data.TriggerID < TriggerID_main)  
	    {   // This if first condition may be not needed. 
            // The algorithm is secured enough....  => Unit testing required.
            // chain->GetEntries(0) get # of entries 
            // Accessible only chain->GetEntries(0) - 1 == Entry starts at 0 
            // Also timing window cut is applied 
			entry++;
            if (entry+1 > chain->GetEntries()) break;
			chain->GetEntry(entry);
			continue;
		}

		// Only TriggerID == TriggerID_main reaches this point 
        event_data.PlaneCode.push_back(buff_data.PlaneCode);
        event_data.PlaneNumber.push_back(buff_data.PlaneNumber);
        event_data.Board_IP.push_back(buff_data.Board_IP);
        event_data.Board_ID.push_back(buff_data.Board_ID);
        event_data.STiC_ID.push_back(buff_data.STiC_ID);
        event_data.Ch_ID.push_back(buff_data.Ch_ID);
        event_data.Ch_Position.push_back(buff_data.Ch_Position);
        event_data.Amp.push_back(buff_data.Amp);
        event_data.Hit_Time.push_back(buff_data.Hit_Time);
        event_data.Fine_Time.push_back(buff_data.Fine_Time);
        event_data.Trig_Time.push_back(buff_data.Trigger_Time);
        event_data.Hit_RealTime.push_back(buff_data.Hit_RealTime);
        event_data.Trig_RealTime.push_back(buff_data.Trig_RealTime);

        event_data.TriggerID = buff_data.TriggerID;
        event_data.TriggerCounter = buff_data.TriggerCounter;

		entry++;
        if (entry+1 > chain->GetEntries(0)) break;
		chain->GetEntry(entry);
	}

    // Update the last read entry 
    event_data.entry = entry;
    
}


// Get Hit data with TriggerID_main
void Reader::GetTriggerData(Reader::HitData &hitdata_buffer, TChain* chain , unsigned int entry, unsigned int TriggerID_main)
{   
    // Step 1 Set read variables
    hitdata_buffer = {};
    hitdata_buffer.collect_flag = 0 ;
    if (entry+1 >= chain->GetEntries()) 
    {
        hitdata_buffer.entry = entry;
    }

    // Buff variables 
    uint64_t TriggerID; 
    uint64_t TriggerCounter; 
    uint32_t           PlaneCode; 
    uint32_t           PlaneNumber;
    uint32_t           Board_IP; 
    uint32_t           Board_ID; 
    uint64_t Trigger_Time;
    double             Trig_RealTime; 

    chain->SetBranchAddress("TriggerID"      , &TriggerID     );
    chain->SetBranchAddress("TriggerCounter" , &TriggerCounter  );
    chain->SetBranchAddress(  "PlaneCode"    , &PlaneCode     );
    chain->SetBranchAddress(  "PlaneNumber"  , &PlaneNumber   );
    chain->SetBranchAddress(   "BoardIP"     , &Board_IP      );
    chain->SetBranchAddress(   "BoardID"     , &Board_ID      );
    chain->SetBranchAddress(   "TriggerTime" , &Trigger_Time  );
    chain->SetBranchAddress("TriggerRealTime", &Trig_RealTime );


    // Step 3 Prepare reading buffer
	chain->GetEntry(entry);    


	// Step 4 Loop TriggerID <= TriggerID_main
    //        Loop and fill struct HitData with the same TriggerID 
	while (TriggerID <= TriggerID_main)
	{
		//if (TriggerID < TriggerID_main || !is_Valid_Time((double)Trigger_Time, (double)Hit_Time, (double)BoardID_config[Board_ID][1], time_window) )  
		if (TriggerID < TriggerID_main)  
	    {   // This if first condition may be not needed. 
            // The algorithm is secured enough....  => Unit testing required.
            // chain->GetEntries(0) get # of entries 
            // Accessible only chain->GetEntries(0) - 1 == Entry starts at 0 
            // Also timing window cut is applied 
			entry++;
            if (entry+1 > chain->GetEntries()) break;
			chain->GetEntry(entry);
			continue;
		}

		// Only TriggerID == TriggerID_main reaches this point 
        hitdata_buffer.collect_flag = 1 ;
        hitdata_buffer.PlaneCode.push_back(PlaneCode);
        hitdata_buffer.PlaneNumber.push_back(PlaneNumber);
        hitdata_buffer.Board_IP.push_back(Board_IP);
        hitdata_buffer.Board_ID.push_back(Board_ID);
        hitdata_buffer.Trig_Time.push_back(Trigger_Time);
        hitdata_buffer.Trig_RealTime.push_back(Trig_RealTime);

        hitdata_buffer.TriggerID = TriggerID;
        hitdata_buffer.TriggerCounter = TriggerCounter;

		entry++;
        if (entry+1 > chain->GetEntries(0)) break;
		chain->GetEntry(entry);
	}

    // Update the last read entry 
    hitdata_buffer.entry = entry;
    
}


/////////////////////////////////////////////////////////////
//////////////////// Read configuration /////////////////////
/////////////////////////////////////////////////////////////

// This function needs strucutrisation 
void Reader::ReadConfiguration(string CfgFilename)
{
    // Step 1 Read the config file
    
    std::ifstream file_cfg(CfgFilename);
    std::string line; 
    std::istringstream buffer;
    unsigned int flag = 0 ; // flag for reading separated part
    unsigned int n_planeNo = 0;
    unsigned int n_plane = 0; // Plane: 0 = X, 1 = Y, 2 = U, 3 = V

    std::array<int, 6> buffer_config;
    unsigned int buffer_idconfig;

    assert(file_cfg.is_open()); // Will raise an error if the file is not opened

    // Step 2 Assign to global variables - Need 2 steps reader
    //                                          1.) Det. Config
    //                                          2.) Constants 

    while (std::getline(file_cfg >> std::ws, line)){
        // Ignore lines containing # 
        if (line.find("Constants") != std::string::npos) break; // Constant will be read in the next loop
        if (line.find("#") != std::string::npos) continue;
    
        // Step 0. buffer a line to a string stream (ss) - For holding the line value 
        std::stringstream ss(line);
        std::string buffer_line; 

        // Step 1. read board config. - the 1st matrix -- Flag == 0
        // Update 17012019 - put to std::unordered_map = python-dict equivalent
        if (line.find("NumberBoard") != std::string::npos) 
        {   // Get n_board_max
            buffer.str(line.substr(line.find("=")+1)); // read value   
            buffer >> n_board_max;
            buffer.clear();
            continue;
        }

        if (line.find("NumberLayer") != std::string::npos) 
        {   // Get n_layer_max
            buffer.str(line.substr(line.find("=")+1)); // read value   
            buffer >> n_layer_max;
            buffer.clear();
            continue;
        }

        if (flag == 0){

            // Read BoardID
            std::getline(ss, buffer_line, ',');
            buffer.str(buffer_line);
            buffer >> buffer_idconfig;
            buffer.clear();
             
            //Read { IP   , Offset, IsEnable, Plane , PlaneNo , Layer }
            //     {  0       1       2        3        4       5     }
            for (unsigned int col = 0; col < buffer_config.size(); col++ )
            {   // col -> Nth Column in Configuration.txt file
                std::getline(ss, buffer_line, ',');
                buffer.str(buffer_line);
                buffer >> buffer_config[col]; 
                buffer.clear();
            }
             
            //Send to BoardID_config
            BoardID_config[buffer_idconfig] = buffer_config;

            //Mapping [IP] to ID 
            IP_GetID[buffer_config[0]] = buffer_idconfig; 

            if (buffer_idconfig == n_board_max) 
            {
                flag = 1 ; 
                continue;
            }
            continue;
        }
        
    
        // Step 2. read Beamspot position - the 2nd matrix -- Flag == 1
        // 17012019 - More efficient implementations are welcome -> This way is not good but it works...
        if (flag == 1) {
        

            std::getline(ss, buffer_line, ','); // 
            buffer.str(buffer_line);
            buffer >> n_plane;
            buffer.clear();

            std::getline(ss, buffer_line, ','); // 
            buffer.str(buffer_line);
            buffer >> n_planeNo;
            buffer.clear();

            std::getline(ss, buffer_line, ','); 
            buffer.str(buffer_line);
            buffer >> beamlowerlimit[n_plane][n_planeNo];
            buffer.clear();

            std::getline(ss, buffer_line, ','); 
            buffer.str(buffer_line);
            buffer >> beamupperlimit[n_plane][n_planeNo];
            buffer.clear();

            if ((n_plane+1)*n_planeNo == n_layer_max) flag = 2;
        }        

        // Step 3. Z position offset -- Flag == 2 
        if (line.find("zPositionOffset") != std::string::npos) {
            ss.str(line.substr(line.find("=")+1));
            for (unsigned int i_zpos = 0; i_zpos < n_layer_max ; i_zpos++){ // depend on no. layer 
                zpos.push_back(double ()); 
                std::getline(ss,buffer_line, ',');
                buffer.str(buffer_line);
                buffer >> zpos[i_zpos]; 
                buffer.clear();
            }
        }
            

    }

    // ===== Constants =====
    // The configuration file has a format -  "key = value"
    // ** find other iterable way to implement this - more brillient ideas are welcome - 23052019 - plan declear map of str + exec() command in cpp 
    // SciFi SiPM characteristic
    while (std::getline(file_cfg >> std::ws, line)){
        //line = line >> std::ws; // clear space and tab
        if (line.find("Constants") != std::string::npos) continue;
        buffer.str(line.substr(line.find("=")+1)); // read value   

        if (line.find("MasterBoardID") != std::string::npos) {
            buffer >> MasterBoardID; 
        }
        else if (line.find("gap_die") != std::string::npos) {
            buffer >> gap_die; 
        }
        else if (line.find("gap_SiPM") != std::string::npos) {
            buffer >> gap_SiPM; 
        } 
        else if (line.find("ch_width") != std::string::npos) {
            buffer >> ch_width; 
        } 
        else if (line.find("fibre_thickness") != std::string::npos) {
            buffer >> fibre_thickness; 
        } 
        else if (line.find("z_offset") != std::string::npos) {
            buffer >> z_offset ; 
        } 
        else if (line.find("radiants") != std::string::npos) {
            buffer >> radiants ; 
            radiants = radiants/180*TMath::Pi();
        } 
        else if (line.find("overlap") != std::string::npos) {
            buffer >>  overlap; 
        } 
        else if (line.find("time_window") != std::string::npos) {
            buffer >> time_window ; 
        } 
        else if (line.find("FPGABoard_period") != std::string::npos) {
            buffer >> FPGABoard_period ; 
            board_frequency = 1000./FPGABoard_period; // == 136MHz 
        } 
        else if (line.find("fine_bits") != std::string::npos) {
            buffer >> fine_bits ; 
        } 
        else if (line.find("enabletiming") != std::string::npos) {
            buffer >>  enabletiming; 
        } 
        else if (line.find("T_Fine") != std::string::npos) {
            buffer >>  T_Fine; 
        } 
        else if (line.find("T_Course") != std::string::npos) {
            buffer >>  T_Course; 
        } 
        else {
            std::cout << "Error! Do you add a new constant?" << std::endl; 
        }
        buffer.clear();
    }



}

/////////////////////////////////////////////////////////////
///////////////// END Read configuration ////////////////////
/////////////////////////////////////////////////////////////













