#include "../include/Event.hpp"


Event::Event() 
{

}

Event::~Event()
{
    return;
}

void Event::EventBuild(uint64_t number,std::vector<double> time, std::vector<uint64_t> trigger)
{
    InitEvent(number, time, trigger);
}

uint64_t Event::Geteventnumber(){
    if(eventnumber) return eventnumber;
    else {std::cout<<"not initialised event"<<std::endl; return -1;}
}

std::vector<double> Event::Gettimestamp(){
    if(timestamp.size()>0)return timestamp;
    else{std::cout<<"not initialised event"<<std::endl; return timestamp;}
}

std::vector<uint64_t> Event::Gettriggercounter(){
    if(triggercounter.size()>0)return triggercounter;
    else{std::cout<<"not initialised event"<<std::endl; return triggercounter;}
}

std::vector<std::vector<Layer>> Event::Getlayers_list(){
    if(layers_list.size()!=0) return layers_list;
    else {std::cout<<"not initialised event"<<std::endl; return layers_list;}
}

void Event::Seteventnumber(uint64_t number){
    eventnumber=number;
}
void Event::Settimestamp(std::vector<double> time){
    timestamp=time;
}
void Event::Settriggercounter(std::vector<uint64_t> trigger){
    triggercounter=trigger;
}
void Event::InitEvent(uint64_t number, std::vector<double> time, std::vector<uint64_t> trigger){
    Seteventnumber(number);
    Settimestamp(time);
    Settriggercounter(trigger);
    layers_list.clear();
}
void Event::AssignLayers(std::vector<std::vector<Layer>> layers){
    layers_list=layers;
}

