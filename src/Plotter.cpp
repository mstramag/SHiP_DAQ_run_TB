/* Plotter Program
 * Class for reading configuration file and STiC data
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 * 08.01.2019
 */

#include <iostream>
#include <sstream>
#include "../include/Plotter.hpp"
#include "../include/Track.hpp"
#include "../include/Alignment.hpp"
#include "../src/Alignment.cpp"
#include "../millepede/millepede.h"
#include "string.h"
#include "dirent.h"
#include "TROOT.h"
#include "TSystem.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TString.h"
#include "TPaveStats.h"
#include "TStyle.h"
#include <cmath>
#include <thread>         
#include <chrono> 
#include <climits>

/////////////////////////////////////////////////////////////
///////////////////// Init && GetSet ////////////////////////
/////////////////////////////////////////////////////////////

Plotter::Plotter()
{
}

/////////////////////////////////////////////////////////////
///////////////////// String manager ////////////////////////
/////////////////////////////////////////////////////////////

void Plotter::SetFileIn(string Run_number, int N_entries, bool time_window_flag, bool Reject_Saturated_flag, bool Force_create, bool Simulation, UInt_t ChOffsetBoard, TString ChFile, TString shift_run) 
{
    // Set constant
    run_number = Run_number;
    z_BinEdge  = reader.display_zPosition_BinEdge.data();
    z_Center   = reader.display_zLayerCenter.data();


    gSystem->Load(GetEnv("ANALYSIS_PATH")+(TString)"/millepede/libmillepede.so");
    // Run set Run_number
    m_Simulation = Simulation; 
    TString rootfile_tree = GetEnv("OUTPUTPATH")+"RUN_"+Run_number+"/";
    TString root_name = "Run_"+Run_number;
    if (time_window_flag) root_name += "_withtime";
    if (!Reject_Saturated_flag) root_name += "_withSatHit";
    rootfile_tree += root_name+".root" ;

    // Check whether the ROOT file exists or not
    ifstream f(rootfile_tree);
    if (!f.good() || Force_create) {
        std::cout << "ROOT file is not yet created.\n Creating" << std::endl;
        reader.CreateTree(Run_number, time_window_flag, Reject_Saturated_flag);    
    }

    TFile* file = new TFile(rootfile_tree);

    std::cout << "Reading: " << rootfile_tree << std::endl;
    data_tree = (TTree*) file->Get("data");   //!pointer to the analyzed TTree or TChain

    // Set branch address
    //data_tree->SetBranchAddress("Eventnum"      ,&Eventnum);
    if (!m_Simulation){
        data_tree->SetBranchAddress("TriggerID"      ,&TriggerID);
    } else {
        std::cout << "Read simulation file" << std::endl;
        data_tree->SetBranchAddress("TriggerID"      ,&sim_TriggerID); 
    }
    data_tree->SetBranchAddress("PlaneCode"  ,&PlaneCode);
    data_tree->SetBranchAddress("PlaneNumber",&PlaneNumber);
    data_tree->SetBranchAddress("BoardIP"    ,&Board_IP);
    data_tree->SetBranchAddress("BoardID"    ,&Board_ID); 
    data_tree->SetBranchAddress("STiCID"     ,&STiC_ID);
    data_tree->SetBranchAddress("ChID"       ,&Ch_ID);
    data_tree->SetBranchAddress("ChPosition" ,&Ch_Position);  
    data_tree->SetBranchAddress("TOT",&Amp);
    data_tree->SetBranchAddress("HitTime",&Hit_Time);
    data_tree->SetBranchAddress("FineTime",&Fine_Time);
    data_tree->SetBranchAddress("TriggerTime",&Trig_Time);
    data_tree->SetBranchAddress("HitRealTime",&Hit_RealTime);
    data_tree->SetBranchAddress("TriggerRealTime",&Trig_RealTime);

    if (N_entries >= data_tree->GetEntries() || N_entries == -1) {
        nentries = data_tree->GetEntries(); 
    } else {
        nentries = N_entries;
    }

    //  Read shift from file
    if (shift_run != "") {
        std::cout << "Read shift parameter from " << GetEnv("DATAPATH") + "RUN_" + shift_run  + "/shift_" + shift_run +".txt" << std::endl;
        reader.ReadShiftFromFile(GetEnv("DATAPATH") + "RUN_" + shift_run + "/shift_" + shift_run +".txt");
    }
    std::cout<<"store dead channels"<<std::endl;
    reader.SetDeadChannels();

    // ChOffset::ReserveChOffset vector 
    // each component corresponds to the time offset from the risingEdge
    // vector that checks if channel is ChTimeOffsetIsInvalid or not
    // ChTimeOffsetIsInvalid means the histogram of the ch has no entries or
    //Number 12 and 512 are hardcoded ! -> Need to improve in the future
    // total number of entries is < 50
    for (unsigned int i = 0; i < reader.n_board_max ; i++){
        std::vector<double> buffer(512, 0.0); 
        std::vector<bool> bufferInv(512, 0.0);
        ChTimeOffset.push_back(buffer); 
        ChTimeOffsetIsInvalid.push_back(bufferInv); 
    }

    // Read ChOffSet 
    if (ChFile != "") {
        if (ChOffsetBoard == 0) {
            if (ChFile == "RECREATE") {
                std::cout << "Compute ChOffset from risingEdge_channel_histo for all boards" << std::endl;
                CalcChannelOffsetsAll();
            } else if (ChFile == "READ") {
                ReadChOffset(); // default = 0 -> ReadAll 
            } else {std::cout << "Please put value only READ or RECREATE"<< std::endl;}

        } else if (ChOffsetBoard > 0 && ChOffsetBoard <= reader.n_board_max){ // may be wrong? 
            if (ChFile == "RECREATE") {
                std::cout << "Compute ChOffset from risingEdge_channel_histo for board "<< ChOffsetBoard << std::endl;
                CalcChannelOffsetsSingleBoard(ChOffsetBoard); 
            } else if (ChFile == "READ") {
                ReadChOffset(ChOffsetBoard); 
            } else {std::cout << "Please put value only READ or RECREATE"<< std::endl;}

        } else {
            std::cout << "Please put ChOffsetBoard between 1 and " << reader.n_board_max << " or -1 to read all board" << std::endl;  
        }
    }
}

/**********************************************************************************************************************************/
//ReadChOffset()/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/
void Plotter::ReadChOffset(UInt_t board)
{

    std::string path ;
    if (run_number.find("_0") != std::string::npos) {
        path = GetEnv("DATAPATH") + "RUN_" + run_number ;
    } else { 
        path = GetEnv("DATAPATH") + "RUN_" + run_number + "_0";
    }

    if (board != 0){
        // Read One file 
        std::string infilepath = path+"/ch_VCSEL_risingEdge_"+to_string(reader.BoardID_config[board][0])+".txt";
        std::cout << "Reading ChOffset from " << infilepath << std::endl;
        std::ifstream chdelay(infilepath);
        cout << infilepath << endl;
        std::string line;
        if (chdelay.is_open()){
            while(std::getline(chdelay, line)){    // read one line from chdelay (ch | Toffset | bool)
                std::stringstream streamline(line); // access line as a stream
                // we need the first two columns (chanel and offset)
                int column1;
                double column2;
                bool column3;
                streamline >> column1 >> column2 >> column3;
                ChTimeOffset[board-1][column1] = column2;
                ChTimeOffsetIsInvalid[board-1][column1] = column3;
            }
            std::cout << "Store off set in p.ChTimeOffset[" << board-1 << "][ChNumber]" << std::endl;
        }
        else {
            cout << "Unable to open the offset file" << endl;
        }
    } else {
        // Read All
        std::string infilepath[12];
        std::ifstream chdelay[12];

        for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
                infilepath[reader.layer_BoardID[ind_layType][ind_layNumber]-1] = path+"/ch_VCSEL_risingEdge_"+to_string(reader.layer_BoardID[ind_layType][ind_layNumber])+".txt";
                chdelay[reader.layer_BoardID[ind_layType][ind_layNumber]-1].open(infilepath[reader.layer_BoardID[ind_layType][ind_layNumber]-1].c_str());
                std::string line;
                if (chdelay[reader.layer_BoardID[ind_layType][ind_layNumber]-1].is_open()){
                    while(std::getline(chdelay[reader.layer_BoardID[ind_layType][ind_layNumber]-1], line)){    // read one line from chdelay (ch | Toffset | bool)
                        std::stringstream streamline(line); // access line as a stream
                        // we need the first two columns (chanel and offset)
                        int column1;
                        double column2;
                        bool column3;
                        streamline >> column1 >> column2 >> column3;
                        ChTimeOffset[reader.layer_BoardID[ind_layType][ind_layNumber]-1][column1] = column2;
                        ChTimeOffsetIsInvalid[reader.layer_BoardID[ind_layType][ind_layNumber]-1][column1] = column3;
                    }
                    std::cout << "Store offset in p.ChTimeOffset[" << reader.layer_BoardID[ind_layType][ind_layNumber]-1 << "][ChNumber]" << std::endl;
                }
                else {
                    cout << "Unable to open the offset file" << endl;
                }
            }
        }
    }

}

/**********************************************************************************************************************************/
//LoadtheEntry(uint64_t evnum)/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Loads the root file and provides the clustering for all the events////////////////////////////////////////////////////////////////
//All the following functions need the events to be virtually loaded and the clustering to be performed/////////////////////////////
/**********************************************************************************************************************************/
void Plotter::LoadtheEntry(uint64_t entry, TTree* chain, bool Build_Cluster)
{

    // Step 2 Set read variables
    // Load Event  => in vector forms

    chain->GetEntry(entry);    

    // Step 2 Define buffer 
    // Should init with 0

    // Hit 
    // Define array this way is nice for memory allocation
    // WIP Numbers 2 and 3 should depend on n_board_max
    unsigned int          buffer_hitsp[4][10][512]    = {0};    // array of amplitudes
    double                buffer_hitst[4][10][512]    = {0};    // array of full time info
    uint64_t              buffer_hits_c[4][10][512]   = {0};    // array of coarse time
    uint32_t              buffer_hits_f[4][10][512]   = {0};    // array of fine time
    unsigned int          buffer_sticids[4][10][512]  = {0};    // array of stidID
    unsigned int          buffer_boardids[4][10][512] = {0};    // array of boardID

    // Trigger
    std::vector<double>   buffer_Trigger_RealTime;
    std::vector<uint64_t> buffer_TriggerID;

    // Layer
    std::vector<std::vector<Layer>> layers;
    std::vector<Layer> buff_layers;

    // Step 3 Fill Trigger buffer to Events 

    for(unsigned int board= 0; board<reader.n_board_max ; board++)
    {
        if (!m_Simulation){
            buffer_TriggerID.push_back(TriggerID->at(board));
            if (TriggerID->at(board)!=ULONG_MAX){
                buffer_Trigger_RealTime.push_back((Trig_RealTime)->at(board));
            } else 
            {
                buffer_Trigger_RealTime.push_back(ULONG_MAX);
            }
        } else{
            if (board == Trig_RealTime->size() ) break;
            buffer_TriggerID.push_back(sim_TriggerID);
            buffer_Trigger_RealTime.push_back((Trig_RealTime)->at(board));
        }
    }

    // Set trigger  Time infomation in Event 
    Event* ev = new Event();
    ev->EventBuild(entry, buffer_Trigger_RealTime, buffer_TriggerID);

    // Step 4 Fill hit buffer  

    for(uint64_t hit_index = 0; hit_index < (Board_ID)->size(); hit_index++) // Loop only Existing hits and use BoardID to read BoardID_config
    { 
        // length -> should be all ChPosition
        unsigned int xy  = reader.BoardID_config[(Board_ID)->at(hit_index)][3];
        unsigned int lay = reader.BoardID_config[(Board_ID)->at(hit_index)][4]-1;// Layer 1 2 3 4
        unsigned int ch_position = (Ch_Position)->at(hit_index) ; 

        buffer_hitsp[xy][lay][ch_position]    = (Amp)->at(hit_index);
        buffer_hits_c[xy][lay][ch_position]   = (Hit_Time)->at(hit_index);
        buffer_hits_f[xy][lay][ch_position]   = (Fine_Time)->at(hit_index);
        buffer_hitst[xy][lay][ch_position]    = (Hit_RealTime)->at(hit_index);
        buffer_sticids[xy][lay][ch_position]  = (STiC_ID)->at(hit_index);
        buffer_boardids[xy][lay][ch_position] = (Board_ID)->at(hit_index);

    }


    // Step 5 Construct Layer ->  Build Cluster and Push

    // All layers is built
    // k == ind_layType
    // j == ind_layNumber
    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
        int k = -1;
        // Reverse loop up of MapLayerType
        for (std::pair<int, string> element: reader.MapLayerType){
            if (element.second == (reader.layer_name.at(ind_layType)[0])[0]) {k = element.first; break;} 
        }
        if (k == -1) {std::cout << "Not set: Error .." << std::endl; return ;}

        // Loop all layer
        for(unsigned int j=0; j < reader.layer_name.at(ind_layType).size() ; j++){
            Layer *layer = new Layer(reader.layer_name[k][j], reader.layer_Position[k][j] ) ; 
            layer->AssignHits(buffer_hitsp[k][j],buffer_hitst[k][j], buffer_hits_c[k][j], buffer_hits_f[k][j]);

            if (Build_Cluster)
            {
                // Apply clustering algorithm and assign the clusters to the layers
                std::vector<Cluster> clusters;
                for(int i=0;i<512;i++){
                    if(buffer_hitsp[k][j][i]>0){ //  TOT > 0 
                        // The first buffer_hit
                        Cluster clu(i,buffer_hitsp[k][j][i],buffer_hitst[k][j][i], buffer_sticids[k][j][i],buffer_boardids[k][j][i], buffer_hits_c[k][j][i], buffer_hits_f[k][j][i]);
                        for(int h=i+1;h<512;h++){
                            if((buffer_hitsp[k][j][h]>0 || reader.dead_channels[k][j][h]==1) && h-i==1){
                                clu.addChannelToTheRight(buffer_hitsp[k][j][h],buffer_hitst[k][j][h], buffer_sticids[k][j][h], buffer_boardids[k][j][h], buffer_hits_c[k][j][h], buffer_hits_f[k][j][h]); 
                                i=h;
                            }
                            else break;
                        }
                        clusters.push_back(clu);
                        clu.clear();
                    }
                }
                layer->AssignClusters(clusters);
                clusters.clear();
                //clusters.shrink_to_fit();
            }

            buff_layers.push_back(*layer); 
            delete layer;
        }
        // Should push to layer  
        layers.push_back(buff_layers);
        buff_layers.clear();
    }


    // Step 6 push to Event and clean buffer 
    ev->AssignLayers(layers);
    Events.push_back(*ev);
    delete ev;

}

/////////////////////////////////////////////////////////////
///////////////////// Plotting Part  ////////////////////////
/////////////////////////////////////////////////////////////

// Function to plot only after EventBuilder
void Plotter::PlotVariable(string Variable, TChain* chain)
{
    // Step 2 Define TH1D
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t[ind_layType][ind_layNumber]=new TH1D(reader.layer_name[ind_layType][ind_layNumber], reader.layer_name[ind_layType][ind_layNumber],512,-0.5,511.5);
            TString selection = "PlaneCode=="+std::to_string(ind_layType)+" && PlaneNumber == "+std::to_string(ind_layNumber+1);
            TString draw = Variable+">>"+reader.layer_name[ind_layType][ind_layNumber];
            chain->Draw(draw, selection , "goff");
        }
    }
}

/**********************************************************************************************************************************/
//*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/ README README  README  README  README */*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*
/**********************************************************************************************************************************/
//PlotExample()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Example how to write function in plotter - for consistancy between function //////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotExample(unsigned int cluster_min_size, TString lay){
    // If cluster is required -> setting "unsigned int cluster_min_size" may be needed
    // If only one  layer is need -> set lay == "X2" for example if you want to look at only X2 layer

    // Step 1 Get the maximum entries - Print anytime overall events 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist_general[0]= new TH1D("Example","Example",1000,0,1000);

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        // Load Entry ith from data_tree with cluster (set last bit to 1) or without (set last bit to 0)
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0); // Always at 0 - Load one by one 

        // Get Layer list
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");

        // Convention when do loop
        // ind_layType == index of layer type (number) == defined in (reader.layer_name)
        // ind_layNumber == index of layer number 
        // ind_cluNumber == index of cluster on a layer in question
        // p == position of a hit in a cluster 
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                // if (lay != reader.layer_name[ind_layType][ind_layNumber])continue; // This line may be added to choose specific layer defined in the function
                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();

                // Loop over all cluster
                if(clusters.size()>0){
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        if(clu.clusterLength()>cluster_min_size){
                            for(unsigned int p=0;p<clu.clusterLength();p++){
                                unsigned int ch=p+clu.startChannel();// ch == position on a board
                                //double TOT=(double)(clu.amplitudeAtChannel(ch));
                                //hist_general[0]->Fill(TOT);
                            }
                        }
                    }//clusters in the layer
                }
            }
        }//layers
    }//events

    // Step 4 Plotting 
    c = new TCanvas("c","Example",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Events");
    hist_general[0]->GetXaxis()->SetTitle("Example");
    hist_general[0]->Draw("hist");
    return c;
}
/**********************************************************************************************************************************/
//PlotBeamSpot()////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots all the hits recorded in every channel during the run///////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

// Change how we plot the beamspot -> should plot all layer
TCanvas * Plotter::PlotBeamSpot(){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Define TH1D
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t[ind_layType][ind_layNumber] = new TH1D(reader.layer_name[ind_layType][ind_layNumber], reader.layer_name[ind_layType][ind_layNumber],512,-0.5,511.5);
        }
    }

    // Step 3 Loop over all entries
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 0); // Load with no clustering
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();

        // each event display all layer
        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size());ind_layNumber++){ 
                Layer layer=(layers[ind_layType]).at(ind_layNumber);
                for(int ch_position = 0; ch_position<512 ; ch_position++){
                    if((layer.GetAmplitudes())[ch_position]>0) t[ind_layType][ind_layNumber]->Fill(ch_position);
                }
            }
        }
    }

    // Step 3 Plotting
    c = new TCanvas("c","Beam Spot",600,800);
    c->Divide(reader.layer_name.size(),reader.layer_name.at(0).size());

    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            c->cd(ind_layType+2*ind_layNumber+1);
            c->Update();
            t[ind_layType][ind_layNumber]->Draw("hist");
            c->Update();
            b0[ind_layType][ind_layNumber]=new TLine(512,0,512,gPad->GetUymax());
            b1[ind_layType][ind_layNumber]=new TLine(1024,0,1024,gPad->GetUymax());
            b0[ind_layType][ind_layNumber]->SetLineColor(kGreen);
            b1[ind_layType][ind_layNumber]->SetLineColor(kGreen);
            b0[ind_layType][ind_layNumber]->SetLineWidth(2);
            b1[ind_layType][ind_layNumber]->SetLineWidth(2);
            b0[ind_layType][ind_layNumber]->Draw("same");
            b1[ind_layType][ind_layNumber]->Draw("same");
        }
    }
    return c;
}



/**********************************************************************************************************************************/
//PlotEvent()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Event display of a single event///////////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotEvent(uint64_t evnum){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Load only one entry
    Events.clear();
    LoadtheEntry(evnum, data_tree, 0); // Load with no clustering
    Event ev=Events.at(0);

    std::vector<std::vector<Layer>> layers =ev.Getlayers_list();

    // Step 3 Loop through layers
    for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){
        for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size());ind_layNumber++){ 
            Layer layer=(layers[ind_layType]).at(ind_layNumber);

            t[ind_layType][ind_layNumber] = new TH1D(reader.layer_name[ind_layType][ind_layNumber], 
                    reader.layer_name[ind_layType][ind_layNumber],512,-0.5,511.5);
            for(int h=0;h<512;h++){
                if((layer.GetAmplitudes())[h]>0){ 
                    t[ind_layType][ind_layNumber]->Fill(h,(layer.GetAmplitudes())[h]);
                }

            }
        }
    }

    // Step 4 Draw amplitude and position of hits
    TString namecanvas="Event ";
    namecanvas+=evnum;
    c = new TCanvas("c",namecanvas,600,800);
    c->Divide(layers.size(), layers.at(0).size()); // Fix this 
    for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){
        std::vector<Layer> layerXY=layers[ind_layType];
        for (unsigned int ind_layNumber=0; ind_layNumber<layerXY.size(); ind_layNumber++ ){
            c->cd(ind_layType+2*ind_layNumber+1);// ind_layType+ind_layNumber+1 position  in canvas
            t[ind_layType][ind_layNumber]->Draw("hist");
            c->Update();
        }
    }
    return c;
}


/**********************************************************************************************************************************/
//PlotInnerTimeDiffSingleBoard()///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between the trigger time and each hit time of the event in the board under investigation////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotInnerTimeDiffSingleBoard(UInt_t board, bool LongMinusPlotRange, bool selected_tracks_flag){

    prettycolors();
    gStyle->SetOptStat(1111);

    // Step 1 Get the maximum entries 

    if (selected_tracks_flag == 1) { 
        DisplaySelectedTracks(-1, 0);
        nentries = selected_events.size();
    }
    std::cout << "No. Entries: " << nentries << std::endl;

    // Set 2D histogram binning info
    int t_nbins ;
    float t_min, t_max;
    if (!LongMinusPlotRange){
        t_min = -10;
        t_max = +10;
        t_nbins = 200;
    }
    else{
        t_min = -200;
        t_max = 0;
        t_nbins = 2000;
    }

    // Step 2 Set the plot
    // adjusting the size 
    hist2D_general[0]= new TH2D("timdiff","Timing offsets;time [ns];channels",t_nbins,t_min,t_max,512,-0.5,511.5);

    // Step 3 Loop all events
    
    for(int i=0;i<nentries;i++){
        Events.clear();
        if (selected_tracks_flag == 0) LoadtheEntry(i, data_tree, 0); // Load with with clustering
        else LoadtheEntry(selected_events.at(i), data_tree, 0); // Load with with clustering
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();

        if(times.at(board-1)!=ULONG_MAX){
            int m =(board-1)%3;
            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
            if (layers[reader.BoardID_config[board][3]].size() < 1) continue;
            Layer layer("");
            layer = ( layers[reader.BoardID_config[board][3]] ).at(reader.BoardID_config[board][4]-1);

            for(int ch=0; ch<512; ch++){
                if((layer.GetAmplitudes())[ch]>0)
                {
                    double Time=(layer.GetTimes())[ch];
                    double TrigTimeShift=times.at(board-1);
                    double ti =-TrigTimeShift+Time;
                    if(selected_tracks_flag==0) hist2D_general[0]->Fill(ti,ch);
                    else{
                        if((layer.GetAmplitudes())[ch]<80)
                            hist2D_general[0]->Fill(ti,ch);
                    }
                }
            }
        }
    }

    // Step 4 Plotting
    c = new TCanvas("c","Timing offsets",800,600);
    c->cd();
    hist2D_general[0]->Draw("colz");
    return c;
}

/**********************************************************************************************************************************/
// PlotInnerTimeDiffAll ///////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotInnerTimeDiffAll(bool LongMinusPlotRange, bool BoardCalibration, bool ChCalibration, bool selected_tracks_flag){

    // Case selected events
    if (selected_tracks_flag == 1) { 
        DisplaySelectedTracks(-1, 0);
        nentries = selected_events.size();
    }

    // Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;


    // Set 2D histogram binning info
    int t_nbins ;
    float t_min, t_max;
    if (!LongMinusPlotRange){
        t_min = -10;
        t_max = +10;
        t_nbins = 200;
    }
    else{
        t_min = -500;
        t_max = 0;
        t_nbins = 500;
    }

    // Define TH2F
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t2[ind_layType][ind_layNumber] = new TH2D(reader.layer_name[ind_layType][ind_layNumber], reader.layer_name[ind_layType][ind_layNumber],t_nbins,t_min,t_max,512,-0.5,511.5);
        }
    }


    // Loop all events or selected events
    for(int i=0;i<nentries;i++){

        Events.clear();
        // if all events
        if (selected_tracks_flag == 0) LoadtheEntry(i, data_tree, 0);
        // if selected events
        else LoadtheEntry(selected_events.at(i), data_tree, 0);

        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();
        for (unsigned int ind_layType = 0; ind_layType < (layers.size()); ind_layType++) {
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if(times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1) !=ULONG_MAX){
                    if (layers[reader.BoardID_config[reader.layer_BoardID[ind_layType][ind_layNumber]][3]].size() < 1) continue;
                    Layer layer("");
                    layer = (layers[ind_layType]).at(ind_layNumber);           

                    for(int ch=0;ch<512;ch++){
                        if((layer.GetAmplitudes())[ch]>0)
                        {
                            double Time=(layer.GetTimes())[ch];
                            double TrigTimeShift=times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1);
                            double TimeOffset;
                            TimeOffset = ChTimeOffset[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch];
                            if(ChTimeOffsetIsInvalid[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch]==1) TimeOffset = 0.;
                            int board = reader.layer_BoardID[ind_layType][ind_layNumber];
                            double ti = Time - TrigTimeShift; 
                            if (ChCalibration) {
                                ti = ti - TimeOffset ;
                            } 
                            
                            if (BoardCalibration){
                                ti = ti - reader.BoardID_config[board][1];
                            }
                            t2[ind_layType][ind_layNumber]->Fill(ti,ch);
                        }
                    } // loop over chanel
                } //if condition
            }
        }
    }// end loop over event

    // Plotting
    c = new TCanvas("c","Beam Spot",800,1000);
    c->Divide(2,6);
    prettycolors();

    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            int fame ;
            if (reader.layer_BoardID[ind_layType][ind_layNumber]%2 == 0) {
                fame = reader.layer_BoardID[ind_layType][ind_layNumber]-1;
            } else {
                fame = reader.layer_BoardID[ind_layType][ind_layNumber]+1;
            }
            c->cd(fame);
            c->Update();
            t2[ind_layType][ind_layNumber]->Draw("colz");
            c->Update();
        }
    }

    return c;
}

/**********************************************************************************************************************************/
//PlotFineTime()///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the fine time in a 2D histogram for all channels////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotFineTime(UInt_t board){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;


    // Step 2 Set the plot
    // adjusting the size 
    hist2D_general[0]= new TH2D("FineTime","FineTime",32,0,32,512,-0.5,511.5);

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 0); 
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();

        if(times.at(board-1)!=ULONG_MAX){
            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
            if (layers[reader.BoardID_config[board][3]].size() < 1) continue;
            Layer layer("");
            layer = ( layers[reader.BoardID_config[board][3]] ).at(reader.BoardID_config[board][4]-1);

            for(int channel=0;channel<512;channel++){
                if((layer.GetAmplitudes())[channel]>0)
                {
                    double FineTime=(double)(layer.GetFineTimes())[channel];
                    hist2D_general[0]->Fill(FineTime,channel);
                }
            }
        }
    }

    // Step 4 Plotting
    c = new TCanvas("c","FineTime",800,600);
    c->cd();
    hist2D_general[0]->Draw("colz");
    return c;
}



/**********************************************************************************************************************************/
//PlotBoardsTimeDiff()//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between each trigger time in board 8 (master) and the board under investigation/////////////////////////
/**********************************************************************************************************************************/

// Need checking
TCanvas * Plotter::PlotBoardsTimeDiff(UInt_t board){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;


    // Step 2 Set the histogram
    hist_general[0]= new TH1D("timdiff","Timing offsets",32000,-100000,100000.);

    // Step 3 Loop over all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 0); // Load with with clustering
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();
        std::vector<uint64_t> trigid= ev.Gettriggercounter();

        if(times.at(board-1)!=ULONG_MAX && times.at(1)!=ULONG_MAX){

            double ti;
            if(times.at(board-1)>times.at(1)){ti=(double)(times.at(board-1)-times.at(1));}
            else {ti=-((double)(times.at(1)-times.at(board-1))); }

            hist_general[0]->Fill(ti);
            if(fabs(ti)>100)std::cout<<i<<" "<<times.at(board-1)<<" "<<times.at(1)<<std::endl;

        }
    }

    // Step 4 Display
    c = new TCanvas("c","Timing offsets",800,600);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Entries/6.25[ns]");
    hist_general[0]->GetXaxis()->SetTitle("time difference [ns]");
    hist_general[0]->Draw("hist");
    std::cout << "Timeing offsets: "<<hist_general[0]->GetMean()<<std::endl;
    return c;
}


/**********************************************************************************************************************************/
//PlotInClusterTOT()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the amplitude of the hits belonging to a cluster////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotInClusterTOT(unsigned int cluster_min_size){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist_general[0]= new TH1D("ClusterTOT","ClusterTOT",250,0,250);

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");

        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();

                // Loop over all cluster
                if(clusters.size()>0){
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        if(clu.clusterLength()>cluster_min_size){
                            for(unsigned int p=0;p<clu.clusterLength();p++){
                                unsigned int ch=p+clu.startChannel();
                                double TOT=(double)(clu.amplitudeAtChannel(ch));
                                hist_general[0]->Fill(TOT);
                            }
                        }
                    }//clusters in the layer
                }
            }
        }//layers
    }//events

    // Step 4 Plotting 
    c = new TCanvas("c","Cluster TOT",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Events");
    hist_general[0]->GetXaxis()->SetTitle("Cluster TOT");
    hist_general[0]->Draw("hist");
    return c;
}

/**********************************************************************************************************************************/
//PlotTOT()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the amplitude of the hits /////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotTOT(TString lay){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist2D_general[0]= new TH2D("TOT","TOT per channel",256,0,256,512,0.5,512.5);

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");

        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if (lay != reader.layer_name[ind_layType][ind_layNumber])continue;
                layer=(layers[ind_layType]).at(ind_layNumber);           
                for(int ch=0;ch<512;ch++){
                    if((layer.GetAmplitudes())[ch]>0)
                    {
                        double Tot=layer.GetAmplitudes()[ch];
                        hist2D_general[0]->Fill(Tot,ch);
                    }
                }

            }
        }//layers
    }//events

    // Step 4 Plotting 
    c = new TCanvas("c","Cluster TOT",800,500);
    c->cd();
    hist2D_general[0]->GetYaxis()->SetTitle("channel");
    hist2D_general[0]->GetXaxis()->SetTitle("TOT");
    hist2D_general[0]->Draw("colz");
    return c;
}
/**********************************************************************************************************************************/
//PlotTOTAll()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plot TOT for all boards /////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotTOTAll(){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t[ind_layType][ind_layNumber]=new TH2D(reader.layer_name[ind_layType][ind_layNumber],reader.layer_name[ind_layType][ind_layNumber],256,0,256,512,-0.5,511.5);
        }
    }

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");

        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                layer=(layers[ind_layType]).at(ind_layNumber);           
                for(int ch=0;ch<512;ch++){
                    if((layer.GetAmplitudes())[ch]>0)
                    {
                        double Tot=layer.GetAmplitudes()[ch];
                        t[ind_layType][ind_layNumber]->Fill(Tot,ch);
                    }
                }
            }
        }//layers
    }//events

    // Step 4 Plotting 
    c = new TCanvas("c","Cluster TOT",800,1200);
    c->Divide(2,6);
    c->SetTitle("TOT per channel");
    //for(int board=1;board<=12;board++){
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            c->cd(ind_layType+2*ind_layNumber+1);
            t[ind_layType][ind_layNumber]->Draw("colz");
            
            c->Update();
        }
    }
    return c;
}

/**********************************************************************************************************************************/
//PlotClusterSize()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots a histogram with the cluster size (number of hits per cluster/////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotClusterSize(){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist_general[0]= new TH1D("ClusterSize","ClusterSize;Cluster size;Events",10,-0.5,9.5);

    // Step 3 Loop over all Events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i  , data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();

        Layer layer("");
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
          for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();
                if(clusters.size()>0){
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        if(clu.clusterLength()>0){ 
                            hist_general[0]->Fill(clu.clusterLength());
                        }
                    }//clusters in the layer
                }
            }
        }//layers

    }//events

    // Step 4 Plottong
    c = new TCanvas("c","Cluster TOT",800,500);
    c->cd();
    hist_general[0]->Draw("hist");
    return c;
}


/**********************************************************************************************************************************/
//PlotClusterBeamSpot()/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots all the clusters recorded in every layer during the run////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotClusterBeamSpot(){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t[ind_layType][ind_layNumber]=new TH1D(reader.layer_name[ind_layType][ind_layNumber],reader.layer_name[ind_layType][ind_layNumber],512,-0.5,511.5);
        }
    }

    // Step 3 Loop over all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();

        //Loop layer 
        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size());ind_layNumber++){ // Loop over layer -ind_layNumber can be 0 to 3
                Layer layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();

                // Loop clusters
                if(clusters.size()>0){
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        if( clu.clusterLength()>1 && clu.clusterLength()<5){
                            int max=clu.clusterLength()-1+clu.startChannel();
                            for(int p=clu.startChannel(); p<max; p++){
                                t[ind_layType][ind_layNumber]->Fill(p);
                            }
                        }
                    }
                }
            }
        }
    }

    // Step 4 Report
    c = new TCanvas("c","Cluster Beam Spot",600,800);
    c->Divide(reader.layer_name.size(),reader.layer_name.at(0).size());

    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            c->cd(ind_layType+2*ind_layNumber+1);
            c->Update();
            t[ind_layType][ind_layNumber]->Draw("hist");
            c->Update();
            b0[ind_layType][ind_layNumber]=new TLine(512,0,512,gPad->GetUymax());
            b1[ind_layType][ind_layNumber]=new TLine(1024,0,1024,gPad->GetUymax());
            b0[ind_layType][ind_layNumber]->SetLineColor(kGreen);
            b1[ind_layType][ind_layNumber]->SetLineColor(kGreen);
            b0[ind_layType][ind_layNumber]->SetLineWidth(2);
            b1[ind_layType][ind_layNumber]->SetLineWidth(2);
            b0[ind_layType][ind_layNumber]->Draw("same");
            b1[ind_layType][ind_layNumber]->Draw("same");
        }
    }
    return c;

}

/**********************************************************************************************************************************/
//PlotClusterTOT()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the total amplitude of the clusters/////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotClusterTOT(unsigned int cluster_min_size){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist_general[0]= new TH1D("ClusterTOT","ClusterTOT",250,0,250);

    // Step 3 Loop over all Events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i  , data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();

        Layer layer("");
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();
                if(clusters.size()>0){
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        if(clu.clusterLength()>cluster_min_size){ 
                            hist_general[0]->Fill((double)(clu.totalAmplitude()));
                        }
                    }//clusters in the layer
                }
            }
        }//layers

    }//events

    // Step 4 Plottong
    c = new TCanvas("c","Cluster TOT",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Events");
    hist_general[0]->GetXaxis()->SetTitle("Cluster TOT");
    hist_general[0]->Draw("hist");
    return c;
}


/**********************************************************************************************************************************/
//PlotHitEfficiency()///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Checks if, for every cluster in the reference plane, there is at least one hit in the plane under investigation///////////////////
//The search is made in the beamspot areas, considering the beamspot shift and the overlap of two fibres////////////////////////////
/**********************************************************************************************************************************/
//Need checking
TCanvas * Plotter::PlotHitEfficiency(TString lay){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 define nec. variables
    hist_general[0]= new TH1I("hiteffhist","Hit efficiency histogram",512,-0.5,511.5);
    int counter=0;
    int totcounter=0;
    int misscount=0;

    // Step 3 Loop over all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();

        std::vector<double> times =ev.Gettimestamp();
        Layer layer("");
        Layer partnerlayer("");
        int plane=-1;
        int lp=-1; // layer in the plane
        int partnerlp=-1;


        // Loop layers
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<(layers[ind_layType]).size(); ind_layNumber++){
                if (lay == reader.layer_name[ind_layType][ind_layNumber]){
                    plane=ind_layType;
                    lp=ind_layNumber; 
                    partnerlp = ind_layNumber+(ind_layNumber%2)*(-1)+(1-(ind_layNumber%2));
                    layer = (layers[ind_layType]).at(ind_layNumber);
                    partnerlayer = (layers[ind_layType]).at(partnerlp);
                }
            }
        }

        // Cluster
        std::vector<Cluster> clusters =partnerlayer.Getclusters_list();
        if(clusters.size()>0){
            for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                Cluster clu=clusters.at(ind_cluNumber);

                bool found=false;
                if(clu.clusterLength() >1){
                    int clpos=(int)(clu.channelPosition());
                    int board=(int)(clu.GetBoardID((unsigned int)clpos));
                    if(clpos<reader.beamupperlimit[plane][partnerlp] && clpos>reader.beamlowerlimit[plane][partnerlp]){
                        totcounter++;

                        int othermean=(reader.beamupperlimit[plane][partnerlp]-reader.beamlowerlimit[plane][partnerlp])/2+
                            reader.beamlowerlimit[plane][partnerlp];
                        int innermean=(reader.beamupperlimit[plane][lp]-reader.beamlowerlimit[plane][lp])/2+reader.beamlowerlimit[plane][lp];
                        int shiftch=othermean-innermean;
                        int underlimit=clpos-reader.overlap-shiftch;
                        int upperlimit=clpos+reader.overlap-shiftch;
                        if((clpos-reader.overlap)<0)underlimit=0;
                        if((clpos+reader.overlap)>511)upperlimit=511;

                        for(int p=underlimit;p<upperlimit;p++){
                            if((layer.GetAmplitudes())[p]>0){
                                found=true;
                                hist_general[0]->Fill(p);
                            }

                        }

                        if(!found){
                            bool mis=false;
                            for(int p=underlimit;p<upperlimit;p++){
                                if(times.at(board)==ULONG_MAX){mis=true;}
                            }
                            if(mis)misscount++;
                        }
                        if(found)counter++;
                        found=false;
                    }
                }
            }
        }
    }

    // Step 4 Report -- Need checking
    if(totcounter ==0) std::cout<<"reference layer dead"<<std::endl;
    double hit_eff = (double)totcounter - (double)misscount ;
    hit_eff = ((double)counter)/hit_eff ;
    std::cout<<"the hit effciency is "<< hit_eff <<std::endl;
    std::cout<<"the  counts are "<<counter<<std::endl;
    std::cout<<"the missed counts are "<<misscount<<std::endl;
    std::cout<<"total hits considered "<<totcounter<<std::endl;
    c = new TCanvas("c","hefficiency",800,600);
    c->cd();
    hist_general[0]->Draw("hist");
    return c;

}

/**********************************************************************************************************************************/
//PlotTrigTrigTime()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between one trigger and the following in the board under investigation//////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotTrigTrigTime(UInt_t board){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Def. histogram
    hist_general[0]= new TH1D("timdiff","Triggers distance",20000,0.,20000000.);

    // Step 3 Loop all events 
    for(int i=1;i<nentries;i++){
        std::cout<<" Event "<<i<<std::endl;
        Events.clear();

        LoadtheEntry(i  , data_tree, 0);
        Event e=Events.at(0);
        LoadtheEntry(i-1, data_tree, 0);
        Event e1=Events.at(1);

        std::vector<double> times =e.Gettimestamp();
        std::vector<double> times1 =e1.Gettimestamp();
        std::cout<<"trigger time: "<<times.at(board-1)<<std::endl;
        std::cout<<"trigger time1: "<<times1.at(board-1)<<std::endl;


        if(times.at(board-1)!=ULONG_MAX && times1.at(board-1)!=ULONG_MAX){

            double ti;
            ti=(double)(times.at(board-1)-times1.at(board-1));

            hist_general[0]->Fill(ti);
        }
    }

    // Step 4 Plotting 
    c=new TCanvas("c","Timing offsets",800,600);
    c->cd();
    c->SetLogx();
    hist_general[0]->GetYaxis()->SetTitle("Entries/[us]");
    hist_general[0]->GetXaxis()->SetTitle("time difference [ns]");
    hist_general[0]->Draw("hist");
    return c;
}


/**********************************************************************************************************************************/
//PlotHitHitTime()//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between the first and the second hit arriving inside clusters of dimension >1///////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotHitHitTime(TString lay){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Def. histogram
    hist_general[0]= new TH1D("hittohit","Hit to hit time",200,-4600,4600);
    hist_general[1]= new TH1D("hittohit1","Hit to hit time if the first channel is on the right",200,-4600,4600);
    hist_general[2]= new TH1D("hittohit2","Hit to hit time if the first channel is on the left",200,-4600,4600);
    hist_general[3]= new TH1D("hittohit3","Hit to hit time same stic",200,-4600,4600);
    hist_general[4]= new TH1D("hittohit4","Hit to hit time same board",200,-4600,4600);
    hist_general[5]= new TH1D("hittohit5","Hit to hit time diff board",200,-4600,4600);

    // Step 3 Loop events 
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i  , data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");

        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if (lay == reader.layer_name[ind_layType][ind_layNumber]){
                    layer = (layers[ind_layType]).at(ind_layNumber);
                }
            }
        }

        std::vector<Cluster> clusters = layer.Getclusters_list();

        for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
            double min=ULONG_MAX;
            double min2=ULONG_MAX;
            int first=1600;
            int second=1600;
            unsigned int Stic1=1000;
            unsigned int Stic2=1000;
            unsigned int board1=1000;
            unsigned int board2=1000;
            Cluster clu=clusters.at(ind_cluNumber);
            if(clu.clusterLength()>1){
                for(unsigned int p=0;p<clu.clusterLength();p++){
                    unsigned int ch=p+clu.startChannel();
                    if(clu.timeAtChannel(ch)<=min){
                        min2=min;
                        min=clu.timeAtChannel(ch);
                        second=first;
                        first=ch;
                        Stic2=Stic1;
                        board2=board1;
                        Stic1=clu.GetSTiCID(ch);
                        board1=clu.GetBoardID(ch);
                    }
                    else if(clu.timeAtChannel(ch)<=min2){
                        min2=clu.timeAtChannel(ch);
                        second=ch;
                        Stic2=clu.GetSTiCID(ch);
                    }
                }
                if(min2!=ULONG_MAX){
                    double diff = ULONG_MAX;
                    if(first>second)diff = min2-min;
                    else diff =min-min2;

                    if(first>second)hist_general[1]->Fill(diff);
                    else hist_general[2]->Fill(diff);
                    if(Stic1==Stic2 && board1==board2)hist_general[3]->Fill(diff);
                    if(board1==board2)hist_general[4]->Fill(diff);
                    else hist_general[5]->Fill(diff);
                    hist_general[0]->Fill(diff);
                }
            }
        }
    }

    // Step 4 Plotting 
    c = new TCanvas("c","HitHitTime",800,500);
    c->Divide(3,2);
    c->cd(1);
    hist_general[0]->GetYaxis()->SetTitle("Entries/46[ps]");
    hist_general[0]->GetXaxis()->SetTitle("time difference [ps]");
    hist_general[0]->Draw("hist");
    c->cd(2);
    hist_general[1]->Draw("hist");
    c->cd(3);
    hist_general[2]->Draw("hist");
    c->cd(4);
    hist_general[3]->Draw("hist");
    c->cd(5);
    hist_general[4]->Draw("hist");
    c->cd(6);
    hist_general[5]->Draw("hist");
    return c;
}


/**********************************************************************************************************************************/
//PlotChHitTime()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the channel VS hit time distribution////////////////////////////////////////////////////////////////////////////////////////
//This function is meant to see if the channels in the midde of the beam spot have as well the first hits///////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotChHitTime(TString lay){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Def. histogram
    hist2D_general[0]= new TH2D("chtime","Ch vs Hit to hit time",512,-0.5,511.5,40,0,200);

    // Step 3 Loop over all events
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if (lay == reader.layer_name[ind_layType][ind_layNumber]){
                    layer = (layers[ind_layType]).at(ind_layNumber);
                }
            }
        }
        std::vector<Cluster> clusters =layer.Getclusters_list();

        for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
            uint64_t min=ULLONG_MAX;
            unsigned int channelone=1537;
            Cluster clu=clusters.at(ind_cluNumber);
            for(unsigned int p=0;p<clu.clusterLength();p++){
                unsigned int ch=p+clu.startChannel();
                if(clu.timeAtChannel(ch)<=min){
                    min=clu.timeAtChannel(ch);
                    channelone=ch;
                }
            }
            for(unsigned int p=0;p<clu.clusterLength();p++){
                unsigned int ch2=p+clu.startChannel();
                if(ch2!=channelone){
                    double diff = clu.timeAtChannel(ch2)-min;

                    hist2D_general[0]->Fill(ch2,diff);
                }
            }
        }
    }

    // Step 4 Plotting 
    c = new TCanvas("c","Inefficiency",800,500);
    c->cd();
    hist2D_general[0]->GetYaxis()->SetTitle("time difference [ps]");
    hist2D_general[0]->GetXaxis()->SetTitle("Channel number");
    hist2D_general[0]->Draw("colz");
    return c;
}




/**********************************************************************************************************************************/
//PlotTOTHitTime()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the amplitude VS hit time distribution//////////////////////////////////////////////////////////////////////////////////////
//This function is meant to see if there is a clear correlation between the amplitude of the signal and its time of arrival/////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotTOTHitTime(TString lay){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Def. histogram
    hist2D_general[0]= new TH2D("TOTtime","TOT vs Hit to hit time",512,-256,256,100,0,4600);

    // Step 3 Loop event
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i  , data_tree, 1);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if (lay == reader.layer_name[ind_layType][ind_layNumber]){
                    layer = (layers[ind_layType]).at(ind_layNumber);
                }
            }
        }
        std::vector<Cluster> clusters =layer.Getclusters_list();

        for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
            double min=ULONG_MAX;
            unsigned int channelone=1537;
            Cluster clu=clusters.at(ind_cluNumber);
            int Stic1=1000;
            int Stic2=1000;
            int first=1600;
            int second=1600;

            for(unsigned int p=0;p<clu.clusterLength();p++){
                unsigned int ch=p+clu.startChannel();
                if(clu.timeAtChannel(ch)<=min){
                    min=clu.timeAtChannel(ch);
                    channelone=ch;
                    first=ch;
                    Stic1=clu.GetSTiCID(channelone);
                }
            }
            for(unsigned int p=0;p<clu.clusterLength();p++){
                unsigned int ch2=p+clu.startChannel(); 
                if(ch2!=channelone){
                    double diff = clu.timeAtChannel(ch2)-min;
                    int tot;
                    tot=(int)clu.amplitudeAtChannel(channelone)-(int)clu.amplitudeAtChannel(ch2);
                    Stic2=clu.GetSTiCID(ch2);
                    second=ch2;
                    if(Stic1==Stic2 && fabs(first-second)<512)hist2D_general[0]->Fill(tot,diff);
                }
            }
        }
    }

    // Step 4 Plotting
    c = new TCanvas("c","Inefficiency",800,500);
    c->cd();
    hist2D_general[0]->GetYaxis()->SetTitle("time difference [ps]");
    hist2D_general[0]->GetXaxis()->SetTitle("TOT difference");
    hist2D_general[0]->Draw("colz");
    return c;
}

/**********************************************************************************************************************************/
// CalcChannelOffsetsSingleBoard(UInt_t board) /////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/
void Plotter::CalcChannelOffsetsSingleBoard(UInt_t board){

    // Step 1 Get the maximum entries
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the plot of the InnerTimeDiff histo
    int t_nbins = 1000;
    float t_min = -57.5;
    float t_max = +57.5;
    hist2D_general[0]= new TH2D("timdiff","Timing offsets",t_nbins,t_min,t_max,512,-0.5,511.5);
    hist2D_general[0]->Reset("ICESM");

    float unit = t_nbins/(t_max - t_min);

    // Step 3 define a vector ChTimeOffset
    // Move to Plotter::ChTimeOffset 

    // Step 4 Loop all events
    for(int i=0;i<nentries;i++){

        Events.clear();
        LoadtheEntry(i, data_tree, 0);
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();
        if(times.at(board-1)!=ULONG_MAX){
            std::vector<std::vector<Layer>> layers = ev.Getlayers_list();

            if (layers[reader.BoardID_config[board][3]].size() < 1) continue;
            Layer layer("");
            layer = ( layers[reader.BoardID_config[board][3]] ).at(reader.BoardID_config[board][4]-1);

            for(int ch=0;ch<512;ch++){
                if((layer.GetAmplitudes())[ch]>0)
                {
                    double Time=(layer.GetTimes())[ch];
                    double TrigTimeShift=times.at(board-1);
                    double ti =Time-TrigTimeShift;
                    hist2D_general[0]->Fill(ti,ch);
                }
            } // loop over chanel
        } //if condition
    } // loop over entries

    // Step5 Create a directory ch_delay_offset/run_number
    std::string path = GetEnv("DATAPATH") + "RUN_" + run_number;

    // Step6 file where we store the time profile for each channel together with the risingEdge line
    string nameoftheTFile = path + "/risingEdge_channel_histo_"+to_string(reader.BoardID_config[board][0])+".root";
    const char * title = nameoftheTFile.c_str();
    TFile f(title,"recreate");

    // Step7 create a canvas to show the time profile for each channel
    //TCanvas* c[512];

    // Step8 Loop over the channels
    for(int ch=0;ch<512;ch++){

        //c[ch] = new TCanvas();
        string name="histo_chanel"+to_string(ch);
        const char * cname = name.c_str();

        hist_general[0] = hist2D_general[0]->ProjectionX("single channel",ch+1,ch+1);
        TH1F *hnew = (TH1F*)hist_general[0]->Clone();
        hnew->SetName(cname);

        //c[ch]->SetTitle(cname);
        //c[ch]->SetName(cname);

        std::string ytitle = "Counts/" + to_string_with_precision(unit,2) + " ns^{-1}";
        hnew->GetYaxis()->SetTitle(ytitle.c_str());
        hnew->GetXaxis()->SetTitle("hitTime - triggerTime (ns)");
        hnew->SetName(cname);
        f.cd();
        hnew->Write();

        // Get the risingEdge time and saving into the vector
        double time_risingEdge = FindRisingEdge(hnew);
        ChTimeOffset[board-1][ch] = time_risingEdge;

        std::string message1 = "\033[1;34m ATT: Zero entries at channel " + to_string(ch) + " (STiC " + to_string(ch_to_sticch(ch).first) + " channel " + to_string(ch_to_sticch(ch).second) + ")\033[0m";
        std::string message2 = "\033[1;33m ATT: Maximum value < 10 (maybe PLL is not locked) at channel " + to_string(ch) + " (STiC " + to_string(ch_to_sticch(ch).first) + " channel " + to_string(ch_to_sticch(ch).second) + ")\033[0m";
        std::string message3 = "\033[1;32m ATT: Entries < 50 at channel " + to_string(ch) + " (STiC " + to_string(ch_to_sticch(ch).first) + " channel " + to_string(ch_to_sticch(ch).second) + ")\033[0m";
        if(hnew->GetEntries()==0){
            std::cout << message1 << std::endl;
            ChTimeOffsetIsInvalid[board-1][ch]=1;
        }
        else if(hnew->GetMaximum() < 10 && hnew->GetEntries()!=0){
            std::cout << message2 << std::endl;
            ChTimeOffsetIsInvalid[board-1][ch]=1;
        }
        else if(hnew->GetEntries() < 50){
            std::cout << message3 << std::endl;
            ChTimeOffsetIsInvalid[board-1][ch]=1;
        }

        hist_general[0]->Reset();
    }

    f.Close();


    // Step9 Writing the data in a txt file
    std::string outfilename;
    outfilename = path + "/ch_VCSEL_risingEdge_"+to_string(reader.BoardID_config[board][0])+".txt";
    std::ofstream output(outfilename); // board 08 is named 8 !
    for(int ch=0;ch<512;ch++){

        // 1. Normal case 
        if (abs(ChTimeOffset[board-1].at(ch)) < 10){
            output << left << setw(15) << ch;
            output << left << setw(15) << setprecision(4) << ChTimeOffset[board-1].at(ch);
            output << left << setw(15) << ChTimeOffsetIsInvalid[board-1].at(ch) << "\n";
        } else {
        
            // 2. Add average 
            std::pair <int,int> SearchSTiCAndChannel = ch_to_sticch(ch) ;
        
            // Find left channel 
            int neighbour_channel_1 = -1; 
            for (int i = (int) 0; i < SearchSTiCAndChannel.second ; i++ ) {
                int ch_left = ch - 2*i; 
                if (abs(ChTimeOffset[board-1].at(ch_left)) < 10 ) {
                    neighbour_channel_1 = ch_left; 
                    break;                 
                }
            
            }

            // Find right channel 
            int neighbour_channel_2 = -1; 
            for (int i = (int) 0; i < (64 - SearchSTiCAndChannel.second) ; i++ ) {
                int ch_right = ch - 2*i; 
                if (abs(ChTimeOffset[board-1].at(ch_right)) < 10 ) {
                    neighbour_channel_2 = ch_right; 
                    break;                 
                }
            
            }

            // Last case - if cannot find any in either left or right  -> use either what we have 
            if (neighbour_channel_1 == -1 && neighbour_channel_2 != -1 ) neighbour_channel_1 = neighbour_channel_2; 
            if (neighbour_channel_2 == -1 && neighbour_channel_1 != -1 ) neighbour_channel_2 = neighbour_channel_1; 

            double mean_neighbour = (ChTimeOffset[board-1].at(neighbour_channel_1) + ChTimeOffset[board-1].at(neighbour_channel_2))/2.; 

            
            ChTimeOffset[board-1][ch] = mean_neighbour;
            output << left << setw(15) << ch;
            output << left << setw(15) << setprecision(4) << ChTimeOffset[board-1].at(ch);
            output << left << setw(15) << ChTimeOffsetIsInvalid[board-1].at(ch) << "\n";
            
        
        } 
    }


    cout << "Channel offsets for run " << run_number << " is in file: " << outfilename << endl; 
    cout << "Assign those offsets in ChTimeOffset[" << board-1 << "][ChNumber]. To use, please call p.ChTimeOffset["<< board-1 <<"][ChNumber]" << std::endl;
    output.close();

}

/**********************************************************************************************************************************/
// CalcChannelOffsetsAll() /////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/
void Plotter::CalcChannelOffsetsAll(){

    // Step 1 Get the maximum entries
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the plot of the InnerTimeDiff histo
    int t_nbins = 1000;
    float t_min = -57.5*0.5;
    float t_max = +57.5*0.5;

    // Step 2 Define TH2F
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t2[ind_layType][ind_layNumber] = new TH2D(reader.layer_name[ind_layType][ind_layNumber], reader.layer_name[ind_layType][ind_layNumber],t_nbins,t_min,t_max,512,-0.5,511.5);
        }
    }

    float unit = t_nbins/(t_max - t_min);

    // Step 3 define a vector ChTimeOffset
    // Move to Plotter::ChTimeOffset

    //Step 4 Loop all events
    for(int i=0;i<nentries;i++){

        Events.clear();
        LoadtheEntry(i, data_tree, 0);
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();
        for (unsigned int ind_layType = 0; ind_layType < (layers.size()); ind_layType++) {
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if(times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1) !=ULONG_MAX){
                    if (layers[reader.BoardID_config[reader.layer_BoardID[ind_layType][ind_layNumber]][3]].size() < 1) continue;
                    Layer layer("");
                    layer = (layers[ind_layType]).at(ind_layNumber);           
                    for(int ch=0;ch<512;ch++){
                        if((layer.GetAmplitudes())[ch]>0)
                        {
                            double Time=(layer.GetTimes())[ch];
                            double TrigTimeShift=times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1);
                            double ti =Time-TrigTimeShift;
                            //if((layer.GetAmplitudes())[ch]<100)
                            t2[ind_layType][ind_layNumber]->Fill(ti,ch);
                        }
                    } // loop over chanel
                } //if condition
            }
        }
    } // loop over entries

    // Step5 Create a directory ch_delay_offset/run_number
    std::string path = GetEnv("DATAPATH") + "RUN_" + run_number;

    // Step6 file where we store the time profile for each channel together with the risingEdge line
    string nameoftheTFile[12];
    std::ofstream log;

    TFile *f[12];

    // Step8 Loop over the channels
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){

            nameoftheTFile[reader.layer_BoardID[ind_layType][ind_layNumber]-1] = path + "/risingEdge_channel_histo_"+to_string(reader.layer_BoardID[ind_layType][ind_layNumber])+".root";
            const char * title = nameoftheTFile[reader.layer_BoardID[ind_layType][ind_layNumber]-1].c_str();

            std::string outfilename = path + "/log_ch_VCSEL_risingEdge_"+to_string(reader.layer_BoardID[ind_layType][ind_layNumber])+".txt";
            log.open(outfilename.c_str());
            f[reader.layer_BoardID[ind_layType][ind_layNumber]-1] = new TFile(title,"recreate");

            for(int ch=0;ch<512;ch++){

                string name="histo_chanel"+to_string(ch);
                const char * cname = name.c_str();
                t[ind_layType][ind_layNumber] = t2[ind_layType][ind_layNumber]->ProjectionX("single channel",ch+1,ch+1);

                TH1F *hnew = (TH1F*)t[ind_layType][ind_layNumber]->Clone();
                hnew->SetName(cname);

                std::string ytitle = "Counts/" + to_string_with_precision(unit,2) + " ns^{-1}";
                hnew->GetYaxis()->SetTitle(ytitle.c_str());
                hnew->GetXaxis()->SetTitle("hitTime - triggerTime (ns)");
                hnew->SetName(cname);
                f[reader.layer_BoardID[ind_layType][ind_layNumber]-1]->cd();
                hnew->Write();

                // Get the risingEdge time and saving into the vector
                double time_risingEdge = FindRisingEdge(hnew);
                ChTimeOffset[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch] = time_risingEdge;

                std::string message1 = "\033[1;34m ATT: Zero entries at channel " + to_string(ch) + " (STiC " + to_string(ch_to_sticch(ch).first) + " channel " + to_string(ch_to_sticch(ch).second) + ")\033[0m";
                std::string message2 = "\033[1;33m ATT: Maximum value < 10 (maybe PLL is not locked) at channel " + to_string(ch) + " (STiC " + to_string(ch_to_sticch(ch).first) + " channel " + to_string(ch_to_sticch(ch).second) +  ")\033[0m";
                std::string message3 = "\033[1;32m ATT: Entries < 50 at channel " + to_string(ch) + " (STiC " + to_string(ch_to_sticch(ch).first) + " channel " + to_string(ch_to_sticch(ch).second) + ")\033[0m";
                if(hnew->GetEntries()==0){
                    log << message1 << std::endl;
                    ChTimeOffsetIsInvalid[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch]=1;
                }
                else if(hnew->GetMaximum() < 10 && hnew->GetEntries()!=0){
                    log << message2 << std::endl;
                    ChTimeOffsetIsInvalid[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch]=1;
                }
                else if(hnew->GetEntries() < 50){
                    log << message3 << std::endl;
                    ChTimeOffsetIsInvalid[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch]=1;
                }



                hnew->Reset();
            }



            f[reader.layer_BoardID[ind_layType][ind_layNumber]-1]->Close();
            log.close();
        }
    }

    // Step9 Writing the data in a txt file
    std::ofstream output; // board 08 is named 8 !
    //outfilename = path + "/ch_VCSEL_risingEdge_"+to_string(reader.BoardID_config[board][0])+".txt";
    //std::ofstream output(outfilename); // board 08 is named 8 !

    std::string outfilename[12];
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            int board = reader.layer_BoardID[ind_layType][ind_layNumber];
            outfilename[board-1] = path + "/ch_VCSEL_risingEdge_"+to_string(reader.layer_BoardID[ind_layType][ind_layNumber])+".txt";
            output.open(outfilename[board-1].c_str());

            bool error_flag = false;
            for(int ch=0;ch<512;ch++){
                // 1. Normal case 
                if (abs(ChTimeOffset[board-1].at(ch)) < 10){
                    output << left << setw(15) << ch;
                    output << left << setw(15) << setprecision(4) << ChTimeOffset[board-1].at(ch);
                    output << left << setw(15) << ChTimeOffsetIsInvalid[board-1].at(ch) << "\n";
                } else {
                
                    // 2. Add average 
                    std::pair <int,int> SearchSTiCAndChannel = ch_to_sticch(ch) ;
                
                    // Find left channel 
                    int neighbour_channel_1 = -1; 
                    for (int i = (int) 0; i < SearchSTiCAndChannel.second ; i++ ) {
                        int ch_left = ch - 2*i; 
			            //std::cout << "Ch: " << ch << " Ch_left: " << ch_left <<std::endl; 
			            if (ch_left < 0 || ch_left >= 512) break;
                        if (abs(ChTimeOffset[board-1].at(ch_left)) < 10 ) {
                            neighbour_channel_1 = ch_left; 
                            break;                 
                        }
                    
                    }

                    // Find right channel 
                    int neighbour_channel_2 = -1; 
                    for (int i = (int) 0; i < (64 - SearchSTiCAndChannel.second) ; i++ ) {
                        int ch_right = ch + 2*i; 
			            if (ch_right < 0 || ch_right >= 512) break;
			            //std::cout << "Ch: " << ch << " Ch_right: " << ch_right <<std::endl; 
                        if (abs(ChTimeOffset[board-1].at(ch_right)) < 10 ) {
                            neighbour_channel_2 = ch_right; 
                            break;                 
                        }
                    
                    }

		            //std::cout << "Pass the search for Ch: "<< ch << std::endl;

                    // Last case - if cannot find any in either left or right  -> use either what we have 
                    if (neighbour_channel_1 == -1 && neighbour_channel_2 != -1 ) neighbour_channel_1 = neighbour_channel_2; 
                    if (neighbour_channel_2 == -1 && neighbour_channel_1 != -1 ) neighbour_channel_2 = neighbour_channel_1; 

                    //std::cout << "Ch: " << ch << " neighbour_channel_1 " << neighbour_channel_1 << " neighbour_channel_2 " << neighbour_channel_2 << std::endl; 
                    if (neighbour_channel_1 == -1 && neighbour_channel_2 == -1) {
                        if (!error_flag) error_flag = true;
                        ChTimeOffset[board-1][ch] = 0;
                        continue;
                    }
                    double mean_neighbour = (ChTimeOffset[board-1].at(neighbour_channel_1) + ChTimeOffset[board-1].at(neighbour_channel_2))/2.; 
                    ChTimeOffset[board-1][ch] = mean_neighbour;
                    output << left << setw(15) << ch;
                    output << left << setw(15) << setprecision(4) << ChTimeOffset[board-1].at(ch);
                    output << left << setw(15) << ChTimeOffsetIsInvalid[board-1].at(ch) << "\n";
                    
                
                } 
            }
            if (error_flag){
                std::cout<< "\033[1;7;31m =*=*=*=*=*= RUN " << run_number <<" Board "<< board << " is not configured properly =*=*=*=*=*= \033[0m"<< std::endl; 
                std::cout<< "\033[1;7;31m For now set all Channel in this Board to zero \033[0m" << std::endl;
                std::cout<< "\033[1;7;31m Please copy the offset file ch_VCSEL_risingEdge from other runs \033[0m" << std::endl;
            }
            output.close();
        }
    }


    // Report
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            int board = reader.layer_BoardID[ind_layType][ind_layNumber];
            cout << "Channel offsets for run " << run_number << " is in file: " << outfilename[board-1] << endl; 
            cout << "Assign those offsets in ChTimeOffset[" << board-1 << "][ChNumber] . To use, please call p.ChTimeOffset["<< board-1 <<"][ChNumber]" << std::endl;
        }
    }

}
/**********************************************************************************************************************************/
// PlotInnerTimeDiffCorrectedSingleBoard ///////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotInnerTimeDiffCorrectedSingleBoard(UInt_t board, bool LongMinusPlotRange, bool injection_corr, bool BoardCalibration, bool ChCalibration, bool Avg_ChOffset, bool selected_tracks_flag){

    if (selected_tracks_flag == 1) { 
        DisplaySelectedTracks(-1, 0);
        nentries = selected_events.size();
    }

    // Setp 0 Apply (or not)  propagation correction 
    double linear_coeff;
    if(injection_corr==1) linear_coeff = 774.242;
    else linear_coeff=0.;

    // Step 1 Get the maximum entries 
    //std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 load the Toffset vector form ch_delay_offset/ch_delay_results_#BoardIP.txt 
    int t_nbins ;
    float t_min, t_max;
    if (!LongMinusPlotRange){
        t_min = -10;
        t_max = +10;
        t_nbins = 200;
    }
    else{
        t_min = -3500;
        t_max = 1500;
        t_nbins = 10000;
    }

    // Step3 Set path to directory ch_delay_offset/runNumber

    // Step 4 Set the 2D histogram
    hist2D_general[0]= new TH2D("timdiff","Timing offsets corrected (VCSEL)",t_nbins,t_min,t_max,512,-0.5,511.5);

    // Step 5 Loop all events
    for(int i=0;i<nentries;i++){

        Events.clear();
        if (selected_tracks_flag == 0) LoadtheEntry(i, data_tree, 0);
        else LoadtheEntry(selected_events.at(i), data_tree, 1); 
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();
        if(times.at(board-1)!=ULONG_MAX){
            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
            if (layers[reader.BoardID_config[board][3]].size() < 1) continue;
            Layer layer("");
            layer = ( layers[reader.BoardID_config[board][3]] ).at(reader.BoardID_config[board][4]-1);

            for(int ch=0;ch<512;ch++){
                if((layer.GetAmplitudes())[ch]>0)
                {
                    double Time=(layer.GetTimes())[ch];
                    double TrigTimeShift=times.at(board-1);
                    double TimeOffset;
                    TimeOffset = ChTimeOffset[board-1][ch];
                    if(ChTimeOffsetIsInvalid[board-1][ch]==1 && !Avg_ChOffset ) TimeOffset = 0.;
                    double ti = Time - TrigTimeShift; 
                    if (ChCalibration) {
                        ti = ti - TimeOffset ;
                    } 
                    
                    if (BoardCalibration){
                        ti = ti - reader.BoardID_config[board][1];
                    }
                    double ti_corr;
                    if(injection_corr==1) ti_corr = ti + ch/linear_coeff;
                    else ti_corr = ti;
                    hist2D_general[0]->Fill(ti_corr,ch);
                }
            } // loop over chanel
        } //if condition
    }// end loop over event

    // Step6 Create a directory ch_delay_offset/run_number
    std::string plot_path = "Plots/RUN_" + run_number;
    std::string cmd1 = "mkdir -p Plots";
    std::string cmd2 = "mkdir -p " + plot_path;
    int systemcmd1 = system(cmd1.c_str());
    int systemcmd2 = system(cmd2.c_str());

    if(systemcmd1 == -1){
        cout << "cmd: " << cmd1 << " FAILED." << endl;
    }
    if(systemcmd2 == -1){
        cout << "cmd: " << cmd2 << " FAILED." << endl;
    }

    //Step7 Plotting
    c = new TCanvas();
    prettycolors();
    hist2D_general[0]->Draw("colz");
    return c;
}

/**********************************************************************************************************************************/
// PlotInnerTimeDiffCorrectedAll ///////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotInnerTimeDiffCorrectedAll(bool LongMinusPlotRange, bool injection_corr, bool BoardCalibration, bool ChCalibration, bool Avg_ChOffset, bool selected_tracks_flag){


    // Apply (or not)  propagation correction 
    double linear_coeff;
    if(injection_corr==1) linear_coeff = 774.242;
    else linear_coeff=0.;

    // Case selected events
    if (selected_tracks_flag == 1) { 
        DisplaySelectedTracks(-1, 0);
        nentries = selected_events.size();
    }

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Define TH2F
    int t_nbins ;
    float t_min, t_max;
    if (!LongMinusPlotRange){
        t_min = -10;
        t_max = +10;
        t_nbins = 200;
    }
    else{
        t_min = -500;
        t_max = 0;
        t_nbins = 1000;
    }

    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t2[ind_layType][ind_layNumber] = new TH2D(reader.layer_name[ind_layType][ind_layNumber], reader.layer_name[ind_layType][ind_layNumber],t_nbins,t_min,t_max,512,-0.5,511.5);
        }
    }

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){

        Events.clear();
        if (selected_tracks_flag == 0) LoadtheEntry(i, data_tree, 0);
        else LoadtheEntry(selected_events.at(i), data_tree, 0);

        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();
        for (unsigned int ind_layType = 0; ind_layType < (layers.size()); ind_layType++) {
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if(times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1) !=ULONG_MAX){
                    if (layers[reader.BoardID_config[reader.layer_BoardID[ind_layType][ind_layNumber]][3]].size() < 1) continue;
                    Layer layer("");
                    layer = (layers[ind_layType]).at(ind_layNumber);           

                    for(int ch=0;ch<512;ch++){
                        if((layer.GetAmplitudes())[ch]>0)
                        {
                            double Time=(layer.GetTimes())[ch];
                            double TrigTimeShift=times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1);
                            double TimeOffset;
                            TimeOffset = ChTimeOffset[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch];
                            if(ChTimeOffsetIsInvalid[reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch]==1 && !Avg_ChOffset) TimeOffset = 0.;
                            int board = reader.layer_BoardID[ind_layType][ind_layNumber];
                            double ti = Time - TrigTimeShift; 
                            if (ChCalibration) {
                                ti = ti - TimeOffset ;
                            } 
                            
                            if (BoardCalibration){
                                ti = ti - reader.BoardID_config[board][1];
                            }
                            double ti_corr;
                            if(injection_corr==1) ti_corr = ti + ch/linear_coeff;
                            else ti_corr = ti;
                            t2[ind_layType][ind_layNumber]->Fill(ti_corr,ch);
                        }
                    } // loop over chanel
                } //if condition
            }
        }
    }// end loop over event

    // Step 3 Plotting
    c = new TCanvas("c","Beam Spot",800,1000);
    c->Divide(2,6);
    prettycolors();

    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            int fame ;
            if (reader.layer_BoardID[ind_layType][ind_layNumber]%2 == 0) {
                fame = reader.layer_BoardID[ind_layType][ind_layNumber]-1;
            } else {
                fame = reader.layer_BoardID[ind_layType][ind_layNumber]+1;
            }
            c->cd(fame);
            c->Update();
            t2[ind_layType][ind_layNumber]->Draw("colz");
            c->Update();
        }
    }

    return c;
}

/**********************************************************************************************************************************/
//PlotCTRCluster()//////////////////////////////////////////////////////////////////////////////////////////////////
//Plot the time diff between the 2 channels of a same cluster that receive the maximum signal amplitude.
//Amplitude_cut -> minimum signal amplitude considered;  offset=1 to use time offsets for the channels 
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotCTRCluster(TString lay, TString runNbr, double Amplitude_cut, bool offset)
{	
    //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    //Path to the directory containing calibration data (offsets)
    TString path = GetEnv("DATAPATH");

    //Def hist
    TH1F * t;
    TString name=  "time difference";
    TString title= "CTR Cluster";
    t= new TH1F(name,title,150,-10,10);

    //Def var. for the Cluster
    unsigned int minLength = 1; // need at least Cluster of size 2 to compute a Delta t
    unsigned int maxLength = 9;
    unsigned int maxAmp = 257;
    double limit = 80000.;


    // Find board given the layer
    int board = 0;

    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            if (lay != reader.layer_name[ind_layType][ind_layNumber])continue;
            else board = reader.layer_BoardID[ind_layType][ind_layNumber];
        }
    }

    cout << "Board Number = " << board << endl;

    double toffset1=0, toffset2=0;

    //Loop over events
    for(int i=0;i<nentries;i++){

        Events.clear();
        LoadtheEntry(i  , data_tree, 1);
        Event e=Events.at(0);

        std::vector<std::vector<Layer>> layers =e.Getlayers_list();
        Layer layer("");
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if (lay == reader.layer_name[ind_layType][ind_layNumber]){
                    layer = (layers[ind_layType]).at(ind_layNumber);
                }
            }
        }

        //Select clusters in the layer with the right conditions
        std::vector<Cluster> clusters = SelectClusters(layer.Getclusters_list() ,limit,minLength,maxLength, maxAmp);

        //loop over clusters of the event
        for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){

            Cluster clu=clusters.at(ind_cluNumber);

            //If clusterlenght ==2, fill the time diff
            if (clu.clusterLength() == 2) {
                //First channel
                int chanel1=clu.startChannel();
                double tmin1 = clu.timeAtChannel(chanel1);
                if(offset==1) toffset1 = ChTimeOffset[board-1][chanel1];
                else toffset1 = 0;
                double Amplitude1 = clu.amplitudeAtChannel(chanel1);

                int chanel2=1+clu.startChannel();
                double tmin2 = clu.timeAtChannel(chanel2);
                if(offset==1) toffset2 = ChTimeOffset[board-1][chanel2];
                else toffset2 = 0;
                double Amplitude2 = clu.amplitudeAtChannel(chanel2);

                if(Amplitude1> Amplitude_cut && Amplitude2 > Amplitude_cut){
                    double time = (tmin1-tmin2) - toffset1 + toffset2;
                    //double time = (tmin1-tmin2);
                    t->Fill(time);
                }
            }

            //If clusterlenght =!2, find the max and 2nd max amplitude and theire respective chanel index
            else {

                //Find the maximum amplitude and its chanel index
                double max_amp = 0;
                unsigned int chanel_index_max = 0;
                for(unsigned int p=0;p<clu.clusterLength() ;p++){
                    unsigned int ch=p+clu.startChannel();
                    if (clu.amplitudeAtChannel(ch)>max_amp){
                        max_amp=clu.amplitudeAtChannel(ch);
                        chanel_index_max=ch;
                    }
                }

                //Find the 2nd max amplitude and its chanel index				
                max_amp = 0;
                unsigned int chanel_index_2ndmax =0;  
                for(unsigned int p=0;p<clu.clusterLength() ;p++){
                    unsigned int ch=p+clu.startChannel();
                    if (ch==chanel_index_max){}
                    else{
                        if (clu.amplitudeAtChannel(ch)>max_amp){
                            max_amp=clu.amplitudeAtChannel(ch);
                            chanel_index_2ndmax=ch;
                        }
                    }
                }
                if((abs(ChTimeOffset[board-1][chanel_index_max])<20)&&(abs(ChTimeOffset[board-1][chanel_index_2ndmax])<20)) {
                    // fill the difference
                    if(clu.amplitudeAtChannel(chanel_index_2ndmax)> Amplitude_cut){
                        double timediff=0;
                        if(offset==1) timediff = (clu.timeAtChannel(chanel_index_max)-clu.timeAtChannel(chanel_index_2ndmax)) -ChTimeOffset[board-1][chanel_index_max]+ChTimeOffset[board-1][chanel_index_2ndmax];
                        else timediff = (clu.timeAtChannel(chanel_index_max)-clu.timeAtChannel(chanel_index_2ndmax));
                        t->Fill(timediff);
                    }
                }
                /*
                // cout to display some infos on the cluster for cluster diff de 2
                cout << " Cluster de taille " << clu.clusterLength() << endl;
                for(unsigned int p=0;p<clu.clusterLength() ;p++){
                unsigned int ch=p+clu.startChannel();
                cout << "Chanel: " << ch << "   amp: " << clu.amplitudeAtChannel(ch);
                cout <<"   time: " << clu.timeAtChanel(ch) << endl;
                }
                cout << "The max amp and the second max amplitude are Chanel " << chanel_index_max << " and " << chanel_index_2ndmax << endl;
                */

            }
        }//end of loop over clusters of the event
    }//end of loop over events

    //Draw histogram
    c= new TCanvas("c","canvas",600,800);
    c->Divide(1,1);	
    t->GetYaxis()->SetTitle("Entries [1/ns]");
    t->GetXaxis()->SetTitle("time difference [ns]");
    t->Draw("hist");

    //Fit 		
    TF1 *f1= new TF1("na","gaus",-10,10);				
    f1->SetParameter(1,0);
    f1->SetParameter(2,1000);
    t->Fit("na", "RQ+", "", -10, 10);						
    f1->Draw("SAME");

    c->Update();

    return c;
}

/**********************************************************************************************************************************/
//PlotInnerTimeDiffCorrected()///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between the trigger time and each hit time of the event in the board under investigation
//Taking into accont the offset of each chanel in the file h_delay_offset/ch_delay_results_#BoardIP.txt
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotLASERInnerTimeDiffCorrected(UInt_t board){

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the plot
    hist2D_general[0]= new TH2D("timdiff","Timing offsets corrected (LASER)",200,-10,10,512,-0.5,511.5);

    // Step 3 Loop all events
    for(int i=0;i<nentries;i++){

        Events.clear();
        LoadtheEntry(i, data_tree, 0);
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();
        if(times.at(board-1)!=ULONG_MAX){
            int m =(board-1)%3;

            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
            if (layers[reader.BoardID_config[board][3]].size() < 1) continue;
            Layer layer("");
            layer = ( layers[reader.BoardID_config[board][3]] ).at(reader.BoardID_config[board][4]-1);

            for(int ind_layType=m*512;ind_layType<(m+1)*512;ind_layType++){
                if((layer.GetAmplitudes())[ind_layType]>0)
                {
                    double Time=(layer.GetTimes())[ind_layType];
                    double TrigTimeShift=times.at(board-1);
                    double ti = Time - TrigTimeShift - reader.BoardID_config[board][1] - ChTimeOffset[board-1][ind_layType];
                    hist2D_general[0]->Fill(ti,ind_layType);
                }
            } // loop over chanel
        } //if condition
    }// end loop over event

    // Step 5 Create a directory 
    std::string plot_path = "Plots/LASER";
    std::string cmd1 = "mkdir -p Plots";
    std::string cmd2 = "mkdir -p " + plot_path;
    int systemcmd1 = system(cmd1.c_str());
    int systemcmd2 = system(cmd2.c_str());

    if(systemcmd1 == -1){
        cout << "cmd: " << cmd1 << " FAILED." << endl;
    }
    if(systemcmd2 == -1){
        cout << "cmd: " << cmd2 << " FAILED." << endl;
    }

    // Step 6 Plotting
    c = new TCanvas();
    TPad *pad1 = new TPad("pad1","",0,0,1,1);
    TPad *pad2 = new TPad("pad2","",0,0,1,1);
    pad2->SetFillColor(0);
    pad2->SetFillStyle(0);
    pad2->SetFrameFillStyle(0);
    pad1->Draw();
    pad1->cd();
    c->Divide(1,1);
    c->cd(1);
    hist2D_general[0]->Draw("colz");
    pad2->Draw();
    pad2->cd();
    hist2D_general[0]->Draw("colzX+"); // plot axis on top
    c->Update();
    TPaveStats *st = (TPaveStats*)hist2D_general[0]->GetListOfFunctions()->FindObject("stats");
    st->SetX1NDC(0.7); //new x start position
    st->SetX2NDC(0.9); //new x end position
    st->SetY1NDC(0.75); //new y start position
    st->SetY2NDC(0.9);  //new y end position
    std::string c_name = plot_path + "/InnerTimeDiffCorrected_Board" + to_string(reader.BoardID_config[board][0]) + ".pdf";
    c->Print(c_name.c_str());
    return c;
}

/**********************************************************************************************************************************/
//PlotResidualSingle()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

  TCanvas * Plotter::PlotResidualSingle(TString lay, bool with_aligned){

	ResidualMean.clear();
	ResidualSigma.clear();

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist_general[0]= new TH1D("Residual","Residual", 200, -1, 1);
	TF1 *f1 = new TF1("gaus","gaus",-0.5,0.5);

    // Set plane 
    int layType_UnderTest = 0;
    if (lay[0] == (TString) "X"){
        layType_UnderTest = 0;
    }else if (lay[0] == (TString) "Y"){
        layType_UnderTest = 1;
    } else {std::cout << "Not yet implemented"; return NULL;}

    // Step 3 Get residual
    // Step 3.1 Setup zpos 
    std::vector<double> zpos ;
    double measured_hit_zpos = -1;
    for (unsigned int i = 0; 2*i < reader.n_layer_max; i++) {
        if (lay == reader.layer_name[layType_UnderTest][i]){
            // Not used ?? -> afraid to take this out ...
            measured_hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1];
            continue;
        }
        zpos.push_back(reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1]);
    }
    if (measured_hit_zpos < 0 ) {std::cout << "Invalid input"; return NULL;}
    
    // Step 3.2 Loop over all event
    for(int i=0;i<nentries;i++){
        // Setup
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        // Def var
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        std::vector<Cluster> clusters;
        Track* track = new Track(); 

		//Get position from the plane in question
		//Variables for finding the right cluster in DUT from mean value of other planes
		double mean_track = 0;  // mean value of found clusters before reconstructing track
		int number_cluster = 0; // number of planes where 1 cluster is found
		double measured_position=-1; // measured_position is position of cluster in DUT
                
        for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
            //if (ind_layNumber != layNumber_UnderTest){
            if (reader.layer_name[layType_UnderTest][ind_layNumber] != lay){
                      
                layer = (layers[layType_UnderTest]).at(ind_layNumber); 
                clusters = layer.Getclusters_list();

                if (clusters.size()==0) break;
                if (clusters.size()==1){
    			    Cluster clu=clusters.at(0);
							
    			    double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
    			    if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
    				mean_track += hit_chpos;
    				number_cluster++;
                }
			}	
		}//layernumber
					
		// Get the mean_track value
		mean_track /= number_cluster;
		
        //loop over all boards to get hit value in every layer
		if (number_cluster == 5){ //only calculate residual if other 5 layers have one cluster
            for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
                        
                layer = (layers[layType_UnderTest]).at(ind_layNumber); 
				clusters = layer.Getclusters_list();
						
				 if (clusters.size()==0) break; //DON'T DELETE THIS OPTION! if no cluster: skip -> otherwise you will try to reconstruct track from no cluster -> error (vector out of range)
                        
                 //get measured hit position in DUT
                 if (lay == reader.layer_name[layType_UnderTest][ind_layNumber]){
                    unsigned int layNumber_UnderTest = ind_layNumber;
				    if (clusters.size()==1) {
        				Cluster clu=clusters.at(0);			
				        measured_position = Channel2AcPosition(clu.channelPosition_weightedMean());
				        if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
        			}//cluster=1 
           					
                    else { //find the closest cluster 
          			    int ind_clu_selected = 0; 
    	    		    int diff = 1000;
						     
        			    for (unsigned int ind_clu = 0 ; ind_clu < clusters.size() ; ind_clu++) { 
        			    	Cluster clu=clusters.at(ind_clu);

					        double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
                           	if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
						    
        				   	if (abs(hit_chpos - mean_track) < diff ) { // Choose this cluster
        				   		ind_clu_selected = ind_clu; 
        				   		diff = abs(hit_chpos - mean_track); 
        				   	} 								
        				}
        				Cluster clu_selected=clusters.at(ind_clu_selected);
				        measured_position = Channel2AcPosition(clu_selected.channelPosition_weightedMean());
				        if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
                    }//closest cluster
                    continue;
                 }//DUT

                //get hits from other 5 layers to get track
                if (clusters.size()== 1) {
                    Cluster clu=clusters.at(0);
				    // construct track from all layers
				    double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
                    if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
				    double hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][ind_layNumber] - 1];
				    track->AddDaughter(hit_chpos, hit_zpos, reader.layer_Position[layType_UnderTest][ind_layNumber]);
                }//cluster==1 (ind_laynumber)
            }//ind_laynumber
                
		    track->FitLinear();
            // Selection on track quality 
            // 1. Cut on primary track - exclude everything further than 5 mm
            if (!(track->CheckHitsOutofRadius(5))) continue; 

            // 2. Cut on track qualities
            if (!(track->GoodQuality())) continue; 

            double residual;
            if (track->LinearFunc->Eval(measured_hit_zpos) > 0 && measured_position > 0){
                residual = track->LinearFunc->Eval(measured_hit_zpos) - measured_position;
                hist_general[0]->Fill(residual);
            }//residual
        }//layers==5
    }//events

    // Step 4 Plotting 
    c = new TCanvas("c","Residual",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Entries/0.01");
    hist_general[0]->GetXaxis()->SetTitle("Residual [mm]");
    hist_general[0]->Fit("gaus","R");

    //hist_general[0]->Draw("hist");

    return c;
}
/**********************************************************************************************************************************/
//AlignmentTracks()////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/
Alignment * Plotter::AlignmentTracks(bool * fixed_plane, unsigned int n_fixed_plane, float * shift){

    // Step 0 Check Configuration file 
    if (reader.n_layer_max/2 != n_fixed_plane ) { 
        std::cout << "Dimension of this argument does not match the one in n_layer_max" << std::endl; 
        std::cout << "Dim_plane: " << reader.n_layer_max/2 << " Dim_fixed_plane " << n_fixed_plane << std::endl; 
        return nullptr; 
    }

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Set the histogram
    hist_general[0]= new TH1D("Residual","Residual", 100, -2, 2);

    Alignment* alignment = new Alignment(); 

    // Step 3 Loop over all event and build tracks 
    for(int i=0;i<nentries;i++){
        // Setup
        Events.clear();
        LoadtheEntry(i, data_tree, 1);
        Event ev=Events.at(0);

        // Def var
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        std::vector<Cluster> clusters;
        std::vector<Track*> track_event;
        Track* track_x = new Track();
        Track* track_y = new Track();
        track_event.push_back(track_x);
        track_event.push_back(track_y);

        // Loop to find data points 
        // ind_layType == plane , ind_layNumber == planenumber
        bool excludeEvent = false; 
        for (unsigned int ind_layType = 0; ind_layType < (layers.size()); ind_layType++) {
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                layer = (layers[ind_layType]).at(ind_layNumber);           
                clusters = layer.Getclusters_list();

                // Preselection
                // Residual should  come from only 1 cluster per ind_layType
                if (clusters.size() !=  1 ) {
                    excludeEvent = true;
                    break;
                } 

                Cluster clu=clusters.at(0);

                // If the plane is fixed -> used for building a track  
                double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
                if (shift != nullptr) {hit_chpos -= shift[reader.layer_Position[ind_layType][ind_layNumber] - 1] ;
                                        //std::cout << "plane " << reader.layer_Position[ind_layType][ind_layNumber] - 1 << " is shifted by " << shift[reader.layer_Position[ind_layType][ind_layNumber] - 1] << std::endl;
                                        }
                double hit_zpos = reader.zpos[reader.layer_Position[ind_layType][ind_layNumber] - 1];
                if (fixed_plane[ind_layNumber]){
                    track_event[ind_layType]->AddDaughter(hit_chpos, hit_zpos, reader.layer_Position[ind_layType][ind_layNumber]);
                } else { // put to OtherHits in track
                    track_event[ind_layType]->AddHit(hit_chpos, hit_zpos, reader.layer_Position[ind_layType][ind_layNumber]); 
                }
            }//plannumber
            if (excludeEvent) break;
			track_event[ind_layType]->FitLinear();
            // Selection on track quality 
            // 1. Cut on primary track - exclude everything further than 5 mm
            if (!(track_event[ind_layType]->CheckHitsOutofRadius(5))) {excludeEvent = true; break;}; 

            // 2. Cut on track qualities
            if (!(track_event[ind_layType]->GoodQuality())) {excludeEvent = true; break;}; 

        }//planeType 
        // Check Multiple clusters - if found - skip
        // Check also track quality 
        if (excludeEvent) continue;

        // Add to Alignment
		alignment->AddTracks(track_event[0], track_event[1]); 
        //if (i > 200) {break;}

    }//events

    // Step 4 return the "alignment" object 

    return alignment;
}

/**********************************************************************************************************************************/
//AlignmentRun()////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::AlignmentRun(Alignment * alignment, bool * fixed_plane, unsigned int n_fixed_plane ){

    // Step 1 Initilisation 
    if (alignment == nullptr) {return;}
    if ((alignment->m_tracks_x).size() != (alignment->m_tracks_y).size() ) { // check tracks
        std::cout << "ERROR in tracks" << std::endl;
        return  ; 
    }
    unsigned int tracks_size = (alignment->m_tracks_x).size(); 


    alignment->InitMethod(); 
    alignment->InitMatrix();

    // Set fixed_plane 
    for (unsigned int ind_fp = 0 ; ind_fp < n_fixed_plane ; ind_fp++){
        if (fixed_plane[ind_fp] == 1){
            alignment->SetParameterSigma(2*ind_fp, 0.); // (index_plane, value)
            alignment->SetParameterSigma(2*ind_fp+1, 0.);
        }
    }

    // Step 2 Iterate the alignment
    unsigned int nIterate = 3 ; 
    for (unsigned int iIterate = 0 ; iIterate < nIterate ; iIterate++){
        alignment->SetMethod() ;
        // Step 2.1 Loop over tracks 
        for (unsigned int ind_track = 0; ind_track < tracks_size ; ind_track++) {
            alignment->FillAlignedMatrixFromTrack(ind_track);
            FITLOC(); // Fit each track  
        }
        // Step 2.2 Fit Global var (shifts)
        FITGLO(alignment->GetAlignedParameter());
    }

    // Step 3 Keep shift
    alignment->KeepShiftParams();

}

/**********************************************************************************************************************************/
//AlignmentStart()////////////////////////////////////////////////////////////////////////////////////////////////////////
// /////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::AlignmentStart(){

    unsigned int nStations = 6;
    //bool fixed_plane[3][6] = {{1, 0, 0, 0, 0, 1}, {0, 1, 1, 1, 1, 0}, {1, 0, 0, 0, 0, 1}};
    //bool fixed_plane[3][6] = {{1, 1, 0, 0, 0, 0}, {0, 0, 1, 1, 0, 0}, {0, 0, 1, 1, 1, 1}};
    //bool fixed_plane[3][6] = {{1, 1, 0, 0, 1, 1}, {1, 0, 1, 1, 0, 1}, {0, 1, 1, 1, 1, 0}};
    bool fixed_plane[6][6] = {{1, 1, 0, 0, 1, 1}, {1, 0, 1, 1, 0, 1}, {0, 1, 1, 1, 1, 0},{1, 1, 0, 0, 1, 1}, {1, 0, 1, 1, 0, 1}, {0, 1, 1, 1, 1, 0}};
    //bool fixed_plane[2][6] = {{1, 0, 0, 0, 0, 1}, {1, 0, 0, 0, 0, 1}}; //  leave for debugging
    
    //bool fixed_plane[6][6] = {{1, 0, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0}, {1, 0, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0}};
    //bool fixed_plane[3][6] = {{1, 0, 0, 0, 0, 1}, {0, 1, 1, 1, 1, 0}, {1, 0, 0, 0, 0, 1} };//, {1, 0, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0}};
    //bool fixed_plane[12][6] = {{1, 1, 1, 1, 1, 0}, {1, 1, 1, 1, 0, 1}, {1, 1, 1, 0, 1, 1}, {1, 1, 0, 1, 1, 1}, {1, 0, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 1}
                              //,{1, 0, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0}, {1, 0, 0, 0, 0, 1}, {0, 1, 0, 0, 1, 0}, {0, 0, 1, 1, 0, 0}
                              //,{1, 1, 1, 1, 1, 0}, {1, 1, 1, 1, 0, 1}, {1, 1, 1, 0, 1, 1}, {1, 1, 0, 1, 1, 1}, {1, 0, 1, 1, 1, 1}, {0, 1, 1, 1, 1, 1}
                              //,{1, 1, 0, 1, 1, 0}, {0, 1, 1, 1, 0, 1}, {1, 1, 1, 0, 1, 0}, {0, 1, 0, 1, 1, 1}, {1, 0, 0, 1, 1, 1}, {0, 1, 1, 1, 0, 1}
                              //};

    // Step 1 Build alignment  object
    Alignment * alignment = new Alignment(); 
    alignment->InitMatrix();

    // Step 2 Iterate over set of fixed planes

    // Step 2.1
    // Should find sth better than this
    for (unsigned int i = 0; i < 6 ; i++ ) {
        Alignment * alignment_buffer = AlignmentTracks(fixed_plane[i], nStations, alignment->GetFinalAlignedParameter());
        AlignmentRun(alignment_buffer, fixed_plane[i], nStations);

        alignment->SetAlignedParameter(alignment_buffer->GetAlignedParameter());
        alignment->KeepShiftParams();
        alignment->PrintKeptParameters();
    }

    // This method below is for fast checking - collect the tracks only once - not reliable
    //// Step 1 Build alignment  object
    //Alignment * alignment = AlignmentTracks(fixed_plane[0], nStations);

    //// Step 2 Iterate over set of fixed planes

    //// Step 2.1
    //// Should find sth better than this
    //for (unsigned int i = 0; i < 6 ; i++ ) {
    //    AlignmentRun(alignment, fixed_plane[i], nStations);
    //    alignment->PrintKeptParameters();
    //}



    // Step 3 Write to file and export to plotter::reader
    alignment->WriteShiftToFile(GetEnv("DATAPATH") + "RUN_" + run_number + "/shift_" + run_number +".txt"); 

    // export to reader -> add feature
    // Angular shift not yet implemented !! if need please add here and modify Reader::ImportShift, GenLayerName, 
    reader.ImportShift(alignment->GetFinalAlignedParameter(), alignment->m_nGlobal - alignment->m_nAngles);

}

///////////////////////// Additional function  ///////////////////////////////

// Input (ChPosition, max_cha_plane) 
// max_cha_plane = 512 in DESY TB 2019
double Plotter::Channel2AcPosition(double Channel, double max_cha_plane){
    
    if (Channel >= max_cha_plane){
        return -9;
    } 

    // Dead region - Multiplicity of gap_dies
    int multi_SiPM = int(max_cha_plane/128);

    int multi_dies = multi_SiPM;

    // compute half layer width to be entered in the middle of the plane
    // Set coordinate (0,0) at the center
    //double half_layer = max_cha_plane*reader.ch_width + multi_dies*reader.gap_die + (multi_SiPM - 1)*reader.gap_SiPM;

    // Shift from dead regions
    double shift = (int(Channel/64)/2 + int(Channel/64)%2)*reader.gap_die+int(Channel/64)/2*reader.gap_SiPM;
    //return (Channel)*reader.ch_width + shift - half_layer + reader.ch_width/2.; 
    // Position + shift  -  (OFFSET) + ChannelShift
    //return (Channel)*reader.ch_width + shift; 
    return (Channel)*reader.ch_width + shift ; 

}



std::vector<Cluster> Plotter::SelectClusters(std::vector<Cluster> clusters,double maxTimeSpread, unsigned int minLength, unsigned int maxLength, unsigned int maxAmp){

    std::vector<Cluster> selec_clusters;
    std::vector<bool> flag(clusters.size(),true);
    std::vector<double> tmin(clusters.size(),ULONG_MAX); std::vector<double> tmax(clusters.size(),0.0);
    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){ //parcours les clusters de l'événement

        unsigned int channelone=1537; unsigned int channellast=1537;
        Cluster clu=clusters.at(ind_cluNumber);

        for(unsigned int p=0;p<clu.clusterLength();p++){ 
            unsigned int ch=p+clu.startChannel();
            if (clu.amplitudeAtChannel(ch)>=maxAmp or clu.clusterLength()<=minLength or clu.clusterLength()>=maxLength){
                flag[ind_cluNumber]=false;
            }
            if(clu.amplitudeAtChannel(ch)<maxAmp and flag[ind_cluNumber]==true){	
                // fill first hit time
                if(clu.timeAtChannel(ch)<=tmin[ind_cluNumber]){
                    tmin[ind_cluNumber]=(double)(clu.timeAtChannel(ch));
                    channelone=ch;
                }
                // fill last hit time
                if(clu.timeAtChannel(ch)>=tmax[ind_cluNumber] and flag[ind_cluNumber]==true){
                    tmax[ind_cluNumber]=(double)(clu.timeAtChannel(ch));
                    channellast=ch;
                }
            }
        }
    }

    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
        if (flag[ind_cluNumber]==true){
            Cluster clu=clusters[ind_cluNumber];
            for(unsigned int p=0;p<clu.clusterLength();p++){
                unsigned int chA=p+clu.startChannel();										
                for(unsigned int l=p;l<clu.clusterLength();l++){
                    unsigned int chB=l+clu.startChannel();
                    double diff = tmax[ind_cluNumber]-tmin[ind_cluNumber];
                    double time=diff*57.5;
                    selec_clusters.push_back(clu);
                }				
            }
        }
    }

    return selec_clusters;
}

/**********************************************************************************************************************************/
//PlotAngularDistribution() per layerType with selected events with 1 cluster in at least 4 layers X and Y ////////////////
/**********************************************************************************************************************************/
// NEED Optimisation - WIP

void Plotter::PlotAngularDistribution(int nbrEvents, bool DisplayEvents) {

    DisplaySelectedTracks(nbrEvents, DisplayEvents); //to select events with single track

    //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    //defining coordinates
    double x;

    //compute half layer width to be entered in the middle of the plane
    double half_layer = 512./2 * reader.ch_width + 2 * reader.gap_die + 1.5 * reader.gap_SiPM;

    TString namecanvas="Angular distribution";
    prettycolors();
    gStyle->SetOptStat(1111);
    c = new TCanvas("c",namecanvas,600,800);
    c->Divide(1,2); // Fix this 

    for(unsigned int ind_layType=0;ind_layType<2;ind_layType++){

        TString plotname = reader.layer_name[ind_layType][0][0] + (TString)" ";

        angular_distribution[ind_layType] = new TH1D("Ang_distr" + plotname, "Angular distribution in " + plotname , 1400, -0.7, 0.7);
        angular_distribution[ind_layType]->GetXaxis()->SetTitle("angle [rad]");
        angular_distribution[ind_layType]->GetYaxis()->SetTitle("events");

    }

    //Loop over events
    for(unsigned int i=0; i<selected_events.size(); i++){

        //Load only one entry for the layer type
        Events.clear();
        LoadtheEntry(selected_events.at(i), data_tree, 1); 
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();

        unsigned int lay_min[layers.size()];

        unsigned int lay_max[layers.size()];

        double tangent[layers.size()];
        double angle[layers.size()];


        //Loop over layer type
        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){

            lay_min[ind_layType]=5; 
            lay_max[ind_layType]=0;

            //Loop over layers
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){

                //calculate the board number
                int board = reader.layer_BoardID[ind_layType][ind_layNumber];

                Layer layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters = layer.Getclusters_list();

                if(clusters.size()==1){ //consider only the selected layers: 1 cluster per layer

                    //verify that the cluster in the current layer has at least one hit which respect the condition: time trigger to hit lower than 5
                    double TrigTimeShift=times.at(board-1);
                    double ClusterTime;
                    double time_diff;
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){

                        Cluster clu=clusters.at(ind_cluNumber);
                        ClusterTime = abs(TrigTimeShift - clu.timeAtChannel(clu.startChannel()));

                        //loop over the channels of the cluster
                        for(unsigned int p=0;p<clu.clusterLength();p++){
                            unsigned int ch=p+clu.startChannel();
                            time_diff = abs(TrigTimeShift - clu.timeAtChannel(ch)); //time trigger to hit at the current channel in the cluster
                            if (ClusterTime > time_diff ) ClusterTime = time_diff;
                        } //channels
                    } //clusters

                    if(ClusterTime < 500){ //if the time condition is respected, the current layer is considered as part of the track

                        if (ind_layNumber > lay_max[ind_layType]) lay_max[ind_layType]=ind_layNumber;
                        if (ind_layNumber < lay_min[ind_layType]) lay_min[ind_layType]=ind_layNumber;
                    }
                }

            }//layers

            //cout << "Min = " << lay_min[ind_layType] << "Max = " << lay_max[ind_layType] << endl;

        }//layer_type


        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){

            Layer layer_min=(layers[ind_layType]).at(lay_min[ind_layType]);
            std::vector<Cluster> clusters_min = layer_min.Getclusters_list();
            double clusterPos_min=0;

            Layer layer_max=(layers[ind_layType]).at(lay_max[ind_layType]);
            std::vector<Cluster> clusters_max = layer_max.Getclusters_list();
            double clusterPos_max=0;

            //calculate the position of the cluster in the top layer of the track    
            for(unsigned int ind_Clusters=0; ind_Clusters<clusters_min.size(); ind_Clusters++){
                Cluster clu_min = clusters_min.at(ind_Clusters);             

                //calculate the position of the cluster in mm
                clusterPos_min = clu_min.channelPosition_weightedMean();
                double rest = clusterPos_min - (int)clusterPos_min;
                unsigned int ch_layer = (int)clusterPos_min;
                // how many half dies
                int mult = int((ch_layer)/64);
                // shift to add to the position due to dead regions
                double shift = int(mult/2) * reader.gap_die + int(mult/2) * reader.gap_SiPM 
                    + (mult%2) * reader.gap_die;
                clusterPos_min =  ( ch_layer*reader.ch_width + shift - half_layer ) + rest*reader.ch_width;
            }//clusters in clusters_min

            double z_min = z_Center[5-lay_min[ind_layType]];

            //calculate the position of the cluster in the bottom layer of the track    
            for(unsigned int ind_Clusters=0; ind_Clusters<clusters_max.size(); ind_Clusters++){
                Cluster clu_max = clusters_max.at(ind_Clusters);
                clusterPos_max = clu_max.channelPosition_weightedMean();
                double rest = clusterPos_max - (int)clusterPos_max;
                unsigned int ch_layer = (int)clusterPos_max;
                // how many half dies
                int mult = int((ch_layer)/64);
                // shift to add to the position due to dead regions
                double shift = int(mult/2) * reader.gap_die + int(mult/2) * reader.gap_SiPM 
                    + (mult%2) * reader.gap_die;
                clusterPos_max =  ( ch_layer*reader.ch_width + shift - half_layer ) + rest*reader.ch_width;
            }
            
            double z_max = z_Center[5-lay_max[ind_layType]];
            
            //calculate tangent of zenit angle and then zenit angle  
            tangent[ind_layType] = (clusterPos_max - clusterPos_min) / (z_min - z_max);
            angle[ind_layType] = atan(tangent[ind_layType]);
            angular_distribution[ind_layType]->Fill(angle[ind_layType]);
            
            //cout << "Tangent[" << ind_layType << "] = " << tangent[ind_layType] << endl;
        }
    } //events
    
    //plot the angular distribution for x and y
    for(unsigned int ind_layType=0;ind_layType<2;ind_layType++){
        c->cd(ind_layType+1);// ind_layType+ind_layNumber+1 position  in canvas
        angular_distribution[ind_layType]->Draw();
        c->Update();
    }
    
}

/**********************************************************************************************************************************/
//CalcTelescopeEfficiency()  Compute single board efficiency for all boards and prints it on the screen /////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::CalcTelescopeEfficiency(TString calibrationRun){

     //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;
    
    //Read timing delays from file to define the time window
    std::vector<double> TimeDelays(12,0); //vector with time delays for all the boards
        for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
            for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
                //calculate the board number
                int board = reader.layer_BoardID[ind_layType][ind_layNumber];
                TimeDelays[board-1] = reader.BoardID_config[board][1];
            }
        }

    //Path to the directory containing the calibration data (offsets)
    std::string path = GetEnv("DATAPATH");

    int lowEdge = 100;
    int upEdge = 100; 

    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){

            int board_test1=0, board_test2=0;

            int n_clusters_det = 0; // detected events 
            int n_clusters_tot = 0; // total number of events 

            int board = reader.layer_BoardID[ind_layType][ind_layNumber];

            //select the 2 boards to check the total number of events
            if ((ind_layNumber != 0) && (ind_layNumber != 5)) {
                board_test1 = board - 2;
                board_test2 = board + 2;
            }
            if (ind_layNumber == 0) {
                board_test1 = board + 2;
                board_test2 = board + 4;
            }
            if (ind_layNumber == 5) {
                board_test1 = board - 2;
                board_test2 = board - 4;
            }

            int layNumber_test1=reader.BoardID_config[board_test1][4]-1; 
            int layNumber_test2=reader.BoardID_config[board_test2][4]-1;


            //Loop all events 
            for(int i=0;i<nentries;i++){
                Events.clear();
                LoadtheEntry(i, data_tree, 1);
                Event ev=Events.at(0);

                std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
                std::vector<double> times =ev.Gettimestamp();
                Layer layer("");
                Layer layer_test1("");
                Layer layer_test2("");

                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();

                layer_test1=(layers[ind_layType]).at(layNumber_test1);
                std::vector<Cluster> clusters_test1 =layer_test1.Getclusters_list();

                layer_test2=(layers[ind_layType]).at(layNumber_test2);
                std::vector<Cluster> clusters_test2 =layer_test2.Getclusters_list();

                double TrigTimeShift=times.at(1); //trigger time is the same for all boards
                double ClusterTime=0;
                double time_diff, time_diff1, time_diff2;
                int n_clusters=0, n_clusters_test1=0, n_clusters_test2=0; 

                if((clusters_test1.size()>0) && (clusters_test2.size()>0)) {

                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters_test1.size();ind_cluNumber++){
                        Cluster clu1=clusters_test1.at(ind_cluNumber);
                        for(unsigned int p=0;p<clu1.clusterLength();p++){
                            unsigned int ch=p+clu1.startChannel();
                            double TimeWithOffset = clu1.timeAtChannel(ch) - reader.BoardID_config[board_test1][1] - ChTimeOffset[board_test1-1][ch]; //subtract time offset from each channel
                            if (p==0) ClusterTime = TimeWithOffset;
                            else if (TimeWithOffset<ClusterTime) ClusterTime = TimeWithOffset;
                        } // hits
                        time_diff1 = abs(TrigTimeShift - ClusterTime);
                        if ((time_diff1 > (abs(TimeDelays[board_test1-1])-lowEdge)) && (time_diff1 < (abs(TimeDelays[board_test1-1])+upEdge))) //cut on time for first test board
                        {
                            n_clusters_test1 ++;   
                        }  
                    }

                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters_test2.size();ind_cluNumber++){
                        Cluster clu2=clusters_test2.at(ind_cluNumber);
                        for(unsigned int p=0;p<clu2.clusterLength();p++){
                            unsigned int ch=p+clu2.startChannel();
                            double TimeWithOffset = clu2.timeAtChannel(ch) - reader.BoardID_config[board_test2][1] - ChTimeOffset[board_test2-1][ch]; //subtract time offset from each channel
                            if (p==0) ClusterTime = TimeWithOffset;
                            else if (TimeWithOffset<ClusterTime) ClusterTime = TimeWithOffset;
                        } // hits
                        time_diff2 = abs(TrigTimeShift - ClusterTime);
                        if ((time_diff2 > (abs(TimeDelays[board_test2-1])-lowEdge)) && (time_diff2 < (abs(TimeDelays[board_test2-1])+upEdge))) //cut on time for second test board
                        {
                            n_clusters_test2 ++;   
                        } 
                    }

                    if ((n_clusters_test1>0) && (n_clusters_test2>0)) {

                        n_clusters_tot++;

                        if (clusters.size()>0)  {

                            for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                                Cluster clu=clusters.at(ind_cluNumber);
                                for(unsigned int p=0;p<clu.clusterLength();p++){
                                    unsigned int ch=p+clu.startChannel();
                                    double TimeWithOffset = clu.timeAtChannel(ch) - reader.BoardID_config[board][1] - ChTimeOffset[board-1][ch]; //subtract the time offset from each channel
                                    if (p==0) ClusterTime = TimeWithOffset;
                                    else if (TimeWithOffset<ClusterTime) ClusterTime = TimeWithOffset;
                                } // hits
                                time_diff = abs(TrigTimeShift - ClusterTime);
                                if ((time_diff > (abs(TimeDelays[board-1])-lowEdge)) && (time_diff < (abs(TimeDelays[board-1])+upEdge))) //cut on time for the selected board
                                {
                                    n_clusters++;   
                                } 
                            }
                        }
                        if(n_clusters>0) {n_clusters_det++;}
                    }
                }
            }

            //Calculate the efficiency
            double eff = ((double)n_clusters_det) / ((double)n_clusters_tot);
            cout << endl;
            cout << "Board " << board << endl;
            cout << endl;
            cout << "Neighbouring boards used to select the sample of events: " << board_test1 << " " << board_test2 << endl;
            //cout << ind_layNumber << endl;
            //cout << ind_layType << endl;

            cout << "Number of events with at least one hit in the selected board and in the two neighbouring boards: " << n_clusters_det << endl;
            cout << "Number of events with at least one hit in the two neighbouring boards: " << n_clusters_tot << endl;
            cout << "Efficiency: " << eff << endl << endl;
        }
    }
}


/**********************************************************************************************************************************/
//Dump events info //////////////////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::DumpEvent(uint64_t evnum){
    // If cluster is required -> setting "unsigned int cluster_min_size" may be needed
    // If only one  layer is need -> set lay == "X2" for example if you want to look at only X2 layer

		Events.clear();
        // Load Entry ith from data_tree with cluster (set last bit to 1) or without (set last bit to 0)
        LoadtheEntry(evnum, data_tree, 1);
        Event ev=Events.at(0); // Always at 0 - Load one by one 
        
               
        // Get Layer list
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");

        // Convention when do loop
        // ind_layType == index of layer type (number) == defined in (reader.layer_name)
        // ind_layNumber == index of layer number 
        // ind_cluNumber == index of cluster on a layer in question
        // p == position of a hit in a cluster 
        for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
			std::cout<<"layertype: "<<ind_layType<<std::endl;
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
				std::cout<<"layer number: "<<ind_layNumber<<std::endl;
                // if (lay != reader.layer_name[ind_layType][ind_layNumber])continue; // This line may be added to choose specific layer defined in the function
                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();

                // Loop over all cluster
                if(clusters.size()>0){
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        clu.dump();
                    }//clusters in the layer
                }
            }
        }//layers
        return;
 }


/**********************************************************************************************************************************/
//FindDeadChannels()////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Finds dead channels and associates STiC and board///////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

// Change how we plot the beamspot -> should plot all layer
void Plotter::FindDeadChannels(){
    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Step 2 Define TH1D
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            t[ind_layType][ind_layNumber] = new TH1D(reader.layer_name[ind_layType][ind_layNumber], reader.layer_name[ind_layType][ind_layNumber],512,-0.5,511.5);
        }
    }

    // Step 3 Loop over all entries
    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, data_tree, 0); // Load with no clustering
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();

        // each event display all layer
        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size());ind_layNumber++){ 
                Layer layer=(layers[ind_layType]).at(ind_layNumber);
                for(int ch_position = 0; ch_position<512 ; ch_position++){
                    if((layer.GetAmplitudes())[ch_position]>0) t[ind_layType][ind_layNumber]->Fill(ch_position);
                }
            }
        }
    }

   //Step 4 

   
   unsigned int Board_ID;
   unsigned int STiC_ID;
   unsigned int STiC_chID;
   for(unsigned int ind_layType=0;ind_layType<2;ind_layType++){
            for (unsigned int ind_layNumber = 0; ind_layNumber<6;ind_layNumber++){ 
				Board_ID= ind_layNumber*2 + (1-ind_layType) + 1;
				for(int bin=1;bin<513; bin++ ){
					if(t[ind_layType][ind_layNumber]->GetBinContent(bin)<4){
						reader.dead_channels[ind_layType][ind_layNumber][bin-1]=1;
						}
				}
			}
	}
   
 
    return ;
}	
	




/**********************************************************************************************************************************/
//SingleEventDisplay()///////////////////////////////////////////////////////////////////
//Primitive single event display  eventnum: start_event_number, nevents: number of events, continous: dispay nevents continous /////////
/**********************************************************************************************************************************/

TCanvas * Plotter::DisplaySingleEvent(int eventnum, int nevents, bool continous){

    //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;
    int loop_event_size;

    //defining coordinates
    double_t z_BinEdge[14] = {-10, -5., 15., 35., 55., 150., 170., 190., 210., 305., 325., 345., 365., 370.}; 
   
    // Display more than one event ?
    if (continous) loop_event_size = nevents; else loop_event_size = 1; 
    //
    for(int ev_num=eventnum; ev_num<eventnum+loop_event_size;ev_num++)	{ // loop over events
		Events.clear();
		LoadtheEntry(ev_num, data_tree, 1); // Load with no clustering
		Event ev=Events.at(0);
		std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
		int counterChannels = 0;
		int counterLayers = 0;

    //Loop through layer type
    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
        TString plotname = reader.layer_name[ind_layType][0][0] + (TString)" ";
        hist2D_general[ind_layType] = new TH2D(plotname, plotname + "Layers, Event " + to_string(ev_num)+";position [mm];Z position", 512, 0, 133, 13, z_BinEdge);

        //Loop over layers
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            Layer layer=(layers[ind_layType]).at(ind_layNumber);
            std::vector<Cluster> clusters =layer.Getclusters_list();
                 for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                    Cluster clu=clusters.at(ind_cluNumber);
                    for(unsigned int hit=0;hit<clu.clusterLength();hit++){
					  //hist2D_general[ind_layType]->Fill(x,z,amp)	
				      hist2D_general[ind_layType]->Fill(Channel2AcPosition(clu.startChannel()+hit), z_Center[ind_layNumber], clu.amplitudeAtChannel(clu.startChannel()+hit));		
					}
				}

        }//layers
    }//type


    //Draw amplitude and position of hits
    TString namecanvas="Event ";
    prettycolors();
    gStyle->SetOptStat(1111);
    c = new TCanvas("c",namecanvas,600,800);
    c->Divide(1,2); // Fix this 
    //Loop over the layer type
    for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){
        c->cd(ind_layType+1);// ind_layType+ind_layNumber+1 position  in canvas
        hist2D_general[ind_layType]->Draw("COLZ");
        c->Update();
    }
   
	} // end loop over events
    return c;
} 

/**********************************************************************************************************************************/
//DisplayEventsPosition()//////////////////////////////////////////////////////////
//Display of a certain number of events ("eventnum") in every board as a function of position /////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::DisplayEventsPosition(int eventnum){

    //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;


    // defining coordinates
    double x;

    //compute half layer width to be entered in the middle of the plane
    double half_layer = 512./2 * reader.ch_width + 2 * reader.gap_die + 1.5 * reader.gap_SiPM;

    unsigned int loop_size;
    if (eventnum == -1) {loop_size=data_tree->GetEntries();}
    else {loop_size = eventnum;}

    //Loop through layer type
    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){

        TString plotname = reader.layer_name[ind_layType][0][0] + (TString)" ";

        hist2D_general[ind_layType] = new TH2D(plotname, plotname + "Layers, Event " + to_string(eventnum)+";position [mm];Z position", 512, -70, 70, 13, z_BinEdge);

        // Loop over events
        for(unsigned int numevents=0; numevents<loop_size; numevents++){

            Events.clear();
            LoadtheEntry(numevents, data_tree, 0); // Load with no clustering
            Event ev=Events.at(0);

            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();

            //Loop over layers
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size());ind_layNumber++){
                Layer layer=(layers[ind_layType]).at(ind_layNumber);


                //Loop over channels
                for(int h=0;h<512;h++){


                    // channel in each layer ranging from 0-512
                    unsigned int ch_layer = h;

                    // how many half dies
                    int mult = int((ch_layer)/64);

                    // shift to add to the position due to dead regions
                    double shift = int(mult/2) * reader.gap_die + int(mult/2) * reader.gap_SiPM 
                        + (mult%2) * reader.gap_die;

                    //first station only x defined for X and U planes
                    x =  ( ch_layer*reader.ch_width + shift - half_layer );

                    if((layer.GetAmplitudes())[h]>0){ 
                        hist2D_general[ind_layType]->Fill(x, z_Center[5-ind_layNumber], (layer.GetAmplitudes())[h]);
                    }

                }
            }
        }
    }

    // Step 4 Draw amplitude and position of hits
    TString namecanvas="Event ";
    prettycolors();
    gStyle->SetOptStat(1111);
    //namecanvas+=evnum;
    c = new TCanvas("c",namecanvas,600,800);
    c->Divide(1,2); // Fix this 
    // Loop over the layer type
    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
        c->cd(ind_layType+1);// ind_layType+ind_layNumber+1 position  in canvas
        hist2D_general[ind_layType]->Draw("COLZ");
        c->Update();
    }
    return c;
}

/**********************************************************************************************************************************/
//DisplaySelectedTracks() ///////////////////////////////////////////////////////////////////////////
//Display Selected events with single track
/**********************************************************************************************************************************/
void Plotter::DisplaySelectedTracks(int nbrEvents, bool eventDisplay) {

    //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    selected_events.clear();
    int loop_size;

    if(nbrEvents==-1) loop_size = nentries;
    else loop_size = nbrEvents;

    //defining coordinates
    double x;

    //half layer width
    double half_layer = 512./2 * reader.ch_width + 2 * reader.gap_die + 1.5 * reader.gap_SiPM;

    if(eventDisplay==1){
        TString namecanvas="Event";
        prettycolors();
        gStyle->SetOptStat(1111);
        c = new TCanvas("c",namecanvas,600,800);
        c->Divide(1,2); // Fix this 
    }

    int candidates = 0;

    //Loop over events
    for(int evNbr=0; evNbr<loop_size; evNbr++){

        //Load only one entry for the layer type
        Events.clear();
        LoadtheEntry(evNbr, data_tree, 1); 
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();
        int counterChannels = 0;

        int counterLayers[layers.size()];
        for (unsigned int i=0; i<layers.size(); i++) {counterLayers[i]=0;}

        //Loop over layer type
        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){

            if(eventDisplay==1){

                TString plotname = reader.layer_name[ind_layType][0][0] + (TString)" ";
                hist2D_general[ind_layType] = new TH2D(plotname, plotname + "Layers, Event " + to_string(evNbr)+";position [mm];Z position", 512, -70, 70, 13, z_BinEdge);
            }

            //Loop over layers
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size());ind_layNumber++){

                Layer layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters = layer.Getclusters_list();

                if(clusters.size() < 2) {//First condition: one cluster per layer

                    //Loop over channels
                    for(int ch=0; ch<512; ch++){

                        double Time=(layer.GetTimes())[ch];
                        double TrigTimeShift=times.at(ind_layNumber);
                        double ti = abs(-TrigTimeShift+Time);

                        if((layer.GetAmplitudes())[ch]>0){

                            counterChannels++;

                            //Second condition: time trigger-to-hit lower than 500 ns
                            if(ti <= 500) {

                                // Calculation of position
                                // channel in each layer ranging from 0-512
                                unsigned int ch_layer = ch;
                                // how many half dies
                                int mult = int((ch_layer)/64);

                                // shift to add to the position due to dead regions
                                double shift = int(mult/2) * reader.gap_die + int(mult/2) * reader.gap_SiPM 
                                    + (mult%2) * reader.gap_die;

                                //position of the considered channel
                                x =  ( ch_layer*reader.ch_width + shift - half_layer );

                                if(eventDisplay==1) hist2D_general[ind_layType]->Fill(x, z_Center[5-ind_layNumber], (layer.GetAmplitudes())[ch]);
                            }
                        }
                    }

                    if (counterChannels > 0) {
                        counterLayers[ind_layType]++; 
                        counterChannels = 0;
                    }
                }
            }//layers

        }//layer_type

        if((counterLayers[0] >= 4) && (counterLayers[1] >= 4)){ // Third condition: at least 4 layers in the track (for each plane, x and y)

            selected_events.push_back(evNbr);
            //std::cout << "Number of the event: " << selected_events.at(candidates) << endl;
            candidates++;

            if(eventDisplay == 1) {

                for(unsigned int ind_layType=0;ind_layType<2;ind_layType++){
                    c->cd(ind_layType+1);// ind_layType+ind_layNumber+1 position  in canvas
                    hist2D_general[ind_layType]->Draw("COLZ");
                    c->Update();

                }
                std::this_thread::sleep_for (std::chrono::seconds(1));
            }
        }
    } //events

    std::cout << "Number of candidates: " << candidates << endl;
}

/**********************************************************************************************************************************/
//PrintSelectedEvents() /////////////////////////////////////////////////////////////////////////////////////
//Print entries and dimension of the private variable "selected_events" //////////////////////////////////
/**********************************************************************************************************************************/
void Plotter::PrintSelectedEvents(){
    for(unsigned int i=0; i<selected_events.size(); i++){
        cout << selected_events.at(i) << endl;
    }
    cout << "Vector size: " << selected_events.size() << endl;
}

/**********************************************************************************************************************************/
//PlotClusterMultiplicity()//////////////////////////////////////////
//Plot and calculate the cluster multiplicity per events in all the boards --> Filling histograms "multiplicity" ////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotClusterMultiplicity(TString calibrationRun){

    //Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    c = new TCanvas("c","Cluster Multiplicity",600,800);
    c->Divide(2,6);

    //Path to the directory containing the calibration data (offsets)
    std::string path = GetEnv("DATAPATH");

    // Get Board Offsets from Configuration file
    std::vector<double> TimeDelays(12,0); //vector with time delays for all the boards

    for(unsigned int ind_layType=0;ind_layType < reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){

            int board = reader.layer_BoardID[ind_layType][ind_layNumber];
            TimeDelays[board-1] = reader.BoardID_config[board][2];

            //Read timing delay from file to determine the time window
            double LowEdge = abs(TimeDelays[board-1]) - 50;  
            double UpEdge = abs(TimeDelays[board-1]) + 50;   

            int n_clusters = 0;
            int count_RejectedEv=0; 
            int zero_clusters=0;
            //int ind_layType = reader.BoardID_config[board][3];
            //int ind_layNumber = reader.BoardID_config[board][4]-1;

            //Set the histograms
            TString histName = "Clu_Mult_board" + to_string(board);
            TString histTitle = "Cluster Multiplicity in Board " + to_string(board);
            multiplicity[board-1]= new TH1D(histName, histTitle, 10, -0.5, 9.5);

            //Loop all events, select with time cut
            for(int i=0;i<nentries;i++){

                Events.clear();
                LoadtheEntry(i, data_tree, 1);
                Event ev=Events.at(0);

                std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
                std::vector<double> times =ev.Gettimestamp();
                Layer layer("");

                layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters =layer.Getclusters_list();

                double TrigTimeShift=times.at(board-1);
                double ClusterTime=0;
                double time_diff;
                if(clusters.size()>0){

                    n_clusters=0;
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){
                        Cluster clu=clusters.at(ind_cluNumber);
                        for(unsigned int p=0;p<clu.clusterLength();p++){
                            unsigned int ch=p+clu.startChannel();
                            double TimeWithOffset = clu.timeAtChannel(ch) - reader.BoardID_config[board][1] - ChTimeOffset[board-1][ch];
                            if (p==0) ClusterTime = TimeWithOffset;
                            else if (TimeWithOffset < ClusterTime) ClusterTime = TimeWithOffset;
                        } // hits
                        time_diff = abs(TrigTimeShift - ClusterTime);
                        if ((time_diff > LowEdge) && (time_diff < UpEdge)) //cut on time 
                        {
                            n_clusters++;   
                        }  
                    } 

                    if(n_clusters==0) {
                        count_RejectedEv++; 
                    } 
                } 
                else{
                    zero_clusters++;
                    n_clusters=0;
                }

                multiplicity[board-1]->Fill(n_clusters);
            }//events

            c->cd(ind_layType+2*ind_layNumber+1);
            multiplicity[board-1]->GetYaxis()->SetTitle("Events");
            multiplicity[board-1]->GetXaxis()->SetTitle("Cluster Multiplicity");
            multiplicity[board-1]->Draw("hist");
            c->Update();

            //Report
            cout << endl;
            cout << "Board number " << board << endl;
            cout << "Time delay: " << abs(TimeDelays[board-1]) << endl;
            cout << "Total Events = " << nentries << endl;
            cout << "Integral of the histogram = " << multiplicity[board-1]->Integral(-1,11) << endl;

            cout << "Events with zero clusters = " << zero_clusters <<endl;
            cout << "Events rejected by time cut = " << count_RejectedEv << endl;

            cout << "Sum of events with zero clusters and rejected events = " << count_RejectedEv + zero_clusters << endl;
            cout << "0-clusters   = " << multiplicity[board-1]->GetBinContent(1) << endl; // must be equal to sum of events with zero clusters and rejected events
            cout << "1-clusters   = " << multiplicity[board-1]->GetBinContent(2) << endl;
            cout << "2-clusters   = " << multiplicity[board-1]->GetBinContent(3) << endl;
            cout << "3-clusters   = " << multiplicity[board-1]->GetBinContent(4) << endl;

        } // layer number 1/6
    } // layer type X/Y

    return c;
}

/*****************************************************************************************************************/
//PlotCTRdiffLayers() ////////////////////////////////////////////////////////////////////////////////////////////
//Plot time correlation and CTR between 2 different layers. Considered times: fastest hits in the clusters. //////
/*****************************************************************************************************************/

TCanvas * Plotter::PlotCTRdiffLayers(TString lay1, TString lay2, TString calibrationRun, double theta_max, bool correction_travelTime, bool injection_corr){
        
    SelectTracksAngularCut(-1, theta_max); //to select events with single track and small zenit angle
    
    // Setp 0 Apply (or not)  propagation correction 
    double linear_coeff;
    if(injection_corr==1) linear_coeff = 774.242;
    else linear_coeff=0.;

    //Calculate the board numbers from layer strings
    //select layer type (0 or 1) and number (from 0 to 5)
    int layType_board1=0, layNumber_board1=0;
    int layType_board2=0, layNumber_board2=0;

    Events.clear();
    LoadtheEntry(1  , data_tree, 1);
    Event ev=Events.at(0);
    std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
    for (unsigned int ind_layType=0; ind_layType<layers.size(); ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
            if (lay1 == reader.layer_name[ind_layType][ind_layNumber]){
                layType_board1 = ind_layType;
                layNumber_board1 = ind_layNumber;
            }
            if (lay2 == reader.layer_name[ind_layType][ind_layNumber]){
                layType_board2 = ind_layType;
                layNumber_board2 = ind_layNumber;
            }
        }//layers
    }//layer types
    
    //calculate the board number
    int board1 = 2 * (layNumber_board1+1) - layType_board1;
    cout << "First Board Number = " << board1 << endl;
    int board2 = 2 * (layNumber_board2+1) - layType_board2;
    cout << "Second Board Number = " << board2 << endl;
    
    TString namecanvas = "CTRdiffLayers";
    TString plotname1 = "Cluster times";
    TString plotname2 = "CTR";
    
    TH2D * ClusterTime_plot = new TH2D(plotname1, plotname1 + ", Layers " + lay1 + " and " + lay2, 1000, -1000, 1000, 1000, -1000, 1000);
    ClusterTime_plot -> GetXaxis() -> SetTitle("time in layer " + lay1 + " [ns]");
    ClusterTime_plot -> GetYaxis() -> SetTitle("time in layer " + lay2 + " [ns]");

    TH1D * TimeDiff_plot = new TH1D(plotname2, plotname2 + ", Layers " + lay1 + " and " + lay2, 200, -20.0, 20.0);
    TimeDiff_plot -> GetXaxis() -> SetTitle("time difference [ns]");
    TimeDiff_plot -> GetYaxis() -> SetTitle("events");

    TH1D * Correction = new TH1D("corr", "Correction, Layers " + lay1 + " and " + lay2, 100 , -1, 1);
    Correction -> GetXaxis() -> SetTitle("correction time [ns]");
    Correction -> GetYaxis() -> SetTitle("events");
    
    //Define two vectors for time offsets in the two layers
    vector<double> Toffsetvector1(512,0);
    vector<double> Toffsetvector2(512,0);
    
    //Open file with offsets calculated from calibration data and fill time offset vector for layer 1
    if (calibrationRun != ""){
        TString path = GetEnv("DATAPATH");//Path to the directory containing the calibration data (offsets)
        std::ifstream chdelay1(path + "RUN_" + calibrationRun + "/ch_VCSEL_risingEdge_" + to_string(board1) + ".txt");
        std::string line;
        if (chdelay1.is_open()){
            while(std::getline(chdelay1, line)){    // read one line from chdelay (ch | Toffset | error | bool)
                std::istringstream streamline1(line); // access line as a stream
                // we need the first two columns (channel and offset)
                int column1;
                float column2;
                streamline1 >> column1 >> column2;
                Toffsetvector1[column1]=column2;
            }
        }
        else {
            cout << "Unable to open the offset file: " << path << "RUN_" << calibrationRun << "/ch_VCSEL_risingEdge_" << board1 << ".txt" << endl;
        }
        
        //Open file with offsets calculated from calibration data and fill time offset vector for layer 2
        std::ifstream chdelay2(path + "RUN_" + calibrationRun + "/ch_VCSEL_risingEdge_" + to_string(board2) + ".txt");
        if (chdelay2.is_open()){
            while(std::getline(chdelay2, line)){    // read one line from chdelay (ch | Toffset | error | bool)
                std::istringstream streamline2(line); // access line as a stream
                // we need the first two columns (chanel and offset)
                int column1;
                float column2;
                streamline2 >> column1 >> column2;
                Toffsetvector2[column1]=column2;
            }
        }
        else {
            cout << "Unable to open the offset file: " << path << "RUN_" << calibrationRun << "/ch_VCSEL_risingEdge_" << board2 << ".txt" << endl;
        }
    } else {
    
        Toffsetvector1 = ChTimeOffset[reader.layer_BoardID[layType_board1][layNumber_board1]-1];
        Toffsetvector2 = ChTimeOffset[reader.layer_BoardID[layType_board2][layNumber_board2]-1];
    
    }
    
    //define z position of the boards and distance between board1 and board2
    double_t z[6]= {5., 45., 160., 200., 315., 355.}; 
    double delta_z = abs(z[5-layNumber_board1] - z[5-layNumber_board2]);
    
    double ClusterTime1=0;
    double ClusterTime2=0;
    double delta_pos=0;
    double corr=0;
    double c_fiber= 150; //velocity of photons in the fiber [mm/ns]   
    //Loop over selected events
    for(unsigned int i=0; i<(selected_events_angCut[0]).size(); i++){

        Events.clear();
        LoadtheEntry((selected_events_angCut[0]).at(i), data_tree, 1);
        Event ev=Events.at(0);
        
        std::vector<double> times =ev.Gettimestamp();
        double TrigTimeShift=times.at(1); //trigger time is the same for all boards
        
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer1 = (layers[layType_board1]).at(layNumber_board1);
        Layer layer2 = (layers[layType_board2]).at(layNumber_board2);
    
        std::vector<Cluster> clusters1 = layer1.Getclusters_list();        
        std::vector<Cluster> clusters2 = layer2.Getclusters_list();
        
        if((clusters1.size()==1)&&(clusters2.size()==1)){ //select only layers with one cluster
            
            for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters1.size();ind_cluNumber++){
                
                Cluster clu1=clusters1.at(ind_cluNumber);
                Cluster clu2=clusters2.at(ind_cluNumber);
                
                //select the fastest hit for the cluster in the first layer
                for(unsigned int p=0;p<clu1.clusterLength();p++){
                    unsigned int ch=p+clu1.startChannel();
                    double timeWithOffset = clu1.timeAtChannel(ch) - reader.BoardID_config[board1][1]- Toffsetvector1[ch]; // //subtract the channel offset and add the board offset
                    timeWithOffset += ch/linear_coeff;
                    //double timeWithOffset = clu1.timeAtChannel(ch);
                    if (p==0) ClusterTime1 = timeWithOffset;
                    else if (timeWithOffset < ClusterTime1) ClusterTime1 = timeWithOffset;
                } //hits cluster 1
                
                //select the fastest hit for the cluster in the second layer
                for(unsigned int p=0;p<clu2.clusterLength();p++){
                    unsigned int ch=p+clu2.startChannel();
                    double timeWithOffset = clu2.timeAtChannel(ch) - reader.BoardID_config[board2][1] - Toffsetvector2[ch]; // 
                    timeWithOffset += ch/linear_coeff;
                    //double timeWithOffset = clu2.timeAtChannel(ch);
                    if (p==0) ClusterTime2 = timeWithOffset;
                    else if (timeWithOffset < ClusterTime2) ClusterTime2 = timeWithOffset;
                } //hits cluster 2
    
            }
            
			ClusterTime1 = ClusterTime1 - TrigTimeShift; 
			ClusterTime2 = ClusterTime2 - TrigTimeShift; 
			
			//correction due to travelling time of the photons in the fibers
			if(correction_travelTime == 1){	
                
                //find the maximum between board1 and board2 (that is the lowest board)
                int max = board2;
                int layType_maxboard = layType_board2;
                if (board1 > board2) {
                    max = board1;
                    layType_maxboard = layType_board1;   
                }
                
                //calculate the shift in position in the lowest board (due to the inclination of the track)
                delta_pos = delta_z * sin(selected_events_angCut[2-layType_maxboard][i]); 
                //NOTE: theta_x (that give a shift in y-layers) are stored in row 1 of selected_events_angCut, while theta_y (that give a shift in x-layers) are stored in row 2
                
                corr = delta_pos/c_fiber;
                Correction -> Fill(corr);
                
                if(max==board2) {	
                    ClusterTime2 = ClusterTime2 - corr;
                }
                if(max==board1) {
                    ClusterTime1 = ClusterTime1 - corr;
                }                
            }

			ClusterTime_plot -> Fill(ClusterTime1, ClusterTime2);
			TimeDiff_plot -> Fill(ClusterTime2 - ClusterTime1);
			cout.precision(20);
			//cout << ClusterTime1 << "   " << ClusterTime2 << endl;
			//cout << ClusterTime2-ClusterTime1 << endl;
           
        }
        
    } //events
    
    //CTR fit 		
    TF1 *f1= new TF1("na", "gaus", -20.0, 20.0);				
    f1->SetParameter(1,0);
    f1->SetParameter(2,1000);
    
    //plot
    prettycolors();
    gStyle->SetOptStat(1111);
    gStyle->SetOptFit(1111);
    c = new TCanvas("c",namecanvas,600,800);
        
    if(correction_travelTime == 1){
        c->Divide(1,3);
        c->cd(1);
        ClusterTime_plot -> Draw("COLZ");
        c->Update();
        c->cd(2);
        TimeDiff_plot -> Draw("");
        TimeDiff_plot->Fit("na", "R");						
        f1->Draw("SAME");
        c->Update();
        c->cd(3);
        Correction -> Draw("");
        c->Update();
    }
    else{
        c->Divide(1,2);
        c->cd(1);
        ClusterTime_plot -> Draw("COLZ");
        c->cd(2);
        TimeDiff_plot -> Draw();
        TimeDiff_plot->Fit("na", "R");						
        f1->Draw("SAME");
    }
    //c->Update();
    
    
    

    
    return c;
}




/**********************************************************************************************************************************/
//SelectTracksAngularCut() ////////////////////////////////////////////////////////
//Select events with single tracks and zenit angle below a certain theta_max///////
/**********************************************************************************************************************************/
void Plotter::SelectTracksAngularCut(int nbrEvents, double theta_max) {

    DisplaySelectedTracks(nbrEvents, 0); //to select events with single track

    selected_events_angCut.clear();

    //defining coordinates
    double x;

    //compute half layer width to be entered in the middle of the plane
    double half_layer = 512./2 * reader.ch_width + 2 * reader.gap_die + 1.5 * reader.gap_SiPM;

    //define vectors to keep track of selected events
    vector<double> selected_events_withcut;
	vector<double> angle_vectorX;
	vector<double> angle_vectorY;

    //Loop over events
    for(unsigned int i=0; i<selected_events.size(); i++){

        //Load only one entry for the layer type
        Events.clear();
        LoadtheEntry(selected_events.at(i), data_tree, 1); 
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();

        unsigned int lay_min[layers.size()];

        unsigned int lay_max[layers.size()];

        double tangent[layers.size()];
        double angle[layers.size()];


        //Loop over layer type
        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){

            lay_min[ind_layType]=5; 
            lay_max[ind_layType]=0;

            //Loop over layers
            for (unsigned int ind_layNumber = 0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){

                //calculate the board number
                int board = 2*(ind_layNumber+1) - ind_layType;

                Layer layer=(layers[ind_layType]).at(ind_layNumber);
                std::vector<Cluster> clusters = layer.Getclusters_list();

                if(clusters.size()==1){ //consider only the selected layers: 1 cluster per layer

                    //verify that the cluster in the current layer has at least one hit which respect the condition: time trigger to hit lower than 500
                    double TrigTimeShift=times.at(board-1);
                    double ClusterTime;
                    double time_diff;
                    for(unsigned int ind_cluNumber=0;ind_cluNumber<clusters.size();ind_cluNumber++){

                        Cluster clu=clusters.at(ind_cluNumber);
                        ClusterTime = abs(TrigTimeShift - clu.timeAtChannel(clu.startChannel()));

                        //loop over the channels of the cluster
                        for(unsigned int p=0;p<clu.clusterLength();p++){
                            unsigned int ch=p+clu.startChannel();
                            time_diff = abs(TrigTimeShift - clu.timeAtChannel(ch)); //time trigger to hit at the current channel in the cluster
                            if (ClusterTime > time_diff ) ClusterTime = time_diff;
                        } //channels
                    } //clusters

                    if(ClusterTime < 500){ //if the time condition is respected, the current layer is considered as part of the track

                        if (ind_layNumber > lay_max[ind_layType]) lay_max[ind_layType]=ind_layNumber;
                        if (ind_layNumber < lay_min[ind_layType]) lay_min[ind_layType]=ind_layNumber;
                    }
                }

            }//layers

            //cout << "Min = " << lay_min[ind_layType] << "Max = " << lay_max[ind_layType] << endl;

        }//layer_type


        for(unsigned int ind_layType=0;ind_layType<layers.size();ind_layType++){

            Layer layer_min=(layers[ind_layType]).at(lay_min[ind_layType]);
            std::vector<Cluster> clusters_min = layer_min.Getclusters_list();
            double clusterPos_min=0;

            Layer layer_max=(layers[ind_layType]).at(lay_max[ind_layType]);
            std::vector<Cluster> clusters_max = layer_max.Getclusters_list();
            double clusterPos_max=0;

            //calculate the position of the cluster in the top layer of the track    
            for(unsigned int ind_Clusters=0; ind_Clusters<clusters_min.size(); ind_Clusters++){
                Cluster clu_min = clusters_min.at(ind_Clusters);             

                //calculate the position of the cluster in mm
                clusterPos_min = clu_min.channelPosition_weightedMean();
                double rest = clusterPos_min - (int)clusterPos_min;
                unsigned int ch_layer = (int)clusterPos_min;
                // how many half dies
                int mult = int((ch_layer)/64);
                // shift to add to the position due to dead regions
                double shift = int(mult/2) * reader.gap_die + int(mult/2) * reader.gap_SiPM 
                    + (mult%2) * reader.gap_die;
                clusterPos_min =  ( ch_layer*reader.ch_width + shift - half_layer ) + rest*reader.ch_width;
            }//clusters in clusters_min

            double z_min = z_Center[5-lay_min[ind_layType]];

            //calculate the position of the cluster in the bottom layer of the track    
            for(unsigned int ind_Clusters=0; ind_Clusters<clusters_max.size(); ind_Clusters++){
                Cluster clu_max = clusters_max.at(ind_Clusters);
                clusterPos_max = clu_max.channelPosition_weightedMean();
                double rest = clusterPos_max - (int)clusterPos_max;
                unsigned int ch_layer = (int)clusterPos_max;
                // how many half dies
                int mult = int((ch_layer)/64);
                // shift to add to the position due to dead regions
                double shift = int(mult/2) * reader.gap_die + int(mult/2) * reader.gap_SiPM 
                    + (mult%2) * reader.gap_die;
                clusterPos_max =  ( ch_layer*reader.ch_width + shift - half_layer ) + rest*reader.ch_width;
            }

            double z_max = z_Center[5-lay_max[ind_layType]];

            //calculate tangent of zenit angle and then zenith angle  
            tangent[ind_layType] = (clusterPos_max - clusterPos_min) / (z_min - z_max);
            angle[ind_layType] = atan(tangent[ind_layType]);
            //angular_distribution[ind_layType]->Fill(angle[ind_layType]);
        }
        
        if((abs(angle[0]) < theta_max) && (abs(angle[1]) < theta_max)) { //cut on the zenith angle
			selected_events_withcut.push_back(selected_events.at(i));
			angle_vectorX.push_back(angle[0]);
			angle_vectorY.push_back(angle[1]);
           
            //cout << "X angle: " << angle[0] << endl;
            //cout << "Y angle: " << angle[1] << endl;
        }
    } //events
    
    selected_events_angCut.push_back(selected_events_withcut);
    selected_events_angCut.push_back(angle_vectorX);
    selected_events_angCut.push_back(angle_vectorY);
    
    cout << "Number of selected events with angular cut: " << selected_events_angCut[0].size() << endl;
    //cout << selected_events_angCut[1].size() << endl;
    //cout << selected_events_angCut[2].size() << endl;
    /*for(int i=0; i<selected_events_angCut[0].size(); i++){
			cout << selected_events_angCut[1][i] << "	" << selected_events_angCut[2][i] << endl;
	}*/
}

/**********************************************************************************************************************************/
//PlotHitMinusTriggerTimeSingleBoard///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between the trigger time and each hit time of the events in the board under investigation////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotHitMinusTriggerTimeSingleBoard(UInt_t board, bool calibration, bool select_tracks){

    if(select_tracks==1) {
        DisplaySelectedTracks(-1, 0); 
    }
    
    prettycolors();
    gStyle->SetOptStat(1111);

    // Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;


    // Set the plot
    // adjust the range
    double t_min=0, t_max=0;
    if(calibration==0) {
        t_min=-500.;
        t_max=-100.;
    }
    else{
        t_min=-50.;
        t_max=50.;
    }
    
    hist2D_general[board-1]= new TH2D("timdiff","Timing offsets:time [ns]:channels",1000,t_min,t_max,512,-0.5,511.5);
    hist2D_general[board-1]->GetXaxis()->SetTitle("hit-trigger time [ns]");
    hist2D_general[board-1]->GetYaxis()->SetTitle("channel");
    
    // Define the loop size 
    int loop_size;
    if(select_tracks==1) {
        loop_size=selected_events.size();
    }
    else{
        loop_size=nentries;
    }
    
    // Loop over events
    for(int i=0;i<loop_size;i++){
        Events.clear();
        if(select_tracks==1) {
            LoadtheEntry(selected_events[i], data_tree, 0); // Load with clustering
        }
        else{
            LoadtheEntry(i, data_tree, 0); // Load with clustering
        }
        
        Event ev=Events.at(0);

        std::vector<double> times =ev.Gettimestamp();

        if(times.at(board-1)!=ULONG_MAX){
            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
            if (layers[reader.BoardID_config[board][3]].size() < 1) continue;
            Layer layer("");
            layer = ( layers[reader.BoardID_config[board][3]] ).at(reader.BoardID_config[board][4]-1);

            for(int ch=0; ch<512; ch++){
                if((layer.GetAmplitudes())[ch]>0)
                {
                    double Time=(layer.GetTimes())[ch];
                    double TrigTimeShift=times.at(board-1);
                    double ti =-TrigTimeShift+Time;
                    hist2D_general[board-1]->Fill(ti,ch);
                }
            }
        }
    }

    // Step 4 Plotting
    c = new TCanvas("c","Timing offsets",800,600);
    c->cd();
    hist2D_general[board-1]->Draw("colz");
    return c;
}


/**********************************************************************************************************************************/

TCanvas * Plotter::PlotHitMinusTriggerTimeAll(bool calibration, bool select_tracks){
    
    if(select_tracks==1) {
        DisplaySelectedTracks(-1, 0); 
    }

    // Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;

    // Set the 2D histograms
    int t_nbins = 1000;
    float t_min, t_max;
    if (calibration==1){
        t_min = -50.;
        t_max = +50.;
    }
    else{
        t_min = -500.;
        t_max = -100.;
    }

    for(UInt_t board=1;board<=12;board++){
        std::string hname = "timediff_" + std::to_string(board-1);
        std::string htitle = "BOARD " + std::to_string(board);
        hist2D_general[board-1]= new TH2D(hname.c_str(),htitle.c_str(),t_nbins,t_min,t_max,512,-0.5,511.5);
        hist2D_general[board-1]->GetXaxis()->SetTitle("hit-trigger time [ns]");
        hist2D_general[board-1]->GetYaxis()->SetTitle("channel");
        hname.clear();
        htitle.clear();
    }

    // Define the loop size 
    int loop_size;
    if(select_tracks==1) {
        loop_size=selected_events.size();
    }
    else{
        loop_size=nentries;
    }

    // Loop over events
    for(int i=0;i<loop_size;i++){

        Events.clear();
        if(select_tracks==1) {
            LoadtheEntry(selected_events[i], data_tree, 0); // Load with clustering
        }
        else{
            LoadtheEntry(i, data_tree, 0); // Load with clustering
        }
        Event ev=Events.at(0);

        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();
        for (unsigned int ind_layType = 0; ind_layType < (layers.size()); ind_layType++) {
            for (unsigned int ind_layNumber=0; ind_layNumber<((layers[ind_layType]).size()); ind_layNumber++){
                if(times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1) !=ULONG_MAX){
                    if (layers[reader.BoardID_config[reader.layer_BoardID[ind_layType][ind_layNumber]][3]].size() < 1) continue;
                    Layer layer("");
                    layer = (layers[ind_layType]).at(ind_layNumber);           

                    for(int ch=0;ch<512;ch++){
                        if((layer.GetAmplitudes())[ch]>0)
                        {
                            double Time=(layer.GetTimes())[ch];
                            double TrigTimeShift=times.at(reader.layer_BoardID[ind_layType][ind_layNumber]-1);
                            double ti = Time - TrigTimeShift;
                            hist2D_general[reader.layer_BoardID[ind_layType][ind_layNumber]-1]->Fill(ti,ch);
                        }
                    } // loop over chanel
                } //if condition
            } // loop over layers
        } //loop over layer types
    }// loop over events

    // Draw histo
    TCanvas *c1 = new TCanvas("c1","Timing offsets",800,1000);
    c1->Divide(2,6);
    prettycolors();
    
    for(int board=1; board<13; board++){
        c1->cd(reader.BoardID_config[board][3]+2*reader.BoardID_config[board][4]-1);
        hist2D_general[board-1]->Draw("colz");
        c1->Update();
    }

    return c1;
}






/**********************************************************************************************************************************/
// PlotChannelOffsetComparison       ///////////////////////////////////////////////////////////////////////////////////////////////
// Plots the difference between the time offset from two different calibration runs ////////////////////////////////////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotChannelOffsetComparison(string run1, string run2){

    hist_general[0]= new TH1D("Time Diff","Time Diff", 100, -2, 2);

    std::string path[2];
    path[0] = GetEnv("DATAPATH") + "RUN_" + run1 + "_0";
    path[1] = GetEnv("DATAPATH") + "RUN_" + run2 + "_0";
    
    double channelTimeOffset[2][12][512];
    double channelTimeOffset_invalid[2][12][512];

    // Read All
    std::string infilepath[2][12];
    std::ifstream chdelay[2][12];

    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            infilepath[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1] = path[0]+"/ch_VCSEL_risingEdge_"+to_string(reader.layer_BoardID[ind_layType][ind_layNumber])+".txt";
            infilepath[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1] = path[1]+"/ch_VCSEL_risingEdge_"+to_string(reader.layer_BoardID[ind_layType][ind_layNumber])+".txt";
            chdelay[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1].open(infilepath[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1].c_str());
            std::string line;
            if (chdelay[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1].is_open()){
                while(std::getline(chdelay[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1], line)){    // read one line from chdelay (ch | Toffset | bool)
                    std::stringstream streamline(line); // access line as a stream
                    // we need the first two columns (chanel and offset)
                    int column1;
                    double column2;
                    bool column3;
                    streamline >> column1 >> column2 >> column3;
                    channelTimeOffset[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1][column1] = column2;
                    channelTimeOffset_invalid[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1][column1] = column3;
                }
                //std::cout << "Store offset in p.ChTimeOffset[" << reader.layer_BoardID[ind_layType][ind_layNumber]-1 << "][ChNumber]" << std::endl;
            }
            else {
                cout << "Unable to open the offset file" << endl;
            }

            chdelay[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1].open(infilepath[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1].c_str());
            if (chdelay[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1].is_open()){
                while(std::getline(chdelay[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1], line)){    // read one line from chdelay (ch | Toffset | bool)
                    std::stringstream streamline(line); // access line as a stream
                    // we need the first two columns (chanel and offset)
                    int column1;
                    double column2;
                    bool column3;
                    streamline >> column1 >> column2 >> column3;
                    channelTimeOffset[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1][column1] = column2;
                    channelTimeOffset_invalid[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1][column1] = column3;
                }
                //std::cout << "Store offset in p.ChTimeOffset_1[" << reader.layer_BoardID[ind_layType][ind_layNumber]-1 << "][ChNumber]" << std::endl;
            }
            else {
                cout << "Unable to open the offset file" << endl;
            }
        }
    }
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            for(int ch=0;ch<512;ch++){
                double ChOffsetDiff;
                ChOffsetDiff = channelTimeOffset[0][reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch] - channelTimeOffset[1][reader.layer_BoardID[ind_layType][ind_layNumber]-1][ch];
                hist_general[0]->Fill(ChOffsetDiff);
                //std::cout << "OffsetDiff: " << ChOffsetDiff << std::endl;
            }
        } // loop over chanel
    }

    // Step 4 Plotting 
    c = new TCanvas("c","Example",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Counts");
    hist_general[0]->GetXaxis()->SetTitle("Time diff");
    hist_general[0]->Draw("hist");
    c->SaveAs("./CompareChOffsets.pdf");
    return c;
}



/**********************************************************************************************************************************/
//CalcTelescopeEfficiencyBeamSingleBoard()  Compute single board efficiency for one board and prints it on the screen (with beam data)
/**********************************************************************************************************************************/

void Plotter::CalcTelescopeEfficiencyBeamSingleBoard(TString lay, bool with_aligned ){

    Efficiency.clear();
    EfficiencyError.clear();
	
	// Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl; 
    
    float OV = 5.0;
    int pos = 0;
    //limit of accepted clusters around residual mean (e.g. limVal=10 => 10*Sigma)
	double limVal=20;

	// Step 2 Defining the paths and filenames
	std::string path = GetEnv("DATAPATH") + "RUN_" + run_number;
	std::string outfile_name="Efficiency_RUN"+run_number+"_pos"+to_string(pos).substr(0,2)+"_60Sig.txt";
	std::string logfile_name="Logfile_RUN"+run_number+"_pos"+to_string(pos).substr(0,2)+"_60Sig.txt";
	std::string inputfile_name=path + "/Residual.txt";	
    
    // Set plane 
    int layType_UnderTest = 0;
    if (lay[0] == (TString) "X"){
        layType_UnderTest = 0;
    }else if (lay[0] == (TString) "Y"){
        layType_UnderTest = 1;
    } else {std::cout << "Not yet implemented"; return ;}
		
	//Def arrays to store residual data in
	double boardnum[12]={0}, mean[12]={0}, sigma[12]={0}; 
	
	//reading in residual values
	ifstream infile; 
	infile.open(inputfile_name);
	while(!infile.eof()){
		for (int i=0; i<12; i++){
			infile >> boardnum[i];
			infile >> mean[i];
			infile >> sigma[i];
		}
	infile.close();
	}
	
    int board = -1;	
    int n_events_tot = 0; // total number of events in all other boards than layerUT
	int n_events_UT = 0; // 1-cluster with good position in layerUT

	//calculate expected zpos of hit
	std::vector<double> zpos ;
	double measured_hit_zpos = -1;
	for (unsigned int i = 0; 2*i < reader.n_layer_max; i++) {
		if (lay == reader.layer_name[layType_UnderTest][i]){
			measured_hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1];
			continue;
		}
		zpos.push_back(reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1]);
	}
	if (measured_hit_zpos < 0 ) {std::cout << "Invalid input"; return;}
    //cout << "This is board number: " << board << endl;
    // Loop over all event
    for(int i=0;i<nentries;i++){
		Events.clear();
		LoadtheEntry(i , data_tree, 1);
		Event ev=Events.at(0);
		std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        double measured_position = -1;

		// Use only tracks with 1 cluster in every plane (except layerUT) for efficiency test
		// One and only one cluster in every other plane! (layerUT can have more clusters)
		int accepted_layers = 0;
		for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
			//if (ind_layNumber != layNumber_UnderTest) {
            if (reader.layer_name[layType_UnderTest][ind_layNumber] != lay){
				Layer selected_layer("");							//selected layer = layer selected inside the loop to compare layer_UnderTest to
				selected_layer=(layers[layType_UnderTest]).at(ind_layNumber);
				std::vector<Cluster> clusters_selected =selected_layer.Getclusters_list();
				if (clusters_selected.size()==1) accepted_layers ++; // check on the number of clusters in layer - one and only one cluster accepted 
			}
		}//layernumber
		
        // check the efficiency only for clean tracks
		if (accepted_layers > 4) {
				
			//define clusters & track
			Layer layer("");
			std::vector<Cluster> clusters;
			Track* track = new Track(); 
					
			//Get position from the plane in question
			//Variables for finding the right cluster from mean value
			double mean_track = 0;
			int number_cluster = 0; 
					
			//loop over all boards to get hit value in every layer
			for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
			//if (ind_layNumber != layNumber_UnderTest){
                if (reader.layer_name[layType_UnderTest][ind_layNumber] != lay){

                    layer = (layers[layType_UnderTest]).at(ind_layNumber);       
                    clusters = layer.Getclusters_list();
						
	    		    if (clusters.size()==0) continue; //DON'T DELETE THIS OPTION! if no cluster: skip -> otherwise you will try to reconstruct track from no cluster -> error (vector out of range)
				    if (clusters.size()==1){
					    Cluster clu=clusters.at(0);
					    double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
					    if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
					    mean_track += hit_chpos;
					    number_cluster++;
				    }
                }
			}//layernumber
					
			// Get the mean_track value 
			mean_track /= number_cluster;

			//and loop again the layer number to reconstruct track
			for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
			    layer = (layers[layType_UnderTest]).at(ind_layNumber); 
			    clusters = layer.Getclusters_list();
						
			    if (clusters.size()==0) continue;

                //get measured hit position in DUT
                //if (ind_layNumber == layNumber_UnderTest){
                if (reader.layer_name[layType_UnderTest][ind_layNumber] == lay){
                    board = reader.layer_BoardID[layType_UnderTest][ind_layNumber];
                    unsigned int layNumber_UnderTest = ind_layNumber;
						    //if (clusters.size()==0) {
                            //    //measured_position = 0;  
                            //    continue; 
                            //}//DON'T DELETE THIS OPTION! if no cluster: skip -> otherwise you will try to reconstruct track from no cluster -> error (vector out of range)
				    if (clusters.size()==1) {
        			    Cluster clu=clusters.at(0);			
				        measured_position = Channel2AcPosition(clu.channelPosition_weightedMean());
				        if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
        		    }//cluster=1 
           					
                    else { //find the closest cluster 
          		   	    int ind_clu_selected = 0; 
    	    	   	    int diff = 1000;
        		   	    for (unsigned int ind_clu = 0 ; ind_clu < clusters.size() ; ind_clu++) { 
        		   		    Cluster clu=clusters.at(ind_clu);
				            double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
                       	    if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
        				    		
                            if (abs(hit_chpos - mean_track) < diff ) { // Choose this cluster
        		   			    ind_clu_selected = ind_clu; 
        		  			    diff = abs(hit_chpos - mean_track); 
        		   		    }    								
        		   	    }
        		   	    Cluster clu_selected=clusters.at(ind_clu_selected);
				        measured_position = Channel2AcPosition(clu_selected.channelPosition_weightedMean());
				        if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
                    }//closest cluster
                    continue;
                }//DUT

			    if (clusters.size()==1) {
			        Cluster clu=clusters.at(0);
                    // construct track from all layers
			        double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
			        if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
				    double hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][ind_layNumber] - 1];
				    track->AddDaughter(hit_chpos, hit_zpos, reader.layer_Position[layType_UnderTest][ind_layNumber]);
			    }//cluster=1 
		    }//layernumber
					
		    track->FitLinear();
            // Selection on track quality 
            // 1. Cut on primary track - exclude everything further than 5 mm
            if (!(track->CheckHitsOutofRadius(5))) continue; 
            // 2. Cut on track qualities
            if (!(track->GoodQuality())) continue; 
                    					
		    //calculate total number of "hits occuring in all 5 layers for same event" (for efficiency)
		    n_events_tot++;
            if (measured_position < 0) continue;
					
            //calc expected position in DUT
            double expected_position = track->LinearFunc->Eval(measured_hit_zpos);
		    //calculate abs diff between expected and measured pos (in mm) in DUT
		    double position_diff = abs(expected_position - measured_position);
					
            //calculate number of hits in DUT
		    if (position_diff<limVal*sigma[board-1]) {
                n_events_UT++;
		    }
	    }// end accepted_layers==5
    }//entries    
			
    cout<<"Calculating the efficiency of board " << board << endl;
    cout << "The total  number of events are " << n_events_tot << endl;
    cout << "The number of eventsin  DUT are " << n_events_UT << endl;

    //Calculate the efficiency
	double eff = ((double)n_events_UT) / (double)(n_events_tot);
    //Calculate efficiency error with binomial distribution 
    double eff_err = sqrt( eff * (1 - eff) / (double)(n_events_tot));

    cout << eff << " +- " << eff_err << endl;
} 
/**********************************************************************************************************************************/
//CalcTelescopeEfficiencyBeamAll()  
//Calculate efficiency for all boards using Residual for track selection (with beam data)
/**********************************************************************************************************************************/

void Plotter::CalcTelescopeEfficiencyBeamAll(bool with_aligned){
    
    Efficiency.clear();
    EfficiencyError.clear();
	
	// Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl; 
    
    float OV = 5.0;
    int pos = 4;
    //limit of accepted clusters around residual mean (e.g. limVal=10 => 10*Sigma)
	double limVal=10;

	// Step 2 Defining the paths and filenames
	std::string path = GetEnv("DATAPATH") + "RUN_" + run_number;
	std::string outfile_name="Efficiency_RUN"+run_number+"_pos"+to_string(pos).substr(0,2)+"_10Sig.txt";
	std::string logfile_name="Logfile_RUN"+run_number+"_pos"+to_string(pos).substr(0,2)+"_10Sig.txt";
	std::string inputfile_name=path + "/Residual.txt";	
		
	//Def arrays to store residual data in
	double boardnum[12]={0}, mean[12]={0}, sigma[12]={0}; 
	//def arrays to store min and max values for sigma cut of clusters
	double cutVal_min[12]={0}, cutVal_max[12]={0}; 
	
	//reading in residual values
	ifstream infile; 
	infile.open(inputfile_name);
	while(!infile.eof()){
		for (int i=0; i<12; i++){
			infile >> boardnum[i];
			infile >> mean[i];
			infile >> sigma[i];
		}
	infile.close();
	}
	
	// Select one layer to test  called    !! layNumber_UnderTest !!
	for (unsigned int layType_UnderTest=0; layType_UnderTest<reader.layer_name.size(); layType_UnderTest++){
		for (unsigned int layNumber_UnderTest=0; layNumber_UnderTest<((reader.layer_name.at(layType_UnderTest)).size()); layNumber_UnderTest++){
			int board = reader.layer_BoardID[layType_UnderTest][layNumber_UnderTest];
			TString lay = reader.layer_name[layType_UnderTest][layNumber_UnderTest];

			int n_events_tot = 0; // total number of events in all other boards than layerUT
			int n_events_UT = 0; // 1-cluster with good position in layerUT
		    int n_events_tracks_four = 0; //total number of events with one cluster in 4 accepted_layers

			//calculate expected zpos of hit
			std::vector<double> zpos ;
			double measured_hit_zpos = -1;
			for (unsigned int i = 0; 2*i < reader.n_layer_max; i++) {
				if (lay == reader.layer_name[layType_UnderTest][i]){
					measured_hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1];
					continue;
				}
				zpos.push_back(reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1]);
			}
			if (measured_hit_zpos < 0 ) {std::cout << "Invalid input"; return;}

            for(int i=0;i<nentries;i++){
				Events.clear();
				LoadtheEntry(i , data_tree, 1);
				Event ev=Events.at(0);
				std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
                double measured_position = -1;

				// Use only tracks with 1 cluster in every plane (except layerUT) for efficiency test
				// One and only one cluster in every other plane! (layerUT can have more clusters)
				int accepted_layers = 0;
				for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
					if (ind_layNumber != layNumber_UnderTest) {
						Layer selected_layer("");							//selected layer = layer selected inside the loop to compare layer_UnderTest to
						selected_layer=(layers[layType_UnderTest]).at(ind_layNumber);
						std::vector<Cluster> clusters_selected =selected_layer.Getclusters_list();
						if (clusters_selected.size()==1) accepted_layers ++; // check on the number of clusters in layer - one and only one cluster accepted 
					}
				}//layernumber
                // check how many tracks i have with 5 layers
			    if (accepted_layers > 3){
                    n_events_tracks_four++;
                }
				// check the efficiency only for clean tracks
				if (accepted_layers > 4) {
					
					//define clusters & track
					Layer layer("");
					std::vector<Cluster> clusters;
					Track* track = new Track(); 
					
					//Get position from the plane in question
					//Variables for finding the right cluster from mean value
					double mean_track = 0;
					int number_cluster = 0; 
					
					//loop over all boards to get hit value in every layer
					for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
						if (ind_layNumber != layNumber_UnderTest){

                            layer = (layers[layType_UnderTest]).at(ind_layNumber); 
						    clusters = layer.Getclusters_list();
						
						    if (clusters.size()==0) continue; //DON'T DELETE THIS OPTION! if no cluster: skip -> otherwise you will try to reconstruct track from no cluster -> error (vector out of range)
						    if (clusters.size()==1){
							    Cluster clu=clusters.at(0);
							    double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
							    if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
							    mean_track += hit_chpos;
							    number_cluster++;
						    }
                        }
					}//layernumber
					
					// Get the mean_track value 
					mean_track /= number_cluster;

					//and loop again the layer number to reconstruct track
					for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
						layer = (layers[layType_UnderTest]).at(ind_layNumber); 
						clusters = layer.Getclusters_list();
						
						if (clusters.size()==0) continue;

                        //get measured hit position in DUT
                        if (ind_layNumber == layNumber_UnderTest){
						    //if (clusters.size()==0) {
                            //    //measured_position = 0;  
                            //    continue; 
                            //}//DON'T DELETE THIS OPTION! if no cluster: skip -> otherwise you will try to reconstruct track from no cluster -> error (vector out of range)
					        if (clusters.size()==1) {
        						Cluster clu=clusters.at(0);			
				                measured_position = Channel2AcPosition(clu.channelPosition_weightedMean());
				                if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
        					}//cluster=1 
           					
                            else { //find the closest cluster 
          				    	int ind_clu_selected = 0; 
    	    			    	int diff = 1000;
        				    	for (unsigned int ind_clu = 0 ; ind_clu < clusters.size() ; ind_clu++) { 
        				    		Cluster clu=clusters.at(ind_clu);
						            double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
                                	if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
        				    		
                                    if (abs(hit_chpos - mean_track) < diff ) { // Choose this cluster
        				    			ind_clu_selected = ind_clu; 
        				    			diff = abs(hit_chpos - mean_track); 
        				    		} 								
        				    	}
        				    	Cluster clu_selected=clusters.at(ind_clu_selected);
				                measured_position = Channel2AcPosition(clu_selected.channelPosition_weightedMean());
				                if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
                            }//closest cluster
                            continue;
                        }//DUT

						if (clusters.size()==1) {
							Cluster clu=clusters.at(0);
                            // construct track from all layers
							double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
							if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
							double hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][ind_layNumber] - 1];
							track->AddDaughter(hit_chpos, hit_zpos, reader.layer_Position[layType_UnderTest][ind_layNumber]);
						}//cluster=1 
					}//layernumber
					
					track->FitLinear();
                    // Selection on track quality 
                    // 1. Cut on primary track - exclude everything further than 5 mm
                    if (!(track->CheckHitsOutofRadius(5))) continue; 
                    // 2. Cut on track qualities
                    if (!(track->GoodQuality())) continue; 
                    					
					//calculate total number of "hits occuring in all 5 layers for same event" (for efficiency)
					n_events_tot++;
                    if (measured_position < 0) continue;
					
                    //calc expected position in DUT
                    double expected_position = track->LinearFunc->Eval(measured_hit_zpos);
					//calculate abs diff between expected and measured pos (in mm) in DUT
					double position_diff = abs(expected_position - measured_position);
					
                    //cout << "The expected position is at " << expected_position << endl;
                    //cout << "The position difference is  " << position_diff << endl;
                    //cout << "The sigma expected from res " << sigma[board-1] << endl;
					
                    //calculate number of hits in DUT
					if (position_diff<limVal*sigma[board-1]) {
                        n_events_UT++;
					}
				}// end accepted_layers==5
			}//entries    
			
			cout<<"Calculating the efficiency of board " << board << endl;
			cout << "The total  number of events are " << n_events_tot << endl;
            cout << "The number of eventsin  DUT are " << n_events_UT << endl;

			//Calculate the efficiency
			double eff = ((double)n_events_UT) / (double)(n_events_tot);
        	Efficiency[board] = eff;
            //Calculate efficiency error with binomial distribution 
            double eff_err = sqrt( eff * (1 - eff) / (double)(n_events_tot));
			EfficiencyError[board] = eff_err;

			ofstream logfile (logfile_name, std::ios::out | std::ios::app);
			logfile << board <<"\t \t" << n_events_tot << "\t \t" << n_events_UT << "\t \t" << n_events_tracks_four << endl;
			logfile.close();
		}//layernumberUT
	}//layerTypeUT
	
	for(int i=1; i<13; i++) {cout << i << " " << Efficiency[i] << " +- " << EfficiencyError[i] << endl;}

	ofstream outfile (outfile_name);
	std::cout << "Creating the efficiency file: " << outfile_name << std::endl;
	if(outfile.is_open()){
	    for(int i=1; i<13; i++) {outfile << i << "\t" << Efficiency[i] << "\t" << EfficiencyError[i] << endl;}
        outfile.close();
	}
	else cout<<"Unable to open output file"<<endl;
}

/**********************************************************************************************************************************/
//PlotResidualAll()  
//Plot the residual for all layers and store in textfile
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotResidualAll(bool with_aligned){
	
	ResidualMean.clear();
	ResidualSigma.clear();

    // Step 1 Get the maximum entries 
    std::cout << "No. Entries: " << nentries << std::endl;
    
    // Step 2 Set the histograms
    for(unsigned int layType_UnderTest=0;layType_UnderTest<reader.layer_name.size();layType_UnderTest++){
        for (unsigned int layNumber_UnderTest=0; layNumber_UnderTest<reader.layer_name.at(layType_UnderTest).size();layNumber_UnderTest++){
            t[layType_UnderTest][layNumber_UnderTest]=new TH1D(reader.layer_name[layType_UnderTest][layNumber_UnderTest],reader.layer_name[layType_UnderTest][layNumber_UnderTest],200, -1, 1);
        }
    }
    // and defining the fit function
    TF1 *f1 = new TF1("gaus","gaus",-0.5,0.5);
    
    //Path to the directory where textfile is created
    std::string path = GetEnv("DATAPATH") + "RUN_" + run_number;
    std::string outfile_name=path + "/Residual.txt";

	//looping over all boards
	for(unsigned int layType_UnderTest=0;layType_UnderTest<reader.layer_name.size();layType_UnderTest++){
        for (unsigned int layNumber_UnderTest=0; layNumber_UnderTest<reader.layer_name.at(layType_UnderTest).size();layNumber_UnderTest++){
  
			int board = reader.layer_BoardID[layType_UnderTest][layNumber_UnderTest];
			TString lay = reader.layer_name[layType_UnderTest][layNumber_UnderTest];

			//calculate expected zpos of hit
			std::vector<double> zpos ;
			double measured_hit_zpos = -1;
			for (unsigned int i = 0; 2*i < reader.n_layer_max; i++) {
				if (lay == reader.layer_name[layType_UnderTest][i]){
					measured_hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1];
					continue;
				}
				zpos.push_back(reader.zpos[reader.layer_Position[layType_UnderTest][i] - 1]);
			}
			if (measured_hit_zpos < 0 ) {std::cout << "Invalid input"; return NULL;}
		
            for(int i=0;i<nentries;i++){
				// Setup
				Events.clear();
				LoadtheEntry(i, data_tree, 1);
				Event ev=Events.at(0);
				
				// Def var
				std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
				Layer layer("");
				std::vector<Cluster> clusters;
				Track* track = new Track(); 
			
				//Get position from the plane in question
				//Variables for finding the right cluster in DUT from mean value of other planes
				double mean_track = 0;
				int number_cluster = 0; 
				double measured_position=-1.; 
                
                for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
                    if (ind_layNumber != layNumber_UnderTest){
                      
                        layer = (layers[layType_UnderTest]).at(ind_layNumber); 
					    clusters = layer.Getclusters_list();

                        if (clusters.size()==0) break;
                        if (clusters.size()==1){
    						Cluster clu=clusters.at(0);
							
    						double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
    						if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
    						mean_track += hit_chpos;
    						number_cluster++;
                        }
					}	
				}//layernumber
					
				// Get the mean_track value 
				mean_track /= number_cluster;

				//loop over all boards to get hit value in every layer
				if (number_cluster == 5){ //only calculate residual if other 5 layers have one cluster
                    for (unsigned int ind_layNumber = 0; ind_layNumber < ((layers[layType_UnderTest]).size()); ind_layNumber++){ // run over 6 layers
                        
                        layer = (layers[layType_UnderTest]).at(ind_layNumber); 
					    clusters = layer.Getclusters_list();
						
					    if (clusters.size()==0) break; //DON'T DELETE THIS OPTION! if no cluster: skip -> otherwise you will try to reconstruct track from no cluster -> error (vector out of range)
                        
                        //get measured hit position in DUT
                        if (ind_layNumber == layNumber_UnderTest){
					        if (clusters.size()==1) {
        						Cluster clu=clusters.at(0);			
				                measured_position = Channel2AcPosition(clu.channelPosition_weightedMean());
				                if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
        					}//cluster=1 
           					
                            else { //find the closest cluster 
          				    	int ind_clu_selected = 0; 
    	    			    	int diff = 1000;
						     
        				    	for (unsigned int ind_clu = 0 ; ind_clu < clusters.size() ; ind_clu++) { 
        				    		Cluster clu=clusters.at(ind_clu);

						            double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
                                	if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
						    
        				    		if (abs(hit_chpos - mean_track) < diff ) { // Choose this cluster
        				    			ind_clu_selected = ind_clu; 
        				    			diff = abs(hit_chpos - mean_track); 
        				    		} 								
        				    	}
        				    	Cluster clu_selected=clusters.at(ind_clu_selected);
				                measured_position = Channel2AcPosition(clu_selected.channelPosition_weightedMean());
				                if (with_aligned) measured_position -= reader.layer_Shift[layType_UnderTest][layNumber_UnderTest];
                            }//closest cluster
                            continue;
                        }//DUT

                        //get hits from other 5 layers to get track
                        if (clusters.size()== 1) {
						    Cluster clu=clusters.at(0);
						    // construct track from all layers
						    double hit_chpos = Channel2AcPosition(clu.channelPosition_weightedMean());
						    if (with_aligned) hit_chpos -= reader.layer_Shift[layType_UnderTest][ind_layNumber];
						    double hit_zpos = reader.zpos[reader.layer_Position[layType_UnderTest][ind_layNumber] - 1];
						    track->AddDaughter(hit_chpos, hit_zpos, reader.layer_Position[layType_UnderTest][ind_layNumber]);
                        }
                    }//loop over 5 other layers to get track
                
		            track->FitLinear();
                    // Selection on track quality 
                    // 1. Cut on primary track - exclude everything further than 5 mm
                    if (!(track->CheckHitsOutofRadius(5))) continue; 

                    // 2. Cut on track qualities
                    if (!(track->GoodQuality())) continue; 

                    double residual;
                    if (track->LinearFunc->Eval(measured_hit_zpos) > 0 && measured_position > 0){
                        residual = track->LinearFunc->Eval(measured_hit_zpos) - measured_position;
				        t[layType_UnderTest][layNumber_UnderTest]->Fill(residual);
                    }
                }//layers==5
			}//events
		}//layNumber_UT
	}//layType_UT	

    // Step 4 Plotting and fitting
    c = new TCanvas("c","Residual",800,1200);
    c->Divide(2,6);
    c->SetTitle("Residual per channel");
    for(unsigned int ind_layType=0;ind_layType<reader.layer_name.size();ind_layType++){
        for (unsigned int ind_layNumber=0; ind_layNumber<reader.layer_name.at(ind_layType).size();ind_layNumber++){
            int board = reader.layer_BoardID[ind_layType][ind_layNumber];
            cout << "fitting residual for board: " << board << endl;
            c->cd(ind_layType+2*ind_layNumber+1);
            
            t[ind_layType][ind_layNumber]->Fit("gaus","R");
            double mean = f1->GetParameter(1);
            double sigma = f1->GetParameter(2);
            ResidualMean[board] = mean;
            ResidualSigma[board] = sigma;
            c->Update();
        }
    }
    //writing mean and sigma values into file
    ofstream outfile (outfile_name);
    std::cout << "Creating the file: " << outfile_name << std::endl;
    if(outfile.is_open()){
	    for(int i=1; i<13; i++) {outfile << i << "\t" << ResidualMean[i] << "\t" << ResidualSigma[i] << endl;}
	    outfile.close();
	}
	else cout<<"Unable to open output file"<<endl;
    return c;	
}
