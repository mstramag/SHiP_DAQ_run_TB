/* Decoder Program
 * Class for decoding STiC output
 * Optimise from SHiP testbeam software Oct2018
 * by S. Ek-In - surapat.ek-in@epfl.ch
 */

#include <iostream>
#include "../include/Decoder.hpp"
/////////////////////////////////////////////////////////////
///////////////////// Init && GetSet ////////////////////////
/////////////////////////////////////////////////////////////
Decoder::Decoder(uint64_t HexCode)
{
    set_Input(HexCode);

    // Set up trigger overall algor.
    TriggerCodeLast = "F";  
    TriggerCodeFirst = "C";
}

void Decoder::set_Input(uint64_t input)
{
    hex_line = input;
} 

uint64_t Decoder::get_Line()
{
    return hex_line;
}

/////////////////////////////////////////////////////////////
/////////////////////// Check Hexline ///////////////////////
/////////////////////////////////////////////////////////////

bool Decoder::is_Amp_Saturated()
{
    return Amp() == 0xFF;
}


/////////////////////////////////////////////////////////////
/////////////////////// Conversion //////////////////////////
/////////////////////////////////////////////////////////////

// ======== Time Conversion =======
// Explainations are welcome....
// These functions below are to convert to ns unit => need optimisation... 

double Decoder::HitRealTime(uint64_t hit_time, uint64_t fine_time , double board_frequency, double fine_bits) 
{
    // hit_time should be equivalent to decoder.Hit_Time()
    double HitTimeShiftMsb=(double)(((hit_time&0xFFFF8000)/(pow(2,15)-1.))*pow(2,15));
    double HitTimeShiftLsb=(double)(hit_time&0x7FFF);
    double HitTimeShift=(HitTimeShiftMsb+HitTimeShiftLsb)*(1./(4.*board_frequency/1000.));
    double finecnt=(double)(fine_time*1./((4.*board_frequency/1000.)*fine_bits));

    return HitTimeShift + finecnt;
}

double Decoder::TriggerRealTime(uint64_t trigger_time , double board_frequency)
{
    // trigger_time should be equivalent to decoder.Trigger_Time()
    double TriggerTMsb=(double)(((trigger_time&0xFFFF8000)/(pow(2,15)-1.))*pow(2,15));
    double TriggerTLsb=(double)(trigger_time&0x7FFF);
    return ((TriggerTMsb+TriggerTLsb)*(1./(4.*board_frequency/1000.)));
}



// ======== Trigger =======
// Used for trigger line

bool Decoder::is_Trigger()
{
    return ((hex_line&0xF000000000000000)>>60)==0xF;
}

bool Decoder::is_Header()
{
    return ((hex_line&0xFFFFFF0000000000)>>40)==0x00CDEF; 
}

uint64_t Decoder::Trigger_Counter() 
{
    return (hex_line >> 34) & 0x0000000000FFFFFF;
}

uint64_t Decoder::Trigger_Time() 
{
    return (hex_line & 0x3FFFFFFFF) << 2;
}

// ======== Configuration =======
// Used for hit line
uint32_t Decoder::Board_IP() 
{
    return (hex_line & 0xFC00000000000000) >> 58 ;
}

uint32_t Decoder::STiC_ID() 
{
    return (hex_line & 0x0380000000000000) >> 55;
}

uint32_t Decoder::Ch_ID() 
{
    return (hex_line & 0x007E000000000000) >> 49;
}

// ======== Time =======
uint64_t Decoder::Amp() 
{
    return (hex_line & 0x0001FE0000000000) >> 41;
}

uint64_t Decoder::Hit_Time() 
{
    // should be in ns
    // Should add Lsb and Msb scheme
    return (hex_line & 0x000001FFFFFFFFE0) >> 5;
}

uint64_t Decoder::Fine_Time() 
{
    return (hex_line & 0x000000000000001F) >> 0;
}

