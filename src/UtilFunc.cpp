/* Utilily Function
 * Dumpster for utility function
 * by S.Ek-In 
 * 15.01.2019
 */

#include <sstream>
#include <iostream>
#include <cstdlib>
#include "TString.h"
#include "TSpectrum.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TPaveStats.h"
#include "TStyle.h"
#include "TColor.h"


string GetEnv( const string & var ) {
     const char * val = ::getenv( var.c_str() );
     if ( val == 0 ) {
         return "";
     }
     else {
         return val;
     }
}

bool StartWith(string main_text, string pre_text) 
{   // Copy from Reader class - work the same
    // strncmp return 0 when main_text contains pre_text
    // add ! for conversion
    return !strncmp(main_text.c_str(), pre_text.c_str(), pre_text.size());
}

void spectrumPeakFinding(TH1 * h, std::vector<double>& peaks_out, std::vector<double>& errors, bool rebin_flag) {

    if (rebin_flag == 1) h->Rebin();  // used for timing fit to avoid fitting problem (fine time bin missing)
    // Step 1 Clear and define variables
    peaks_out.clear();
    errors.clear();
    
    TString tmp_canv_name = Form("%s_tmp_c",h->GetName());
    TCanvas * defc = new TCanvas(tmp_canv_name,tmp_canv_name);

    // Step 2 Search for TSpectrum
    TSpectrum * s = new TSpectrum();
    double sigma = 2;   // 
    s->Search(h,sigma,"nodraw",0.1); //search (const TH1 *hist, double_t sigma=2, option_t *option="", double_t threshold=0.05)
					//hist: pointer to the histogram of source spectrum
					//sigma: sigma of searched peaks,
					//threshold: (default=0.05) peaks with amplitude less than threshold*highest_peak are discarded. 0<threshold<1

    // Step 3 Peak finder
    unsigned int npeaks = s->GetNPeaks();
    if(npeaks>0) {
	    double *xpeaks = s->GetPositionX();
	    double *ypeaks = s->GetPositionY();

	    std::vector<std::pair<double,double>> positions;
	    positions.clear();

	    double maxY(0);
	    for(unsigned int peak = 0 ; peak < npeaks ; ++peak) {
	        std::pair<double,double> pos (xpeaks[peak],ypeaks[peak]);
	        positions.push_back(pos);
	    }
	    sort(positions.begin(), positions.end());
	   
	    // estimate peak width for fit
	    double width = 0;
	    if(npeaks>1) {
	        double dist = (positions.at(npeaks-1)).first - (positions.at(0)).first;
	        width = 0.75*(dist/(npeaks-1));
	    }
	    
	    for(unsigned int peak = 0 ; peak < npeaks ; ++peak)
	    {
	        // range for gaussian fit +/- = to peak width
	        double minf;
	        double maxf;
	        if(width > 0) {
	            minf = (positions.at(peak)).first-0.5*width;
	            maxf = (positions.at(peak)).first+0.5*width;
	        } else {
	            unsigned int bin_min(h->FindBin((positions.at(peak)).first)), bin_max(h->FindBin((positions.at(peak)).first));
	            double fraction = 0.5;
	            bool not_reach = true;
	            while(bin_min>1 && not_reach) {
	                --bin_min;
	                double bin_content = h->GetBinContent(bin_min);
	                if(bin_content<fraction*(positions.at(peak)).second) not_reach = false;
	            }
	            not_reach = true;
	            while(bin_max<((unsigned int)h->GetNbinsX()) && not_reach) {
	                ++bin_max;
	                double bin_content = h->GetBinContent(bin_max);
	                if(bin_content<fraction*(positions.at(peak)).second) not_reach = false;
	            }
	            minf = h->GetBinCenter(bin_min);
	            maxf = h->GetBinCenter(bin_max);
	        }
	        //~ cout << peak << "  " << minf << "  " << maxf << endl;
	        
	        TString ordered_fit("");
	        ordered_fit += peak;
	        ordered_fit = "peak " + ordered_fit;
	        TF1 * fit = new TF1(ordered_fit,"gaus",minf,maxf);
            fit->SetParameter(1,22);
	        fit->SetLineColor(kRed);
	        h->Fit(ordered_fit,"QR+");
	         
	        peaks_out.push_back(fit->GetParameter("Mean"));
	        errors.push_back(fit->GetParError(1));
	    }
    }
    TCanvas* canvas = (TCanvas*)gROOT->FindObject("c1");
    delete s;
    delete defc;
    return;
}

// Function that converts ch(0-512) to stic+ch(0-64)
std::pair <int,int> ch_to_sticch(int ch){
  int channel=0, stic=0;
  
  stic=((int)(ch/128))*2+(ch%2);
  channel=(ch-((int)(stic/2))*128-stic%2)/2;

  std::pair <int,int> stic_channel (stic,channel);
  return stic_channel;
}

// FindRisingEdge: Function that given a histogram, 
// returns the bin at which the rising edge starts
double FindRisingEdge(TH1 * h) {

    int maxBin = h->GetMaximumBin(); 
    int chosenBin = 0;

    for(int i=0; i<maxBin;i++){
        if( h->GetBinContent(i) >= 0.05 * h->GetBinContent(maxBin)){
            if(h->GetBinContent(i+1) > 0 && h->GetBinContent(i+2) > 0){
                chosenBin = i;
                break;
            }
        }
    }

    return h->GetBinCenter(chosenBin);
}

// Improved to_string function to add a precision
template <typename T>
std::string to_string_with_precision(const T value, const int n = 1)
{
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << value;
    return out.str();
}

// Introduces a nice color pattern in colz option for TH2
void prettycolors(){

 gStyle->SetPadGridY(1);
 gStyle->SetPadGridX(1);
 gStyle->SetOptStat(0);

  //this part just makes pretty colours
  const Int_t NRGBs = 5;
  const Int_t NCont = 255;

  double stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
  double  red[NRGBs]   = {0.00, 0.00, 0.87, 1.00, 0.51};
  double green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
  double  blue[NRGBs]  = {0.51, 1.00, 0.12, 0.00, 0.00};

  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);

}
