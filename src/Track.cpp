#include "../include/Track.hpp"
#include<iostream>
#include "TGraphErrors.h"
#include "TF1.h"
#include "math.h"

Track::Track()
{
}

Track::~Track()
{
    return;
}

void Track::FitLinear() {
    // Cast vector to array and track fit
    double data[Daughter.size()];
    double zpos_array[Daughter_zpos.size()];
    std::copy(Daughter.begin(), Daughter.end(), data);
    std::copy(Daughter_zpos.begin(), Daughter_zpos.end(), zpos_array);

    //for (unsigned int i = 0; i < Daughter.size(); i++){
    //    std::cout<< "Test daughter: " << Daughter[i] << " zpos: " << Daughter_zpos[i] << std::endl;
    //    std::cout<< "Input Test daughter: " << data[i] << " zpos: " << zpos_array[i] << std::endl;
    //}

    TF1 *linear = FitLinearTrack(zpos_array, data, 0, 0, Daughter.size());

}

TF1 * Track::FitLinearTrack(double *data_x, double *data_y, double *error_x, double *error_y, int data_size, bool silence) {

    // Step 1 Define param
    if (sizeof(data_x) != sizeof(data_y)){
        std::cout << "Wrong size array of data" << std::endl;
    }

    //for (unsigned int i = 0; i < 5; i++){
    //    std::cout << "data_x: " << data_x[i] << " data_y: " << data_y[i] <<  std::endl;

    //}
    //TGraphErrors *graph = new TGraphErrors((sizeof(data_x)/sizeof(*data_x)), data_x, data_y, error_x, error_y );
    TGraph *graph = new TGraph(data_size, data_x, data_y);

    // Step 2 Fit func 
    TF1 *linear = new TF1("linear", "[0] + [1]*x");
    if (silence) {
        graph->Fit(linear,"Q");  
    } else {
        graph->Fit(linear);  
    }

    // Step 3 export parameters
    Offset = linear->GetParameter(0); 
    OffsetError = linear->GetParError(0); 
    Slope  = linear->GetParameter(1); 
    SlopeError  = linear->GetParError(1); 
    LinearFunc = linear; 

    return linear;
}

bool Track::CheckHitsOutofRadius(double radius) { // radius in mm
    
    for (unsigned int ind_hits = 0; ind_hits < OtherHits.size() ; ind_hits++){
        double distance = fabs(LinearFunc->Eval(OtherHits_zpos[ind_hits]) - OtherHits[ind_hits]); 
        if (distance > radius) return false;
    }
    return true;
}

bool Track::GoodQuality(){
    if (LinearFunc->GetChisquare() > 0.01 ) return false; 
    return true;
}

double Track::Quality(){
    return LinearFunc->GetChisquare();
}

