#include "../src/Cluster.cpp"
#include "../include/Layer.hpp"
#include "TString.h"

Layer::Layer(TString name, int lay_pos)
{
    InitLayer(name);
    SetlayerPos(lay_pos);
}

Layer::~Layer()
{
    return;
}

TString Layer::Getlayername(){
    if(layername!="") return layername;
    else {/*std::cout<<"not initialised Layer"<<std::endl;*/ return "";}
}

std::vector<Cluster> Layer::Getclusters_list(){
    if(clusters_list.size()!=0) return clusters_list;
    else {/*std::cout<<"not initialised Layer"<<std::endl;*/ return clusters_list;}
}
unsigned int * Layer::GetAmplitudes(){
    return hits_position;
}
double * Layer::GetTimes(){
    return hits_time;
}

uint64_t * Layer::GetCoarseTimes(){
    return hits_coarse;
}

uint32_t * Layer::GetFineTimes(){
    return hits_fine;
}
void Layer::Setlayername(TString name){
    layername=name;
}

void Layer::SetlayerPos(int lay_pos){
    LayerPos=lay_pos;
}

void Layer::InitLayer(TString name){
    Setlayername(name);
    clusters_list.clear();
}
void Layer::AssignClusters(std::vector<Cluster> clusters){
    clusters_list=clusters;
}

void Layer::AssignHits(unsigned int hits_pos[1536], double hits_t[1536], uint64_t hits_coa[1536], uint32_t hits_fi[1536]){
    for(int i=0;i<1536;i++)
    {
        hits_position[i]=hits_pos[i];
        hits_time[i]=hits_t[i];
        hits_coarse[i] = hits_coa[i];
        hits_fine[i] = hits_fi[i];
        //if (hits_position[i] != 0)
        //{
        //    std::cout << "[In AssignHits] TEST hit position: " << hits_position[i] << std::endl;
        //}
    }
}





