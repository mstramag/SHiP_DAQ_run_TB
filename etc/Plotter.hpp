//
//  Plotter.hpp
//  
//
//  Created by stramaglia on 19/06/18.
//

#ifndef Plotter_hpp
#define Plotter_hpp

#include <TROOT.h>
#include <stdio.h>
#include <TFile.h>
#include <TString.h>
#include <iostream>
#include <TSystem.h>
#include "Event.hpp"
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TLine.h>

class Plotter{
public:
    //Global variables
    TString m_file;
    Int_t m_limit;
    std::vector<Event> Events;
    TCanvas *c;
    TH1 *t[2][4];
    TH2 *t2[2][4];
    TLine *b0[2][4];
    TLine *b1[2][4];

    TH1   *hist_general[10];
    TH2 *hist2D_general[10];

    TString layer_name[2][4] ; 

    int beamlowerlimit[2][4];
    int beamupperlimit[2][4];

    TString rootfile_tree;
    double clock_feq;
    double board_frequency;
    double fine_bits;

    double time_window;
    bool enabletiming;
    
    int BoardID_Map[2][4][3];

    double fibre_thickness;
    double gap_die;
    double gap_SiPM;
    double ch_width;
    double zpos[4];
    double z_offset;
    double radiants;
    int overlap; //overlap between two fibres (when stereo angle) in channels
    double board_offsetns[24];
    uint32_t board_map[24];

    // Configuration variables
    // 

    //========================

    Plotter();
    virtual ~Plotter();
    virtual void SetName(TString file_name, TString CfgFilename = "Configuration.txt");
    virtual void Init(TString file_name, TString CfgFilename = "Configuration.txt"); // Set Config file as default - changable after
    virtual Int_t CreateTree();
    virtual void LoadtheEntry(uint64_t evnum, TTree* fChain);
    virtual void DumpEvent(uint64_t evnum);
    TCanvas * PlotEvent(uint64_t evnum);
    TCanvas * PlotBeamSpot();
    TCanvas * PlotHitEfficiency(TString lay);
    TCanvas * PlotClusterBeamSpot();
    TCanvas * PlotClusterMultiplicity();
    TCanvas * PlotHitHitTime(TString lay);
    TCanvas * PlotChHitTime(TString lay);
    TCanvas * PlotTOTHitTime(TString lay);
    TCanvas * PlotBoardsTimeDiff(UInt_t board);
    TCanvas * PlotInnerTimeDiff(UInt_t board);
    TCanvas * PlotTrigTrigTime(UInt_t board);
    void      Position(int ch_layer,int layer,double &x,double &y,double &z);
    TCanvas * PlotClusterTOT();
    TCanvas * PlotInClusterTOT();	
    void      PrintTrigTime(uint64_t board);
    void      DumpFullEvents();
};//Plotter class

#endif /* Plotter_hpp */

#ifdef Plotter_cpp
Plotter::Plotter()
{
    //std::cout<<"max number of hits belonging to the same event is set to: "<<m_limit<<std::endl;
    std::cout<<"choose the filename using the SetName method"<<std::endl;
    c = NULL;
    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4 ; j++){
            t[k][j]  = NULL;
            t2[k][j] = NULL;
            b0[k][j] = NULL;
            b1[k][j] = NULL;
        }
    }

    for (unsigned int k=0; k<10; k++){
        hist_general[k]   = NULL;
        hist2D_general[k] = NULL;
    }

}

Plotter::~Plotter(){
    if(c) delete c;
    for(int k=0;k<2;k++){
        for (unsigned int j=0; j<4 ; j++){
            if(t[k][j])  delete t[k][j];
            if(t2[k][j]) delete t2[k][j];
            if(b0[k][j]) delete b0[k][j];
            if(b1[k][j]) delete b1[k][j];
        }
    }

    for (unsigned int k=0; k<10; k++){
        if (hist_general[k])   delete hist_general[k];
        if (hist2D_general[k]) delete hist2D_general[k];
    }

    return;
}


#endif // #ifdef Plotter_cpp
