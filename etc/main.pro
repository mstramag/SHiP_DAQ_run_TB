TEMPLATE = app
TARGET = analysis 
CONFIG += debug

CLASSES+= \
  Event \
  Layer \
  Cluster \
  Plotter \

SOURCES += \
  main.cpp

for(class, CLASSES) {
  HEADERS += include/$${class}.hpp
  SOURCES += $${class}.cpp
}

DEPENDPATH+= \

INCLUDEPATH += $$DEPENDPATH

include(analysis.pri)

LIBS += \
  -L$$DESTDIR \

