//
//  decoder.hpp
//  
//
//  Created by stramaglia on 19/06/18.
//

#ifndef decoder_hpp
#define decoder_hpp

#include <TROOT.h>
#include <stdio.h>
#include <TFile.h>
#include <TString.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <TSystem.h>
#include "Event.hpp"
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>

class decoder{
public:
    //Global variables
    TString m_file;

    decoder();
    virtual ~decoder();
    virtual void SetName(TString file_name);
    virtual Int_t dump();
};//decoder class

#endif /* decoder_hpp */

#ifdef decoder_cpp
decoder::decoder()
{
}

decoder::~decoder(){
}

#endif // #ifdef decoder_cpp
