#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include "include/Cluster.hpp"

Cluster::Cluster()
{
}

Cluster::Cluster(short int startChannel, unsigned int startAmplitude, double startTime,unsigned int SticID,unsigned int BoardID, uint64_t startCoarse, uint32_t startFine) :
m_startChannel(startChannel)
{
    m_amplitudes.push_back(startAmplitude);
    m_times.push_back(startTime);
    m_coarsetimes.push_back(startCoarse);
    m_finetimes.push_back(startFine);
    m_STiCIDs.push_back(SticID);
    m_BoardIDs.push_back(BoardID);
}

Cluster::~Cluster()
{
    clear();
}

unsigned int Cluster::clusterLength() const
{
    return m_amplitudes.size();
}

unsigned int Cluster::totalAmplitude() const
{
    unsigned int amp = 0;
    for (unsigned int i = 0 ; i < m_amplitudes.size() ; ++i)
        amp += m_amplitudes.at(i);
    return amp;
}

float Cluster::channelPosition() const
{
    // position of channel with highest entry
    if (m_amplitudes.size() == 0) {
        std::cout << "Channel with maximum amplitude couldn't be found since the cluster has a length of 0!" << std::endl;
        return 0;
    }
    
    short int maxChannel = -1;
    unsigned int maxAmp = 0;
    for (unsigned int i = 0; i < m_amplitudes.size(); ++i) {
        if (m_amplitudes.at(i) > maxAmp) {
            maxAmp = m_amplitudes.at(i);
            maxChannel = i;
        }
    }
    return maxChannel + m_startChannel;
}

float Cluster::channelPosition_weightedMean() const
{
    float weightedSum = 0.;
    float sumOfWeights = 0.;
    
    for (unsigned int i = 0; i < m_amplitudes.size(); ++i) {
        unsigned int amp = m_amplitudes.at(i);
        
        if (amp > 0.0f) {
            weightedSum += amp * (m_startChannel + i);
            sumOfWeights += amp;
        }
    }
    
    return sumOfWeights == 0.0f ? 0.0f : weightedSum / sumOfWeights;
}

void Cluster::addChannelToTheLeft(unsigned int amplitude,double time,unsigned int SticID, unsigned int BoardID,  uint64_t Coarse, uint32_t Fine)
{
    --m_startChannel;
    m_amplitudes.insert(m_amplitudes.begin(), amplitude);
    m_times.insert(m_times.begin(), time);
    m_coarsetimes.insert(m_coarsetimes.begin(), Coarse);
    m_finetimes.insert(m_finetimes.begin(), Fine);
    m_STiCIDs.insert(m_STiCIDs.begin(),SticID);
    m_BoardIDs.insert(m_BoardIDs.begin(),BoardID);
}

void Cluster::addChannelToTheRight(unsigned int amplitude,double time,unsigned int SticID, unsigned int BoardID, uint64_t Coarse, uint32_t Fine)
{
    m_amplitudes.push_back(amplitude);
    m_times.push_back(time);
    m_coarsetimes.push_back(Coarse);
    m_finetimes.push_back(Fine);
    m_STiCIDs.push_back(SticID);
    m_BoardIDs.push_back(BoardID);
}

void Cluster::clear()
{
    m_amplitudes.clear();
    m_times.clear();
    m_startChannel=-1;
    m_STiCIDs.clear();
    m_BoardIDs.clear();
    m_coarsetimes.clear();
    m_finetimes.clear();
}

void Cluster::dump() const
{
    //std::cout << "Cluster " << this << std::endl;
    for (unsigned int i = 0 ; i < m_amplitudes.size(); ++i)
        std::cout << "channel: " << std::dec<< m_startChannel+i << "\t amplitude: " << std::dec<< m_amplitudes.at(i) << "\t time: " << std::dec<< m_times.at(i) <<std::endl;
    std::cout << "pos: " << std::dec<< channelPosition() << " pos (wm): " << std::dec << channelPosition_weightedMean() << std::endl;
}

