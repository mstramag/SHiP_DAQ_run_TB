#!/usr/bin/env python
"""

Rewriting Plotter.cpp in python

run with pytest.doctest:
   >> pytest --doctest-module

"""

import re
import os
import ROOT
from glob import glob
from array import array

__author__ = 'Chitsanu Khurewathanakul'

#===============================================================================

class HexLine(str):
  """
  Represent single line of hex string, with intepretation as properties.

  >>> HexLine('72d42c339d500ab4')
  '72d42c339d500ab4'

  ## strip whitespace, force to lower case
  >>> HexLine('72d42c339d500ab4') == HexLine('  72D42C339D500AB4   ')
  True

  >>> HexLine('111111')
  Traceback (most recent call last):
  ...
  AssertionError: Invalid length 6 != 16

  >>> HexLine('not__hexadecimal')
  Traceback (most recent call last):
  ...
  ValueError: invalid literal for int() with base 16: 'not__hexadecimal'

  """

  def __new__(cls, raw):
    assert isinstance(raw, basestring)
    raw = raw.strip().lower()
    ## make sure the string is base16
    try: 
      int_val = int(raw, 16)
    except:
      raise
    assert len(raw) == 16, 'Invalid length %i != 16 (%s)'%(len(raw), raw)
    ## finally
    obj = super(HexLine, cls).__new__(cls, raw)
    obj.int_val = int_val
    return obj

  @property
  def is_header(self):
    """
    >>> HexLine('abcdef0361000004').is_header
    True

    >>> HexLine('fffc00040000415d').is_header
    False

    >>> HexLine('72d42c339d500ab4').is_header
    False
    """
    return self.startswith('abcdef')

  @property
  def is_trigger(self):
    """
    >>> HexLine('abcdef0361000004').is_trigger
    False

    ## Obsevation: FFFC only is NOT enough, 
    # it can goes monotonically up to fffc, fffd, fffe
    >>> HexLine('fffc00040000415d').is_trigger
    True
    >>> HexLine('fffd0003ad556198').is_trigger
    True

    >>> HexLine('72d42c339d500ab4').is_trigger
    False
    """
    return self.startswith('fff')

  @property
  def trigger_counter(self):
    return (self.int_val >> 34) & 0x000000000000FFFF

  @property
  def amp(self):
    """
    >>> HexLine('72d42c339d500ab4').amp
    22
    """
    return (self.int_val & 0x0001FE0000000000) >> 41

  @property
  def is_amp_saturated(self):
    return self.amp == 0xFF

  @property
  def time(self):
    """
    >>> HexLine('72d42c339d500ab4').time
    6927581269
    """
    return (self.int_val & 0x000001FFFFFFFFE0) >> 5

  @property
  def board_ip(self):
    """
    >>> HexLine('72d42c339d500ab4').board_ip
    28
    """
    return int((self.int_val & 0xFC00000000000000) >> 58)

  @property
  def stic_id(self):
    """
    >>> HexLine('72d42c339d500ab4').stic_id
    5
    """
    return int((self.int_val & 0x0380000000000000) >> 55)

  @property
  def ch_id(self):
    """
    >>> HexLine('72d42c339d500ab4').ch_id
    42
    """
    return int((self.int_val & 0x007E000000000000) >> 49)

  @property
  def fine_time(self):
    """
    >>> HexLine('72d42c339d500ab4').fine_time
    20
    """
    return int((self.int_val & 0x000000000000001F) >> 0)

  def is_valid_time(self, trigger_time, board_offset, threshold):
    """
    Return True if the time of this hit is within the threshold (ns).
    """
    diff = abs(trigger_time-self.time)
    return abs(board_offset-diff) < threshold

#===============================================================================

class TriggerBlock(tuple):
  """
  Represent a list of HexLine, such that
  - the first one `is_trigger==True`.
  - the header line is ignored.
  - hits with 

  >>> TriggerBlock([
  ...   'fffc00040000415d',
  ...   '7230500000e2f73d',
  ...   '72b0b00000e2e739',
  ...   'abcdef0001000001',
  ...   'abcdef0002000002',
  ...   '7211fe00285d3838',   # <--- AMP SATURATED
  ...   'abcdef0003000003',
  ...   '729c260033534e3a',
  ...   '721e2400335faec4',
  ...   'abcdef0004000002',
  ...   '729bfe0037d2276a',   # <--- AMP SATURATED
  ...   'abcdef0005000001',
  ...   'abcdef0006000004',
  ...   '723e26009c2525c0',
  ... ])
  TriggerBlock: nhits=7

  """
  def __new__(cls, other):
    lines = [HexLine(line) for line in other]
    assert lines[0].is_trigger, 'Expect first line.is_trigger == True'
    assert not any(line.is_trigger for line in lines[1:]), 'Incorrect block partitioning'
    # filter out the headers
    lines = [lines[0]] + [line for line in lines[1:] if not line.is_header]
    obj = super(cls, TriggerBlock).__new__(cls, lines)
    obj.trigger_counter_overflow = 0
    return obj

  def __repr__(self):
    return 'TriggerBlock %s -- id=%i, time=%i, nhits=%i'%(self[0], self.trigger_counter, self.trigger_time, len(self)-1)

  @property
  def trigger_time(self):
    """
    convert to 160MHz clock cycles
    """
    return (self[0].int_val & 0x3FFFFFFFF) << 2

  @property
  def trigger_counter(self):
    """
    Return trigger counter, taken into account how many times overflow occurs
    """
    return self[0].trigger_counter + (0xFFFF+1)*self.trigger_counter_overflow

  def valid_hits(self, board_offset, threshold):
    """
    Yield the hit (HexLine) in this block which is valid.
    
    - amp not saturated
    - hit diff within threshold

    """
    for hit in self[1:]:
      if not hit.is_amp_saturated:
        if hit.is_valid_time(self.trigger_time, board_offset, threshold):
          yield hit

#===============================================================================
# DOCUMENT
#===============================================================================

def is_blacklisted(line):
  """
  Known blacklisted line that can be ignored,
  so that other bad lines can be handled.
  """
  if line.startswith('***'):
    return True
  if line.startswith('No data received'):
    return True
  return False

class BoardDocument(object):
  """
  Represent a file containing the readout from single board.
  Prepare the reading into TTree and save to root.
  """

  def __init__(self, fpath, board_offset=0, threshold=1500):
    super(BoardDocument, self).__init__()
    self.fpath        = fpath
    self.board_offset = board_offset
    self.threshold    = threshold
    #
    self.board_id = int(re.search(r'arm_(\d+)_', fpath).group(1))
    assert 1 <= self.board_id <= 24, 'Inavlid board-ID: %i'%self.board_id
    self.target = './%s/rootfiles/%s.root' % os.path.split(self.fpath)

  @property
  def trigger_blocks(self):
    """
    Yield the trigger block as it traverse the document.
    """
    lines_buffer = None # don't start accepting yet
    with open(self.fpath) as fin:

      ## Loop over each line in the doc
      for i,line in enumerate(fin):
        if i <= 10: # skip first few header lines
          continue
        if i%100000==0:
          print('Reading line: %i'%i)
        if is_blacklisted(line):
          continue

        ## Collect the line into buffer
        hline = HexLine(line)
        if hline.is_trigger: # flush buffer
          if lines_buffer:
            block = TriggerBlock(lines_buffer)
            yield block
          lines_buffer = [line] # reset buffer
        else: # fill buffer
          if lines_buffer:
            lines_buffer.append(line)

    ## End-of-file, flush last buffer too
    yield TriggerBlock(lines_buffer)

  def _tree_init(self):
    """
    Preparing the TTree to persist.
    """

    ## Init
    self._file = ROOT.TFile(self.target, 'recreate')
    self._tree = ROOT.TTree('data', 'data tree')
    
    ## Prepare fields, same as the attributes of HexLine/TriggerBlock
    self._params = { # keep pointer to array, even though the val is const.
      'BoardID': array('I', [self.board_id]),
    }
    self._params_hit = {
      'time'     : array('L', [0]),
      'amp'      : array('I', [0]), # I = unsigned int (2 bytes)
      'stic_id'  : array('I', [0]),
      'ch_id'    : array('I', [0]),
      'fine_time': array('I', [0]),
    }
    self._params_block = {
      'trigger_time'   : array('L', [0]), # L = unsigned long (4 bytes)
      'trigger_counter': array('L', [0]),
    }

    ## Assign branch to TTree
    self._tree.Branch('BoardID'  , self._params['BoardID']              , 'BoardID/I') # CONST
    self._tree.Branch('TriggerID', self._params_block['trigger_counter'], 'TriggerID/L')
    self._tree.Branch('TriggerT' , self._params_block['trigger_time']   , 'TriggerT/L')
    self._tree.Branch('HitTime'  , self._params_hit['time']             , 'HitTime/L')
    self._tree.Branch('TOT'      , self._params_hit['amp']              , 'TOT/I')
    self._tree.Branch('STiCID'   , self._params_hit['stic_id']          , 'STiCID/I')
    self._tree.Branch('Ch'       , self._params_hit['ch_id']            , 'Ch/I')
    self._tree.Branch('FineTime' , self._params_hit['fine_time']        , 'FineTime/I')

  def _tree_fill(self, trigger_block, hex_line):
    """
    Given the hit (hex_line), bind value to the array & fill to tree
    """
    for key,arr in self._params_block.iteritems():
      arr[0] = getattr(trigger_block, key)
    for key,arr in self._params_hit.iteritems():
      arr[0] = getattr(hex_line, key)
    self._tree.Fill()

  def save_as_root(self):
    """
    Save this document as ROOT file,
    looping over each trigger block & process valid hits
    """

    ## Prepare the loop
    self._tree_init()
    last_trigger_counter = None
    count_trig_overflow = 0
    for block in self.trigger_blocks:

      ## Provide trigger-ID correction due to overflow
      if last_trigger_counter:
        if (last_trigger_counter % 0xFFFF) == 0 and (block.trigger_counter == 0):
          count_trig_overflow += 1
      block.trigger_counter_overflow = count_trig_overflow
      
      ## Assert continuity of counter
      if last_trigger_counter:
        assert block.trigger_counter - last_trigger_counter == 1, \
          'trig counter JUMP? %i --> %i'%(last_trigger_counter, block.trigger_counter)
      last_trigger_counter = block.trigger_counter

      ## Process the valid hits
      for hit in block.valid_hits(self.board_offset, self.threshold):
        self._tree_fill(block, hit)

    ## Finally
    self._tree.Write()
    self._file.Close()
    print('Saved: '+self.target)

#===============================================================================

def main():
  """
  Loop over all files found in the given search path,
  then convert each one into ROOT file.

  """
  search_path = './arm_*_ip'
  # search_path = '/home/lphe/rawdata/SciFi_StAl/RUN_00060/arm_*_ip'
  for fpath in glob(search_path):
    print('Loading: '+fpath)
    BoardDocument(fpath).save_as_root()

if __name__ == '__main__':
  main()
