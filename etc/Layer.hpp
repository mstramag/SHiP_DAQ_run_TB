//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan 30 22:17:17 2018 by ROOT version 6.12/04
// from TTree datastream/datastream
// found on file: STiCdata.root
//////////////////////////////////////////////////////////

#ifndef Layer_hpp
#define Layer_hpp

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
#include "Cluster.hpp"
#include <stdio.h>

// Header file for the classes stored in the TTree if any.

class Layer {
    
    public :
    
    TString                 layername;
    double                  timestamp;
    uint64_t                triggercounter;
    std::vector<Cluster>    clusters_list;
    unsigned int            hits_position[1536];
    double                  hits_time[1536];
    uint64_t                hits_coarse[1536]; 
    uint32_t                hits_fine[1536];
    
    
    Layer(TString name);
    ~Layer();
    TString                  Getlayername();
    double                   Gettimestamp();
    Int_t                    Gettriggercounter();
    std::vector<Cluster>     Getclusters_list();
    uint64_t                 *GetCoarseTimes();
    uint32_t                 *GetFineTimes();
    unsigned int             *GetAmplitudes();
    double                   *GetTimes();
    void                     Setlayername(TString name);
    void                     AssignClusters(std::vector<Cluster> clusters);
    void                     InitLayer(TString name);
    void                     AssignHits(unsigned int hits_pos[1536], double hits_t[1536], uint64_t hits_coa[1536], uint32_t hits_fi[1536]);
};

#endif

