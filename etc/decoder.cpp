#define decoder_cpp
//
//  decoder.cpp
//  
//
//  Created by stramaglia on 19/06/18.
//

#include "include/decoder.hpp"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <TChain.h>

#include <TH2.h>
#include <TH1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include "TApplication.h"
#include <iostream>
#include <climits>


using namespace std;

void decoder::SetName(TString file_name){
    
    m_file=file_name;
 
}

Int_t decoder::dump()
{
/*
    uint64_t hitdata;
    TString tmpfile="data/";
    tmpfile+=m_file;
    ifstream raw;
    std::cout<<tmpfile<<std::endl;
    Int_t nlines=0;
    string pippo;
    raw.open(tmpfile);
    while (!raw.eof())
    {
        getline(raw,pippo);
        if(pippo.find("#")!=std::string::npos || pippo.find("*")!=std::string::npos || pippo.find("a trigger:")!=std::string::npos)continue;
        uint64_t pippoint;
        std::stringstream strm(pippo);

        strm >> std::hex >> pippoint;
        
        nlines++;
        
        std::cout<<std::hex<<pippoint<<std::endl;
        
        if (!raw.good()) break;
    }
    raw.close();

*/
    
    std::vector<uint32_t> board_id;
    std::vector<uint32_t> stic_id;
    std::vector<uint32_t> ch_id;
    std::vector<uint32_t> amp;
    std::vector<uint64_t> hit_time;
    std::vector<uint32_t> fine_time;
    std::vector<uint64_t> TriggerT;
    bool arm1=false;
    uint32_t  board_map[24]={100,8,100, 100,25,100, 100,17,100, 100,27,100, 100,21,100,100,12,100,100,5,100, 100,24,100};
    
    //uint32_t  board_map[24]={100,2,100, ,10,16, 13,12,9, 27,11,1, 24,21,6, 100,100,100, 25,7,3, 8,5,29};

    uint64_t hitdata;
    TString tmpfile="/home/lphe/rawdata/SciFi_tb2/";
    tmpfile+=m_file;
    ifstream raw;

    Int_t nlines=0;
    
    raw.open(tmpfile);
    string pippo;
    bool fileend=true;
    while (!raw.eof())
    {
    if(!raw.eof()){
        getline(raw,pippo);
        if( raw.eof() ) break; 
        if(pippo.find("#")!=std::string::npos || pippo.find("*")!=std::string::npos || pippo.find("a trigger:")!=std::string::npos || pippo ==" "){
            std::cout<<pippo<<std::endl;
            continue;
        }
        std::stringstream strm(pippo);
        strm >>std::hex>>hitdata;
        
        uint64_t trig_time_now,trig_time_last,trig_time_diff,counter;
        uint32_t board_ip;
        uint32_t stic_id;
        uint32_t ch_id;
        uint32_t amp;
        uint64_t hit_time;
        uint32_t fine_time;
        uint64_t TriggerT;

        if(!hitdata)std::cout<<"no meaningful value"<<std::endl;
        //header
        else if(((hitdata&0xFFFFFF0000000000)>>40)==0xABCDEF) {
            std::cout<<"header"<<std::endl;
            continue;
        }
        
        //trigger
        else if (((hitdata&0xFFFC000000000000)>>50)==0x3FFF) {
            counter = (hitdata>>34)&0x000000000000FFFF;
            trig_time_now = ((hitdata&0x3FFFFFFFF)<<2);  // convert to 640MHz clock cycles
            //std::cout<<std::hex<<hitdata<<std::endl;
            printf("Trigger_time= %09llX\n",trig_time_now);
            printf("Counter= %04llX\n",counter);
            //if(arm1)std::cout<<"count "<<std::dec<<counter<<std::endl;
            continue;
        }
        
        //hit data
        else  {
            // reject time out events
            uint32_t testamp=(int)((hitdata&0x0001FE0000000000)>>41);
            //if(testamp!=0xFF)
            if(1)
            {
                
                board_ip=(int)((hitdata&0xFC00000000000000)>>58);  //6b
                stic_id=(int)((hitdata&0x0380000000000000)>>55);  //3b
                ch_id=(int)((hitdata&0x007E000000000000)>>49);  //6b
                amp=(int)((hitdata&0x0001FE0000000000)>>41);  //8b
                hit_time=(hitdata&0x000001FFFFFFFFE0)>>5;   //36b
                fine_time=(int)((hitdata&0x000000000000001F)>>0);   //5b
            
                uint32_t  board_id=0;
                bool check=false;
                for(int i=0;i<24;i++){
                    if(board_ip==board_map[i]){
                    board_id=i;
                        check=true;
                    }
                }
                if(!check){
                    std::cout<<"not found board "<<board_ip<<std::endl;
                }
                
                uint32_t  board_inMod=board_id%3;
                uint32_t  pos=((board_inMod)*512)+((int)(stic_id/2))*128+(ch_id*2)+(stic_id%2);
                
                    printf("Hit: board_id= %02d stic_id= %01d ch_id=  %03d ch in module =%03d amp= %03d hit_time= %09llX fine_time= %02X\n", board_id,stic_id,ch_id,pos,amp,hit_time,fine_time);
                
            }
        nlines++;
        }
        if (!raw.good()) break;
        if( raw.eof() ) break;
        //if(raw.eof())fileend=false;
        }
    }
    raw.close();
    
    
    
    return nlines;
}
