macx {
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.5
}
unix {
    QMAKE_CXXFLAGS += -fPIC
}

NO_QUSB {
    QMAKE_CXXFLAGS += -DNO_QUSB
}
NEWUSBBOARD_NO_QUSB {
    QMAKE_CXXFLAGS += -DNO_QUSB
    QMAKE_CXXFLAGS += -DNEW_USBBOARD
}

QMAKE_CXXFLAGS+= -std=c++11 -Wno-deprecated-register

CONFIG     += qt warn_on
CONFIG     += debug

# Seperate source & build dirs
DESTDIR     = $$PWD/Builds
OBJECTS_DIR = .tmp
MOC_DIR     = .tmp
UI_DIR      = .tmp
RCC_DIR     = .tmp

QMAKE_LFLAGS -= -Wl,--as-needed
