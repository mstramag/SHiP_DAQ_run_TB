//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Jan 30 22:17:17 2018 by ROOT version 6.12/04
// from TTree datastream/datastream
// found on file: STiCdata.root
//////////////////////////////////////////////////////////

#ifndef Event_hpp
#define Event_hpp

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
#include "Layer.hpp"
#include <stdio.h>

// Header file for the classes stored in the TTree if any.

class Event {
    
    public :
    
    uint64_t                                eventnumber;
    std::vector<double>                     timestamp;
    std::vector<uint64_t>                   triggercounter;
    std::vector<std::vector<Layer>>         layers_list;
    
    
    Event(uint64_t number, std::vector<double> time, std::vector<uint64_t> trigger);
    ~Event();
    uint64_t                            Geteventnumber();
    std::vector<double>                 Gettimestamp();
    std::vector<uint64_t>               Gettriggercounter();
    std::vector<std::vector<Layer>>     Getlayers_list();
    void                                Seteventnumber(uint64_t number);
    void                                Settimestamp(std::vector<double> time);
    void                                Settriggercounter(std::vector<uint64_t> trigger);
    void                                AssignLayers(std::vector<std::vector<Layer>> layers);
    void                                InitEvent(uint64_t number, std::vector<double> time, std::vector<uint64_t> trigger);
};

#endif

