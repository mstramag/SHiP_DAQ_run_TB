/*  The problem here is that ROOT must load all of cpp file
 *  --> Not practical if you want to test a file
 */

#include <TROOT.h>
#include "TString.h"

void loader(int run, TString time){
gROOT->ProcessLine(".L Cluster.cpp++");
gROOT->ProcessLine(".L Layer.cpp++");
gROOT->ProcessLine(".L Event.cpp++");
gROOT->ProcessLine(".L Plotter.cpp++");
/*gROOT->ProcessLine("Plotter p");

TString runn;
runn.Form("%d",run);
TString setoptions = "p.SetName(\"RUN_"+runn+"/\","+"\""+time+"\")";

gROOT->ProcessLine(setoptions);
//gROOT->ProcessLine("p.CreateTree()");
gROOT->ProcessLine("p.DumpEvent(1000)");
*/
}
