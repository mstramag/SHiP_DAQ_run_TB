#define Plotter_cpp
//
//  Plotter.cpp
//  
//
//  Created by stramaglia on 19/06/18.
//

#include "include/Plotter.hpp"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <TChain.h>

#include <TH2.h>
#include <TH1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <stdlib.h>
#include <string>
#include <stdlib.h>
#include "TApplication.h"
#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <climits>
#include "TMath.h"
#include <limits>

// Add assert for debugging
//#include <assert.h>  

using namespace std;


/**********************************************************************************************************************************/
//Init()/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Initailises the global variables //////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/



void Plotter::Init(TString file_name, TString CfgFilename){
    
    TString rootfilemid(file_name(0,file_name.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";
    rootfile_tree=rootfile; 
    gSystem->AccessPathName(rootfile);
    if(gSystem->AccessPathName(rootfile))
        std::cout<<"no file with name "<<rootfile<<", to create one, call CreateTree"<<std::endl;
    //else std::cout<<"the .root file exists, plot the variables! To load the tree, LoadtheTree"<<std::endl;
    
    // Step 1 Read the config file
    
    std::ifstream file_cfg(CfgFilename);
    std::string line; 
    std::istringstream buffer;
    unsigned int flag = 0 ; // flag for reading separated part
    unsigned int n_layer = 0;
    unsigned int n_plane = 0; // 0 = X, 1 = Y
    unsigned int n_board = 0;
    int BoardID_Map_temporary[24];

    //assert(file_cfg.is_open()); // Will raise an error if the file is not opened

    // Step 2 Assign to global variables - Need 2 steps reader
    //                                          1.) Det. Config
    //                                          2.) Constants 

    while (std::getline(file_cfg >> std::ws, line)){
        // Ignore lines containing # 
        if (line.find("#") != std::string::npos) continue;
        if (line.find("Constants") != std::string::npos) break;
    
        // Step 0. buffer a line to a string stream (ss) - For holding the line value 
        std::stringstream ss(line);
        std::string buffer_line; 

        // Step 1. read board config. - the 1st matrix -- Flag == 0
        //read in format X1 X2 Y1 Y2 Y3 Y4 X3 X4 -> change to X1 X2 X3 X4 Y1 Y2 Y3 Y4 only for BoardID Map 
        if (flag == 0){
            // Read BoardID
            std::getline(ss, buffer_line, ',');
            buffer.str(buffer_line);
            buffer >> BoardID_Map_temporary[n_board];
            buffer.clear();
             
            //Read BoardIP
            std::getline(ss, buffer_line, ',');
            buffer.str(buffer_line);
            buffer >> board_map[n_board];
            buffer.clear();
             
            // Read Board Offset
            std::getline(ss, buffer_line, ',');
            buffer.str(buffer_line);
            buffer >> board_offsetns[n_board];
            buffer.clear();
             
            //std::cout << line << " " << BoardID_Map_temporary[n_board] << " " << board_map[n_board] << " " << board_offsetns[n_board] << std::endl;
            n_board++;
            if (n_board == 24) flag = 1;
        }
        
    
        // Step 2. read Beamspot position - the 2nd matrix -- Flag == 1
        if (line.find("beam_low") != std::string::npos ) flag = 1 ; // Search for th beam_low line and start looping 8 layers 

        if (flag == 1) {
            if (line.find("Y") != std::string::npos) n_plane = 1;

            std::getline(ss, buffer_line, ','); // 
            buffer.str(buffer_line);
            buffer >> layer_name[n_plane][n_layer%4]; // n_plane = n_layer/4 or n_layer > 3
            buffer.clear();

            std::getline(ss, buffer_line, ','); 
            buffer.str(buffer_line);
            buffer >> beamlowerlimit[n_plane][n_layer%4];
            buffer.clear();

            std::getline(ss, buffer_line, ','); 
            buffer.str(buffer_line);
            buffer >> beamupperlimit[n_plane][n_layer%4];
            buffer.clear();

            n_layer++;
            if (n_layer == 8) flag = 2;
        }        

        // Step 3. Z position offset -- Flag == 2 
        if (line.find("zPositionOffset") != std::string::npos) {
            ss.str(line.substr(line.find("=")+1));
            //assert(ss=="0.,18.05,39.5,57.55"); // this line is for checking - must delete after 
            //std::cout << line.substr(line.find("=")) << std::endl;
            //std::cout << ss << std::endl;
            for (unsigned int i_zpos = 0; i_zpos < 4 ; i_zpos++){
                std::getline(ss,buffer_line, ',');
                buffer.str(buffer_line);
                buffer >> zpos[i_zpos]; 
                //std::cout << "ZPOS " << i_zpos << " " << zpos[i_zpos] << std::endl; 
                buffer.clear();
            }
        }
            

    }

    // Swapping S1 S2 S3 S4 S5 S6 S7 S8
    for (unsigned int k = 6; k < 12; k++) {
        std::swap(BoardID_Map_temporary[k], BoardID_Map_temporary[k+12]); // Swap S3 S4 with  S7 S8
        std::swap(BoardID_Map_temporary[k+6], BoardID_Map_temporary[k+12]); // S1 S2 S7 S8 S5 S6 S3 S4 -> Swap S5 S6 with  S3 S4
    }

    for(int k=0;k<2;k++)for(int j=0;j<4;j++)for(int h=0;h<3;h++) BoardID_Map[k][j][h]=BoardID_Map_temporary[k*2+j*4+h*3];
/*
    // ===== Board config ===== 
    TString layer_name_temporary[2][4] = {{"X1","X2","X3","X4"},{"Y1","Y2","Y3","Y4"}};
    int beamlowerlimit_temporary[2][4]={{800,780,680,680},{750,750,770,0}};
    int beamupperlimit_temporary[2][4]={{850,840,750,750},{850,850,870,0}};

    // X1 X2 X3 X4 Y1 Y2 Y3 Y4 
    int BoardID_Map_temporary[2][4][3] = { { {1,2,3},  {4,5,6}, {19,20,21}, {22,23,24} }, { {7,8,9}, {10,11,12}, {13,14,15}, {16,17,18}}};
    // X1 X2 Y1 Y2 Y3 Y4 X3 X4 
    uint32_t board_map_temporary[24]={100,8,100, 100,25,100, 100,17,100, 100,27,100, 100,21,100,100,12,100,100,5,100, 100,24,100};
    double board_offsetns_temporary[24]={0,441,0,0,364,0,0,404,0,0,404,0,0,441,0,0,444,0,0,404,0,0,451,0};
    double zpos_temporary[4]={0., 18.05, 39.5, 57.55};

    for(int k=0;k<2;k++)for(int j=0;j<4;j++)layer_name[k][j]=layer_name_temporary[k][j];
    for(int k=0;k<2;k++)for(int j=0;j<4;j++)beamlowerlimit[k][j]=beamlowerlimit_temporary[k][j];
    for(int k=0;k<2;k++)for(int j=0;j<4;j++)beamupperlimit[k][j]=beamupperlimit_temporary[k][j];
    for(int k=0;k<24;k++)board_map[k]=board_map_temporary[k];
    for(int k=0;k<24;k++)board_offsetns[k]=board_offsetns_temporary[k];
    for(int k=0;k<2;k++)for(int j=0;j<4;j++)for(int h=0;h<3;h++)BoardID_Map[k][j][h]=BoardID_Map_temporary[k][j][h];
    for(int k=0;k<4;k++)zpos[k]=zpos_temporary[k];

*/
    // ===== Constants =====
    // The configuration file has a format -  "key = value"
    // ** found other simple way to implement this - more brillient ideas are welcome 
    // SciFi SiPM characteristic
    while (std::getline(file_cfg >> std::ws, line)){
        //line = line >> std::ws; // clear space and tab
        if (line.find("Constants") != std::string::npos) continue;
        buffer.str(line.substr(line.find("=")+1)); // read value   

        if (line.find("gap_die") != std::string::npos) {
            buffer >> gap_die; 
        }
        else if (line.find("gap_SiPM") != std::string::npos) {
            buffer >> gap_SiPM; 
        } 
        else if (line.find("ch_width") != std::string::npos) {
            buffer >> ch_width; 
        } 
        else if (line.find("fibre_thickness") != std::string::npos) {
            buffer >> fibre_thickness; 
        } 
        else if (line.find("z_offset") != std::string::npos) {
            buffer >> z_offset ; 
        } 
        else if (line.find("radiants") != std::string::npos) {
            buffer >> radiants ; 
            radiants = radiants/180*TMath::Pi();
        } 
        else if (line.find("overlap") != std::string::npos) {
            buffer >>  overlap; 
        } 
        else if (line.find("time_window") != std::string::npos) {
            buffer >> time_window ; 
        } 
        else if (line.find("board_frequency") != std::string::npos) {
            buffer >> board_frequency ; 
        } 
        else if (line.find("fine_bits") != std::string::npos) {
            buffer >> fine_bits ; 
        } 
        else if (line.find("enabletiming") != std::string::npos) {
            buffer >>  enabletiming; 
        } 
        else if (line.find("clock_feq") != std::string::npos) {
            buffer >>  clock_feq; 
        } 
        else {
            std::cout << "Error! Do you add a new constant?" << std::endl; 
        }
        buffer.clear();
    }

    // Need checking
    //std::cout<< gap_die << " " << clock_feq << " " << radiants << std::endl;
/*
    //assert(enabletiming == 0);
    //assert(fibre_thickness == 0.250); 
    gap_die=0.220;
    gap_SiPM=0.40;
    ch_width=0.250;
    fibre_thickness=0.250;
    z_offset=160.;
    radiants=2.5/180*TMath::Pi();
    overlap=72;
    time_window=9;

    // Board characteristic
    board_frequency=136;
    fine_bits=32;
    enabletiming=0;
    clock_feq = 1.84; // Clock is 680 MHz 
*/


}



/**********************************************************************************************************************************/
//SetName()/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Sets the run you want to analyse /////////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::SetName(TString file_name, TString CfgFilename){
    
    m_file=file_name;
    TString tmpfile = getenv("DATAPATH_OCT18");
    tmpfile+="/"+m_file;
    ifstream raw;
    raw.open(tmpfile);
    Init(file_name, CfgFilename);
}



/**********************************************************************************************************************************/
//CreateTree()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CreateTree is the Event builder. From the data files coming from the 24 (or less) boards, hits belonging /////////////////////////
//to the same trigger count are merged. The assumption is that boards never miss to receive a trigger //////////////////////////////
/**********************************************************************************************************************************/

Int_t Plotter::CreateTree()
{

    //the first trigger that the board receives is the Reset, this flag is used to discard all hits coming before the second trigger
    bool flagcntone[24]={0};
    
    
    //define the root output file
    TFile *outputfile=new TFile(rootfile_tree,"RECREATE");
    TTree *tree= new TTree("data","data tree");
    
    //infos to be stored in the tree
    std::vector<uint32_t> board_id;     //physical id of the boards (1-24)
    std::vector<uint32_t> stic_id;      //identification number of the stic position inside one board (0-3)
    std::vector<uint32_t> ch_id;        //channel number in the stic (0-63)
    std::vector<uint32_t> amp;          //time over threshold of the hit
    std::vector<uint64_t> hit_time;     //coarse arrival time of the hit
    std::vector<uint32_t> fine_time;    //fine arrival time of the hit
    std::vector<uint64_t> TriggerT;     //trigger time per very board
    
    Int_t Eventnum=0;                   //ID of the current event
    std::vector<UInt_t> TriggerID;      //Identification number of the trigger (trigger count on the board)
    
    //Definition of the Branches that we want to store inside the tree
    tree->Branch("BoardID",&board_id);
    tree->Branch("STiCID",&stic_id);
    tree->Branch("Ch",&ch_id);
    tree->Branch("TOT",&amp);
    tree->Branch("HitTime",&hit_time);
    tree->Branch("Eventnum",&Eventnum);
    tree->Branch("TriggerID",&TriggerID);
    tree->Branch("FineTime",&fine_time);
    tree->Branch("TriggerT",&TriggerT);
    
    //flags that will be useful to align the triggers and the hits in all the boards
    UInt_t TriggerCheck[24]={0};
    UInt_t Overroll[24]={0};
    UInt_t TriggerCnt[24]={0};
    UInt_t TriggerCntLast[24]={0};
    
    //path where to find the data files
    TString tmpfile=getenv("DATAPATH_OCT18");
    
    cout << "     " << tmpfile << endl;
    tmpfile+="/"+m_file;
    cout << "     " << tmpfile << endl;
    ifstream raw;
    
    //Array of data converted from the lines for the boards.
    std::vector<uint64_t> filevector[24];
    for(UInt_t i=0;i<24;i++) {filevector[i].clear();} // Clean the filevector - this should contain data
    
    
    UInt_t tot_number_events=0;
    
    DIR* dirp = opendir(tmpfile);
    struct dirent * dp; //pointer to the directory
    
    // tmpfile - data/
    // RUN - dirp - loop over directory in tmpfile
    // list of files in RUN - dp - list of files in dirp
    // log file - tmpfilefile - data log file
    // load all the files infos in vectors - filevectors - (one for every board)
    UInt_t Trigcounter = 0;
    while((dp = readdir(dirp)) != NULL){   
        
        TString tmpfilefile=tmpfile+dp->d_name;
        string che(tmpfilefile);
        
        if(che.find("arm_")!=std::string::npos){
            std::cout<<"reading file: "<<dp->d_name<<std::endl;
            
            // Read the id of the board
            Int_t b=-1; //b is the physical board id
            // UInt_t c=0;
            std::string sub = dp->d_name;
            sub = sub.substr(4,6);
            b=stoi(sub);
            std::cout<<"board number: "<<b<<std::endl;
            
            // Declare variables to read the data file
            raw.open(tmpfilefile);
            string hitdata_string; //this string is used as temporary container of each line in the file
            Trigcounter = 0; //Trigger counter coming from the board counter
            
            // Read the file
            while (!raw.eof()) {
                
                // Openning this file and read data
                // Get line
                getline(raw,hitdata_string);
                if( raw.eof() ) break;  
                uint64_t hitdata_stringint; //used to convert the string into number
                std::stringstream strm(hitdata_string);
                strm >>std::hex>>hitdata_stringint;
                // Trigger
                // The trigger word is flagged in the data through the identifier "FFFC"
                if (((hitdata_stringint&0xFFFC000000000000)>>50)==0x3FFF) {
                    if(b!=-1 && flagcntone[b-1]){filevector[b-1].push_back(hitdata_stringint);}
                    
                    // Extract the counter from the trigger line
                    Trigcounter=(hitdata_stringint>>34)&0x000000000000FFFF;
                              
                    if(b!=-1 && (Trigcounter>0 || filevector[b-1].size()>0))flagcntone[b-1]=1; //If the first event has been overcome, the flag to save the data is enabled
                    if(b!=-1){
                        // even in the assumption that all the boards receive all triggers, it can happen that the trigger information is not sent from the board
                        // the trigger counter has as well 16 bits available, so after 65536 events is overrolls, we do not want to restart the numeration for the events
                        // these lines take care of all the possible combinations that can happen among boards, and try to align the events coming from the single boards
                        // here is where the event building is actually done
                        if(Trigcounter>TriggerCntLast[b-1] && (Trigcounter-TriggerCntLast[b-1])>1) TriggerCnt[b-1]+=Trigcounter-TriggerCntLast[b-1];
                        else TriggerCnt[b-1]++;
                        TriggerCntLast[b-1]=Trigcounter;
                        if(flagcntone[b-1])filevector[b-1].push_back(Trigcounter);
                        if(Trigcounter>TriggerCheck[b-1])TriggerCheck[b-1]++;
                        else{Overroll[b-1]++; TriggerCheck[b-1]=Trigcounter;}
                    }
                }
                //discard all lines that do not contain data
                else if(hitdata_string.find("#")!=std::string::npos || hitdata_string.find("*")!=std::string::npos || hitdata_string == " " || hitdata_string.find("ZEROs")!=std::string::npos){
                }
                //the last case is the hits information
                else{
                    uint64_t hitdata_stringint; //used to convert the string into number
                    std::stringstream strm(hitdata_string);
                    strm >>std::hex>>hitdata_stringint;
                    if(b!=-1 && flagcntone[b-1]){filevector[b-1].push_back(hitdata_stringint);}
                }
                if (!raw.good()) break;
                // =*=*=*=*=*=*=*=*=*=**=*= End of reading file =*=*=*=*=*=*=*=*=*=*=*=*
            }
            std::cout << "BOARD: " << b << " Trigcounter: " << Trigcounter << std::endl;
            raw.close();//close board file
            
        }//if arm
    }//while inside the directory
    closedir(dirp); //close the directory
    
    // At this point, 24 vectors are filled with trigger time, trigger count and hit information for all the boards
    
    bool save=true; //flag to choose if one event will be saved
    
    for(int j=0;j<24;j++)if(TriggerCnt[j]>tot_number_events)tot_number_events=TriggerCnt[j];        // total number of events registered, taken as the maximum number of events registered by one board
    UInt_t max_Overrolls=0;                                                                         // maximum number of times that one counter overrolled
    for(int j=0;j<24;j++)if(Overroll[j]>max_Overrolls)max_Overrolls=Overroll[j];
    
    // sanity check printouts
    for(int j=0;j<24;j++){std::cout<<"Triggers for board: "<<j+1<<" "<<TriggerCnt[j]<<std::endl;}
    std::cout<<"Events collected are: "<<tot_number_events<<std::endl;
    std::cout<<"Overrolls are: "<<max_Overrolls<<std::endl;
    
    //second step is -
    //read the vectors to build the events
    
    UInt_t stop_point[24]={0};                                                      //integer used to "remember" the vector element with the last hit in one event
    UInt_t Triggerc[24]={1};                                                        //trigger counter
    
    uint64_t trig_time_now[24];//,trig_time_last[24],trig_time_diff[24];               //trigger time in a given event, trigger time in the last event, difference between the two
    
    UInt_t Overrolls_board[24]={0};                                                 //counter of overrols per board until all the boards employed overrolled
    
    for(UInt_t l=0;l<tot_number_events;l++){
        int global_Overroll=0;                                                      //flag to check if all boards got to the overroll
        for(int b=0;b<24;b++){global_Overroll+=Overrolls_board[b];}
        if(global_Overroll>=7)for(int b=0;b<24;b++){Overrolls_board[b]=0;/*save=false; std::cout<<"save at false for Overroll"<<std::endl;*/} //in case the run is split into spills, the overroll is the new reset
        
        // cleaning the vectors at the beginning of every event
        
        TriggerT.clear();
        TriggerID.clear();
        for(int i=0;i<24;i++){
            TriggerT.push_back(100000000);
            TriggerID.push_back(100000000);
        }
        
        board_id.clear();
        stic_id.clear();
        ch_id.clear();
        amp.clear();
        hit_time.clear();
        fine_time.clear();
        
        for(UInt_t bo=0;bo<24;bo++){ //for loop on the boards
            int eventseparator=0;
            
            Int_t next=100000;
            
            if(Overrolls_board[bo])continue;
            
            for(UInt_t le=stop_point[bo]; le<filevector[bo].size();le++){
                uint64_t hitdata=filevector[bo].at(le);
                bool foundtrigger=false;
                //header
                if(((hitdata&0xFFFFFF0000000000)>>40)==0xABCDEF) {
                    continue;
                }
                
                //trigger
                else if (((hitdata&0xFFFC000000000000)>>50)==0x3FFF) {
                    eventseparator++;
                    //case in which one board missed to send a trigger
                    if(eventseparator==1 && le!=filevector[bo].size()-1 && filevector[bo].at(le+1) >Triggerc[bo]+1){
                        eventseparator++;
                        Triggerc[bo]++;
                        }
                    //case in which a Reset signal was sent or the counter reached its end and started again
                    if(eventseparator==1 && filevector[bo].at(le+1) <Triggerc[bo]+1 && Triggerc[bo]!=0){eventseparator++; Triggerc[bo]=filevector[bo].at(le+1);
                        }
                    //normal case (the trigger count is increasing monotone)
                    if(eventseparator==1)
                    {
                        foundtrigger=true;
                        Triggerc[bo]=filevector[bo].at(le+1);
                        trig_time_now[bo] = ((hitdata&0x3FFFFFFFF)<<2);  // convert to 170MHz clock cycles or equivalent
                        //trig_time_diff[bo] = trig_time_now[bo]-trig_time_last[bo];
                        //trig_time_last[bo] = trig_time_now[bo];
                        
                        TriggerT.at(bo)=trig_time_now[bo]; // 680MHz or equivalent
                        TriggerID.at(bo)=(int)filevector[bo].at(le+1);
                        le++;
                    }
                    //trig_time_last[bo]=trig_time_now[bo];
                    if(eventseparator>1){
                        if(filevector[bo].at(le+1) <Triggerc[bo]+1){
                            Overrolls_board[bo]++;
                            Triggerc[bo]=-1;
                            }
                        stop_point[bo]=le-1;
                        le=filevector[bo].size();
                        continue;
                    }

                }
                if(foundtrigger){
                    //look for data between one trigger and the following
                    for(int k=le+1;k<next+(int)le+1;k++){
                            if(k<0 || ((unsigned int)(k))>=filevector[bo].size())continue;
                            uint64_t hitdatad=filevector[bo].at(k);
                            //header
                            if(((hitdatad&0xFFFFFF0000000000)>>40)==0xABCDEF) {
                                continue;
                            }
                            
                            //trigger or trigger counter
                            else if (((hitdatad&0xFFFC000000000000)>>50)==0x3FFF ||(k>1 && ((filevector[bo].at(k-1)&0xFFFC000000000000)>>50)==0x3FFF)) {
                                next=k-(le+1);
                                continue;
                            }
                            //hit data
                            else  {
                                // reject time out events
                                //uint32_t testamp=(int)((hitdatad&0x0001FE0000000000)>>41);
                                if(eventseparator==1) // dont use the time out from the amp = 255
                                //if(testamp!=0xFF && eventseparator==1) // use the time out from the amp = 255
                                 {
                                        //uint32_t board_ip=((int)((hitdatad&0xFC00000000000000)>>58));  //6b
                                        board_id.push_back(bo+1);
                                        stic_id.push_back((int)((hitdatad&0x0380000000000000)>>55));  //3b
                                        ch_id.push_back((int)((hitdatad&0x007E000000000000)>>49));  //6b
                                        amp.push_back((int)((hitdatad&0x0001FE0000000000)>>41));  //8b
                                        hit_time.push_back((hitdatad&0x000001FFFFFFFFE0)>>5);   //36b
                                        fine_time.push_back((int)((hitdatad&0x000000000000001F)>>0));   //5b
                                        //printf("Hit: board_id= %02d stic_id= %01d ch_id= %02d sipm_id=  %03d amp= %03d hit_time= %09llX fine_time= %02X tmptime= %06X timelast= %06X\n ",board_id.at(linewrap),stic_id.at(linewrap),ch_id.at(linewrap),(ch_id.at(linewrap)*2 + stic_id.at(linewrap)),amp.at(linewrap),hit_time.at(linewrap),fine_time.at(linewrap),hit_time_tmp,hit_time_last);
                                }
                                else {
                                    continue;
                                }
                            } //end else hitdata
                        }//for between two triggers
                    }//if foundtrigger
                }//for on the size of the vector
            }//for on the boards
            int testT=0;
            for(int b=0;b<24;b++){if(TriggerT[b]==100000000)testT++;}// check how many boards did not receive a trigger
            if(testT>23){save=false;} // if only one board did receive the trigger, so not save the event
        
        if(save){
            tree->Fill();
            Eventnum++;
        }
        save =true;
        
    }//for on the events
    
    //write the tree in the file
    tree->Write();
    outputfile->Close();
   
    std::cout<<"file "<<rootfile_tree<<" created"<<std::endl;
    std::cout<<"the .root file exists, plot the variables! To load the tree, LoadtheTree"<<std::endl;
    return 0;
}


/**********************************************************************************************************************************/
//LoadtheEntry(uint64_t evnum)/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Loads the root file and provides the clustering for all the events////////////////////////////////////////////////////////////////
//All the following functions need the events to be virtually loaded and the clustering to be performed/////////////////////////////
/**********************************************************************************************************************************/


void Plotter::LoadtheEntry(uint64_t evnum, TTree* fChain){
   
    // Declaration of leaf types
    vector<unsigned int>    *BoardID;
    vector<unsigned int>    *STiCID;
    vector<unsigned int>    *Ch;
    vector<unsigned int>    *TOT;
    vector<uint64_t>        *HitTime;
    Int_t                   Eventnum;
    vector<uint32_t>        *FineTime;
    vector<UInt_t>          *TriggerID;
    vector<uint64_t>        *TriggerT;
    
    BoardID     = 0;
    STiCID      = 0;
    Ch          = 0;
    TOT         = 0;
    HitTime     = 0;
    FineTime    = 0;
    TriggerT    = 0;
    TriggerID   = 0;
    
    // List of branches
    TBranch        *b_BoardID;   //!
    TBranch        *b_STiCID;   //!
    TBranch        *b_Ch;   //!
    TBranch        *b_TOT;   //!
    TBranch        *b_HitTime;   //!
    TBranch        *b_Eventnum;   //!
    TBranch        *b_FineTime;   //!
    TBranch        *b_TriggerID;   //!
    TBranch        *b_TriggerT;   //!
    
    fChain->SetBranchAddress("BoardID",     &BoardID,   &b_BoardID);
    fChain->SetBranchAddress("STiCID",      &STiCID,    &b_STiCID);
    fChain->SetBranchAddress("Ch",          &Ch,        &b_Ch);
    fChain->SetBranchAddress("TOT",         &TOT,       &b_TOT);
    fChain->SetBranchAddress("HitTime",     &HitTime,   &b_HitTime);
    fChain->SetBranchAddress("Eventnum",    &Eventnum,  &b_Eventnum);
    fChain->SetBranchAddress("TriggerID",   &TriggerID, &b_TriggerID);
    fChain->SetBranchAddress("FineTime",    &FineTime,  &b_FineTime);
    fChain->SetBranchAddress("TriggerT",    &TriggerT,  &b_TriggerT);
    Events.clear();
    Long64_t        nentries = fChain->GetEntriesFast();
    
    
    
    uint64_t jentry=evnum;
    unsigned int    hitsp[2][4][1536]      ={0};    // array of amplitudes
    double          hitst[2][4][1536]      ={0};    // array of full time info
    uint64_t        hits_c[2][4][1536]     ={0};    // array of coarse time
    uint32_t        hits_f[2][4][1536]     ={0};    // array of fine time
    unsigned int    sticids[2][4][1536]    ={1000}; // array of stidID
    unsigned int    boardids[2][4][1536]   ={1000}; // array of boardID
   
    
    Long64_t nb = fChain->GetEntry(jentry); 
    
    std::vector<double>   time;
    std::vector<uint64_t>     trigid;
    
    for(int b=0;b<24;b++){
        trigid.push_back(TriggerID->at(b));
        double TriggerTMsb=(double)(((TriggerT->at(b)&0xFFFF8000)/(pow(2,15)-1))*pow(2,15));
        double TriggerTLsb=(double)(TriggerT->at(b)&0x7FFF);
        double tt=(double)((TriggerTMsb+TriggerTLsb)*(1/(board_frequency/1000)));
        if(TriggerID->at(b)==100000000)tt=100000000;
        time.push_back(tt);
        //cout.precision(dbl::max_digits10);
    }
    
    Event ev(Eventnum,time,trigid);
    Int_t length = Ch->size();
    
    for(int i=0;i<length;i++){
        
        uint32_t  board_inLay=(BoardID->at(i)-1)%3;                                                             //board poition (0-2) inside a single layer
        uint32_t  ch_position=((board_inLay)*512)+((int)(STiCID->at(i)/2))*128+(Ch->at(i)*2)+(STiCID->at(i)%2); //channel position
        int lay=-1;
        int xy=-1;
        //assign layer indexes
        for(int k=0;k<2;k++){
            for(int j=0;j<4;j++){
                if(BoardID_Map[k][j][board_inLay]==(int)BoardID->at(i)){
                    lay=j;
                    xy=k;
                }
            }
        }
        //if(hitsp[xy][lay][ch_position]!=0){std::cout<<"double assignment!!"<<std::endl;
        //std::cout<<std::dec<<"board: "<<BoardID->at(i)<<" "<<STiCID->at(i)<<" "<<Ch->at(i)<<" "<<board_inLay<<" "<<ch_position<<" "<<lay<<" "<<TOT->at(i)<<" "<<hitsp[xy][lay][ch_position]<<" "<<std::hex<<HitTime->at(i)<<" "<<hitst[xy][lay][ch_position]<<std::endl;
        //}
        
        // Fill hits arrays
        if(lay!=-1 && xy!=-1){
            hitsp[xy][lay][ch_position]=TOT->at(i);
            double HitTimeShiftMsb=(double)(((HitTime->at(i)&0xFFFF8000)/(pow(2,15)-1))*pow(2,15));
            double HitTimeShiftLsb=(double)(HitTime->at(i)&0x7FFF);
            double HitTimeShift=(HitTimeShiftMsb+HitTimeShiftLsb)*(1/(board_frequency/1000));
            //cout.precision(dbl::max_digits10);
            double finecnt=(double)(FineTime->at(i)*1/((board_frequency/1000)*fine_bits));
            if(enabletiming && fabs(hitst[xy][lay][ch_position]-board_offsetns[BoardID->at(i)-1])<time_window){
                hits_c[xy][lay][ch_position] = HitTime->at(i);
                hits_f[xy][lay][ch_position] = FineTime->at(i);
                hitst[xy][lay][ch_position]=HitTimeShift+finecnt;
                sticids[xy][lay][ch_position]=STiCID->at(i);
                boardids[xy][lay][ch_position]=BoardID->at(i);
            }
        }
    }
    
    // Create layers with the hits and clusters information
    
    std::vector<std::vector<Layer>> layers;
    std::vector<Layer> layers_x;
    std::vector<Layer> layers_y;
    layers.clear();
    layers_x.clear();
    layers_y.clear();
    for(int k=0;k<2;k++){
        for(int j=0;j<4;j++){
            TString layname=layer_name[k][j];
            Layer layer(layname);
            layer.AssignHits(hitsp[k][j],hitst[k][j], hits_c[k][j], hits_f[k][j]);
            
            // Apply clustering algorithm and assign the clusters to the layers
            // Algorithm ?
            std::vector<Cluster> clusters;
            for(int i=0;i<1536;i++){
                if(hitsp[k][j][i]>0){ // Require TOT > 0 
                    Cluster clu(i,hitsp[k][j][i],hitst[k][j][i],sticids[k][j][i],boardids[k][j][i], hits_c[k][j][i], hits_f[k][j][i]);
                    for(int h=i+1;h<1536;h++){
                        if(hitsp[k][j][h]>0 && h-i==1){
                            clu.addChannelToTheRight(hitsp[k][j][h],hitst[k][j][h],sticids[k][j][h], boardids[k][j][h], hits_c[k][j][h], hits_f[k][j][h]); 
                            i=h;
                        }
                        else break;
                    }
                    clusters.push_back(clu);
                    clu.clear();
                }
            }
            layer.AssignClusters(clusters);
            if (k) layers_y.push_back(layer);
            else layers_x.push_back(layer);
            clusters.clear();
        }
    }
    layers.push_back(layers_x);
    layers.push_back(layers_y);
    ev.AssignLayers(layers);
    Events.push_back(ev);
}


/**********************************************************************************************************************************/
//DumpEvent()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Dumps the clusters info for every event///////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/


void Plotter::DumpEvent(uint64_t evnum){

    // Open the data root file
    
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of opening file    

    if(evnum>=(uint64_t)nentries){
        std::cout<<"not initialised event"<<std::endl;
        return;
    }
    
    Events.clear();
    LoadtheEntry(evnum, fChain);
    Event ev=Events.at(0);
    std::vector<double> times =ev.Gettimestamp();
    
    std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
    for(unsigned int k=0;k<layers.size();k++){ // Loop over XY plane - k can be 0 or 1
	std::cout<<"x or y "<<k<<std::endl;
        for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
            std::cout<<"layer: "<<j<<std::endl;
	    Layer layer=(layers[k]).at(j);
            std::vector<Cluster> clust = layer.Getclusters_list();
            for(unsigned int i=0;i<clust.size();i++){
                std::cout<<"Layer "<<layer_name[k][j]<<std::endl; // Thie line is suppoed to be change after design the configuration file
                Cluster clusttmp=clust.at(i);
                clusttmp.dump();
            }
        }
    }
    f->Close();
}


/**********************************************************************************************************************************/
//DumpFullEvents()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Dumps the clusters info for every event///////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::DumpFullEvents(){

    // Open the data root file
    
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    int numberEv=nentries;
    int cntevents=0;
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i, fChain);
		Event ev=Events.at(0);
		std::vector<double> times =ev.Gettimestamp();
		int full[2][4]={0};
		std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
		for(unsigned int k=0;k<layers.size();k++){
            for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
                Layer layer=(layers[k]).at(j);
		    	for(int h=0;h<1536;h++){
		    		if((layer.GetAmplitudes())[h]>0)full[k][j]=1; // GetAmplitude -> Ch Position
		    	}
            }
		}
		int counthits=0;
		for(unsigned int k=0;k<2;k++){
            for (unsigned int j=0; j<4 ; j++ ){
			    counthits+=full[k][j];
            }
		}
		if(counthits>=6)cntevents++;
	}
	std::cout<<"good full tracks are in: "<<cntevents<<std::endl;
    f->Close();
}

/**********************************************************************************************************************************/
//PlotEvent()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Event display of a single event///////////////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotEvent(uint64_t evnum){
    // Open the data root file
    
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    //TString modname[8]={"X1","X2","Y1","Y2","Y3","Y4","X3","X4"};
    Events.clear();
    LoadtheEntry(evnum, fChain);
    Event ev=Events.at(0);
    std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
    TString e="Event ";
    e+=evnum;

    TString namecanvas=e;


    for(unsigned int k=0;k<layers.size();k++){
        for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
            Layer layer=(layers[k]).at(j);
            t[k][j]=new TH1D(layer_name[k][j], layer_name[k][j],1536,-0.5,1535.5);
            for(int h=0;h<1536;h++){
                if((layer.GetAmplitudes())[h]>0)t[k][j]->Fill(h,(layer.GetAmplitudes())[h]);
            }
        }
    }

    // Draw amplitude and position of hits
    c = new TCanvas("c",namecanvas,600,800);
    c->Divide(2,4);
    for(unsigned int k=0;k<layers.size();k++){
        std::vector<Layer> layerXY=layers.at(k);
        for (unsigned int j=0; j<layerXY.size(); j++ ){
            c->cd(k+2*j+1);// k+j+1 position  in canvas
            c->Update();
            t[k][j]->Draw("hist");
        }
    }
    f->Close();
    return c;
}

/**********************************************************************************************************************************/
//PlotBeamSpot()////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots all the hits recorded in every channel during the run///////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

// Change how we plot the beamspot -> shold plot all layer
TCanvas * Plotter::PlotBeamSpot(){

    // Open the data root file
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
            t[k][j]=new TH1D(layer_name[k][j],layer_name[k][j],1536,-0.5,1535.5);
        }
    }

    for(int i=0;i<nentries;i++){
        Events.clear();
        LoadtheEntry(i, fChain);
        Event ev=Events.at(0);
        
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        
        for(unsigned int k=0;k<layers.size();k++){
            for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
                Layer layer=(layers[k]).at(j);
                for(int j=0;j<1536;j++){
                    if((layer.GetAmplitudes())[j]>0)t[k][j]->Fill(j);
                }
            }
            
        }
        
    }
    
    c = new TCanvas("c","Beam Spot",600,800);
    c->Divide(2,4);
    
    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
            
            c->cd(k+2*j+1);
            
            c->Update();
            t[k][j]->Draw("hist");
            c->Update();
            b0[k][j]=new TLine(512,0,512,gPad->GetUymax());
            b1[k][j]=new TLine(1024,0,1024,gPad->GetUymax());
            b0[k][j]->SetLineColor(kGreen);
            b1[k][j]->SetLineColor(kGreen);
            b0[k][j]->SetLineWidth(2);
            b1[k][j]->SetLineWidth(2);
            b0[k][j]->Draw("same");
            b1[k][j]->Draw("same");
        }
    }
    f->Close();
    return c;

}

/**********************************************************************************************************************************/
//PlotClusterBeamSpot()/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots all the clusters recorded in every layer during the run////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/


 TCanvas * Plotter::PlotClusterBeamSpot(){

    // Open the data root file
    
    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    int numberEv=nentries;
    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
            t[k][j]=new TH1D(layer_name[k][j],layer_name[k][j],1536,-0.5,1535.5);
        }
    }
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i, fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        
        for(unsigned int k=0;k<layers.size();k++){
            
            for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
                Layer layer=(layers[k]).at(j);
                std::vector<Cluster> clusters =layer.Getclusters_list();
                
                if(clusters.size()>0){
                    for(unsigned int h=0;h<clusters.size();h++){
                        Cluster clu=clusters.at(h);
                        if( clu.clusterLength()>1 && clu.clusterLength()<5){
                            int max=clu.clusterLength()-1+clu.startChannel();
                            for(int p=clu.startChannel(); p<max; p++){
                                t[k][j]->Fill(p);
                            }
                        }
                    }
                }
            }
        }
    }
    c = new TCanvas("c","Cluster Beam Spot",600,800);
    c->Divide(2,4);
    
    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
        
            c->cd(k+2*j+1);

            c->Update();
            t[k][j]->Draw("hist");
            c->Update();
            b0[k][j]=new TLine(512,0,512,gPad->GetUymax());
            b1[k][j]=new TLine(1024,0,1024,gPad->GetUymax());
            b0[k][j]->SetLineColor(kGreen);
            b1[k][j]->SetLineColor(kGreen);
            b0[k][j]->SetLineWidth(2);
            b1[k][j]->SetLineWidth(2);
            b0[k][j]->Draw("same");
            b1[k][j]->Draw("same");
        }
    }
    f->Close();
    return c;

}
 
/**********************************************************************************************************************************/
//PlotClusterMultiplicity()/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots number of clusters per event - distribution ////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotClusterMultiplicity(){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    int numberEv=nentries;
    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
            t[k][j]=new TH1D(layer_name[k][j],layer_name[k][j],1536,-0.5,1535.5);
        }
    }
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i, fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        
        for(unsigned int k=0;k<layers.size();k++){
            for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
                Layer layer=(layers[k]).at(j);
                std::vector<Cluster> clusters =layer.Getclusters_list();
                t[k][j]->Fill(clusters.size());
            }
        }
    }

    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
   	        cout << layer_name[k][j] << " : " << t[k][j]->GetMean(1) << endl;
        }
    }
    c = new TCanvas("c","Cluster Beam Spot",600,800);
    c->Divide(2,4);
    
    for(unsigned int k=0;k<2;k++){
        for (unsigned int j=0; j<4;j++){
            c->cd(k+2*j+1);

            c->Update();
            t[k][j]->Draw("hist");
            t[k][j]->GetXaxis()->SetRangeUser(-0.5,50);
            c->Update();
            gPad->SetLogy();
        }
    }
    f->Close();
    return c;

}

/**********************************************************************************************************************************/
//PlotHitEfficiency()///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Checks if, for every cluster in the reference plane, there is at least one hit in the plane under investigation///////////////////
//The search is made in the beamspot areas, considering the beamspot shift and the overlap of two fibres////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotHitEfficiency(TString lay){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    int numberEv=nentries;
    
    // Before Beam position defined here
    hist_general[0]= new TH1I("hiteffhist","Hit efficiency histogram",1536,-0.5,1535.5);
    int counter=0;
    int totcounter=0;
    int misscount=0;
    for(int i=0;i<numberEv;i++){

        Events.clear();
        LoadtheEntry(i, fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        std::vector<double> times =ev.Gettimestamp();
        Layer layer("");
        Layer partnerlayer("");
        int plane=-1;
        int lp=-1; // layer in the plane
        int partnerlp=-1;
        for (unsigned int k=0; k<layers.size(); k++){
            for (unsigned int j=0; j<(layers[k]).size(); j++){
                if (lay == layer_name[k][j]){
                    plane=k;
                    lp=j; 
                    partnerlp = j+(j%2)*(-1)+(1-(j%2));
                    layer = (layers[k]).at(j);
                    partnerlayer = (layers[k]).at(partnerlp);
                }
            }
        }
        std::vector<Cluster> clusters =partnerlayer.Getclusters_list();
        if(clusters.size()>0){
            for(unsigned int j=0;j<clusters.size();j++){
                Cluster clu=clusters.at(j);
                
                bool found=false;
                if(clu.clusterLength() >1){
                    int clpos=(int)(clu.channelPosition());
                    int board=(int)(clu.GetBoardID((unsigned int)clpos));
                    if(clpos<beamupperlimit[plane][partnerlp] && clpos>beamlowerlimit[plane][partnerlp]){
                        totcounter++;
                        
                        int othermean=(beamupperlimit[plane][partnerlp]-beamlowerlimit[plane][partnerlp])/2+beamlowerlimit[plane][partnerlp];
                        int innermean=(beamupperlimit[plane][lp]-beamlowerlimit[plane][lp])/2+beamlowerlimit[plane][lp];
                        int shiftch=othermean-innermean;
                        int underlimit=clpos-overlap-shiftch;
                        int upperlimit=clpos+overlap-shiftch;
                        if((clpos-overlap)<0)underlimit=0;
                        if((clpos+overlap)>1535)upperlimit=1535;
                        
                        for(int p=underlimit;p<upperlimit;p++){
                            if((layer.GetAmplitudes())[p]>0){
                                found=true;
                                hist_general[0]->Fill(p);
                            }
                            
                        }

                        if(!found){
                            bool mis=false;
                            for(int p=underlimit;p<upperlimit;p++){
                                if(times.at(board)==100000000){mis=true;}
                            }
                            if(mis)misscount++;
                        }
                        if(found)counter++;
                        found=false;
                    }
                }
            }
        }
    }
    if(totcounter>0)std::cout<<"the hit effciency is "<<(double)counter/(totcounter-misscount)<<std::endl;
    else std::cout<<"reference layer dead"<<std::endl;
    std::cout<<"the missed counts are "<<misscount<<std::endl;
    std::cout<<"total hits considered "<<totcounter<<std::endl;
    c = new TCanvas("c","hefficiency",800,600);
    c->cd();
    hist_general[0]->Draw("hist");
    f->Close();
    return c;
    
}

/**********************************************************************************************************************************/
//PlotBoardsTimeDiff()//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between each trigger time in board 8 (master) and the board under investigation/////////////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotBoardsTimeDiff(UInt_t board){
    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
    int numberEv=nentries;
    hist_general[0]= new TH1D("timdiff","Timing offsets",32000,-100000,100000.);
    c=new TCanvas("c","Timing offsets",800,600);
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i, fChain);
        Event ev=Events.at(0);
        std::vector<double> times =ev.Gettimestamp();
        std::vector<uint64_t> trigid= ev.Gettriggercounter();

        if(times.at(board-1)!=100000000 && times.at(10)!=100000000){

            double ti;
            if(times.at(board-1)>times.at(7)){ti=(double)(times.at(board-1)-times.at(7));}
            else {ti=-((double)(times.at(7)-times.at(board-1))); }

            hist_general[0]->Fill(ti);
            if(fabs(ti)>100)std::cout<<i<<" "<<times.at(board-1)<<" "<<times.at(10)<<std::endl;

        }
    }
    std::cout<<hist_general[0]->GetMean()<<std::endl;
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Entries/6.25[ns]");
    hist_general[0]->GetXaxis()->SetTitle("time difference [ns]");
    hist_general[0]->Draw("hist");
    f->Close();
    return c;
}

/**********************************************************************************************************************************/
//PlotTrigTrigTime()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between one trigger and the following in the board under investigation//////////////////////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotTrigTrigTime(UInt_t board){
    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
    int numberEv=nentries;
    hist_general[0]= new TH1D("timdiff","Triggers distance",20000,0.,20000000.);
    c=new TCanvas("c","Timing offsets",800,600);
    for(int i=1;i<numberEv;i++){
        std::cout<<" Event "<<i<<std::endl;
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event e=Events.at(0);
        LoadtheEntry(i-1, fChain);
        Event e1=Events.at(1);
        std::vector<double> times =e.Gettimestamp();
        std::vector<double> times1 =e1.Gettimestamp();
        std::cout<<"trigger time: "<<times.at(board-1)<<std::endl;
        std::cout<<"trigger time1: "<<times1.at(board-1)<<std::endl;
        
        if(times.at(board-1)!=100000000 && times1.at(board-1)!=100000000){
 
            double ti;
            ti=(double)(times.at(board-1)-times1.at(board-1));
            ti=ti*clock_feq;

            hist_general[0]->Fill(ti);
        }
    }
    c->cd();
    c->SetLogx();
    hist_general[0]->GetYaxis()->SetTitle("Entries/[us]");
    hist_general[0]->GetXaxis()->SetTitle("time difference [ns]");
    hist_general[0]->Draw("hist");
    f->Close();
    return c;
}

/**********************************************************************************************************************************/
//PlotInnerTimeDiff()///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between the trigger time and each hit time of the event in the board under investigation////////////////
/**********************************************************************************************************************************/


TCanvas * Plotter::PlotInnerTimeDiff(UInt_t board){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    int numberEv=nentries;
    hist2D_general[0]= new TH2D("timdiff","Timing offsets",1000,0.,1000,1536,-0.5,1535.5);
    c=new TCanvas("c","Timing offsets",800,600);
    for(int i=0;i<numberEv;i++){
        
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<double> times =ev.Gettimestamp();
        
        if(times.at(board-1)!=100000000){
            int m =(board-1)%3;
            std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
            Layer layer("");
            for(unsigned int k=0;k<layers.size();k++){
                for (unsigned int j = 0; j<((layers[k]).size());j++){ // Loop over layer -j can be 0 to 3
                    for (unsigned int b = 0; b< 3 ; b++) {
                        if (board == (UInt_t)BoardID_Map[k][j][b]){
                            layer=(layers[k]).at(j);
                        }
                    }
                }
            }
        
            for(int j=m*512;j<(m+1)*512;j++){
                if((layer.GetAmplitudes())[j]>0)
                {
                    double Time=(layer.GetTimes())[j];
                    double TrigTimeShift=times.at(board-1);
                    double ti =fabs(TrigTimeShift-Time);
                    hist2D_general[0]->Fill(ti,j);
                }
            }
        }
    }
    c->cd();
    //gStyle->SetPalette(kGreenRedViolet); // Comment out to avoid error
    hist2D_general[0]->Draw("colz");
    f->Close();
    return c;
}




/**********************************************************************************************************************************/
//PlotChHitTime()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the channel VS hit time distribution////////////////////////////////////////////////////////////////////////////////////////
//This function is meant to see if the channels in the midde of the beam spot have as well the first hits///////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotChHitTime(TString lay){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
    
    int numberEv=nentries;
    hist2D_general[0]= new TH2D("chtime","Ch vs Hit to hit time",1536,-0.5,1535.5,40,0,2000);
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        for (unsigned int k=0; k<layers.size(); k++){
            for (unsigned int j=0; j<((layers[k]).size()); j++){
                if (lay == layer_name[k][j]){
                    layer = (layers[k]).at(j);
                }
            }
        }
        std::vector<Cluster> clusters =layer.Getclusters_list();
        
        for(unsigned int j=0;j<clusters.size();j++){
            uint64_t min=ULLONG_MAX;
            unsigned int channelone=1537;
            Cluster clu=clusters.at(j);
            for(unsigned int k=0;k<clu.clusterLength();k++){
                unsigned int ch=k+clu.startChannel();
                if(clu.timeAtChannel(ch)<=min){
                    min=clu.timeAtChannel(ch);
                    channelone=ch;
                }
            }
            for(unsigned int k=0;k<clu.clusterLength();k++){
                unsigned int ch2=k+clu.startChannel();
                if(ch2!=channelone){
                    double diff = clu.timeAtChannel(ch2)-min;
                    
                    hist2D_general[0]->Fill(ch2,diff);
                }
            }
        }
    }
    
    c = new TCanvas("c","Inefficiency",800,500);
    c->cd();
    hist2D_general[0]->GetYaxis()->SetTitle("time difference [ps]");
    hist2D_general[0]->GetXaxis()->SetTitle("layer channel");
    hist2D_general[0]->Draw("colz");
    f->Close();
    return c;
}

/**********************************************************************************************************************************/
//PlotTOTHitTime()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the amplitude VS hit time distribution//////////////////////////////////////////////////////////////////////////////////////
//This function is meant to see if there is a clear correlation between the amplitude of the signal and its time of arrival/////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotTOTHitTime(TString lay){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
    
    int numberEv=nentries;
    hist2D_general[0]= new TH2D("TOTtime","TOT vs Hit to hit time",512,-256,256,100,0,4600);
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        for (unsigned int k=0; k<layers.size(); k++){
            for (unsigned int j=0; j<((layers[k]).size()); j++){
                if (lay == layer_name[k][j]){
                    layer = (layers[k]).at(j);
                }
            }
        }
        std::vector<Cluster> clusters =layer.Getclusters_list();
        
        for(unsigned int j=0;j<clusters.size();j++){
            double min=DBL_MAX;
            unsigned int channelone=1537;
            Cluster clu=clusters.at(j);
            int Stic1=1000;
            int Stic2=1000;
            int first=1600;
            int second=1600;
            
            for(unsigned int k=0;k<clu.clusterLength();k++){
                unsigned int ch=k+clu.startChannel();
                if(clu.timeAtChannel(ch)<=min){
                    min=clu.timeAtChannel(ch);
                    channelone=ch;
                    first=ch;
                    Stic1=clu.GetSTiCID(channelone);
                }
            }
            for(unsigned int k=0;k<clu.clusterLength();k++){
                unsigned int ch2=k+clu.startChannel(); 
                if(ch2!=channelone){
                    double diff = clu.timeAtChannel(ch2)-min;
                    int tot;
                    tot=(int)clu.amplitudeAtChannel(channelone)-(int)clu.amplitudeAtChannel(ch2);
                    Stic2=clu.GetSTiCID(ch2);
                    second=ch2;
                    if(Stic1==Stic2 && fabs(first-second)<512)hist2D_general[0]->Fill(tot,diff);
                }
            }
        }
    }
    
    c = new TCanvas("c","Inefficiency",800,500);
    c->cd();
    hist2D_general[0]->GetYaxis()->SetTitle("time difference [ps]");
    hist2D_general[0]->GetXaxis()->SetTitle("TOT difference");
    hist2D_general[0]->Draw("colz");
    f->Close();
    return c;
}


/**********************************************************************************************************************************/
//PlotClusterTOT()//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the total amplitude of the clusters/////////////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotClusterTOT(){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
 
    int numberEv=nentries;
    hist_general[0]= new TH1D("ClusterTOT","ClusterTOT",250,0,250);
 
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers = ev.Getlayers_list();
        Layer layer("");
        for (unsigned int k=0; k<layers.size(); k++){
            for (unsigned int j=0; j<((layers[k]).size()); j++){
            layer=(layers[k]).at(j);
            std::vector<Cluster> clusters =layer.Getclusters_list();
                if(clusters.size()>0){
                    for(unsigned int h=0;h<clusters.size();h++){
                    Cluster clu=clusters.at(h);
                        if(clu.clusterLength()>1){
                            double TOT=(double)(clu.totalAmplitude());
                            hist_general[0]->Fill(TOT);
                        }
                    }//clusters in the layer
                }
            }
        }//layers
 
    }//events
 
    c = new TCanvas("c","Cluster TOT",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Events");
    hist_general[0]->GetXaxis()->SetTitle("Cluster TOT");
    hist_general[0]->Draw("hist");
    f->Close();
    return c;
 }

/**********************************************************************************************************************************/
//PlotInClusterTOT()////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the amplitude of the hits belonging to a cluster////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotInClusterTOT(){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
    
    int numberEv=nentries;
    hist_general[0]= new TH1D("ClusterTOT","ClusterTOT",250,0,250);
    
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        for (unsigned int k=0; k<layers.size(); k++){
            for (unsigned int j=0; j<((layers[k]).size()); j++){
                layer=(layers[k]).at(j);
                std::vector<Cluster> clusters =layer.Getclusters_list();
            if(clusters.size()>0){
                for(unsigned int h=0;j<clusters.size();h++){
                    
                    Cluster clu=clusters.at(h);
                    if(clu.clusterLength()>1){
                        for(unsigned int q=0;q<clu.clusterLength();q++){
                            unsigned int ch=q+clu.startChannel();
                            double TOT=(double)(clu.amplitudeAtChannel(ch));
                            hist_general[0]->Fill(TOT);
                        }
                    }
                }//clusters in the layer
            }
        }
        }//layers
    }//events
    
    c = new TCanvas("c","Cluster TOT",800,500);
    c->cd();
    hist_general[0]->GetYaxis()->SetTitle("Events");
    hist_general[0]->GetXaxis()->SetTitle("Cluster TOT");
    hist_general[0]->Draw("hist");
    f->Close();
    return c;
}

/**********************************************************************************************************************************/
//PlotPlotHitHitTime()//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Plots the time difference between the first and the second hit arriving inside clusters of dimension >1///////////////////////////
/**********************************************************************************************************************************/

TCanvas * Plotter::PlotHitHitTime(TString lay){
    
    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    

    int numberEv=nentries;
    hist_general[0]= new TH1D("hittohit","Hit to hit time",200,-4600,4600);
    hist_general[1]= new TH1D("hittohit1","Hit to hit time if the first channel is on the right",200,-4600,4600);
    hist_general[2]= new TH1D("hittohit2","Hit to hit time if the first channel is on the left",200,-4600,4600);
    hist_general[3]= new TH1D("hittohit3","Hit to hit time same stic",200,-4600,4600);
    hist_general[4]= new TH1D("hittohit4","Hit to hit time same board",200,-4600,4600);
    hist_general[5]= new TH1D("hittohit5","Hit to hit time diff board",200,-4600,4600);
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<std::vector<Layer>> layers =ev.Getlayers_list();
        Layer layer("");
        for (unsigned int k=0; k<layers.size(); k++){
            for (unsigned int j=0; j<((layers[k]).size()); j++){
                if (lay == layer_name[k][j]){
                    layer = (layers[k]).at(j);
                }
            }
        }
        std::vector<Cluster> clusters =layer.Getclusters_list();
        
        for(unsigned int j=0;j<clusters.size();j++){
            double min=DBL_MAX;
            double min2=DBL_MAX;
            int first=1600;
            int second=1600;
            unsigned int Stic1=1000;
            unsigned int Stic2=1000;
            unsigned int board1=1000;
            unsigned int board2=1000;
            Cluster clu=clusters.at(j);
            if(clu.clusterLength()>1){
                for(unsigned int k=0;k<clu.clusterLength();k++){
                    unsigned int ch=k+clu.startChannel();
                    if(clu.timeAtChannel(ch)<=min){
                        min2=min;
                        min=clu.timeAtChannel(ch);
                        second=first;
                        first=ch;
                        Stic2=Stic1;
                        board2=board1;
                        Stic1=clu.GetSTiCID(ch);
                        board1=clu.GetBoardID(ch);
                    }
                    else if(clu.timeAtChannel(ch)<=min2){
                        min2=clu.timeAtChannel(ch);
                        second=ch;
                        Stic2=clu.GetSTiCID(ch);
                    }
                }
                if(min2!=DBL_MAX){
                    double diff = DBL_MAX;
                    if(first>second)diff = min2-min;
                    else diff =min-min2;
                    
                    if(first>second)hist_general[1]->Fill(diff);
                    else hist_general[2]->Fill(diff);
                    if(Stic1==Stic2 && board1==board2)hist_general[3]->Fill(diff);
                    if(board1==board2)hist_general[4]->Fill(diff);
                    else hist_general[5]->Fill(diff);
                    hist_general[0]->Fill(diff);
                }
            }
        }
    }
    
    c = new TCanvas("c","HitHitTime",800,500);
    c->Divide(3,2);
    c->cd(1);
    hist_general[0]->GetYaxis()->SetTitle("Entries/46[ps]");
    hist_general[0]->GetXaxis()->SetTitle("time difference [ps]");
    hist_general[0]->Draw("hist");
    c->cd(2);
    hist_general[1]->Draw("hist");
    c->cd(3);
    hist_general[2]->Draw("hist");
    c->cd(4);
    hist_general[3]->Draw("hist");
    c->cd(5);
    hist_general[4]->Draw("hist");
    c->cd(6);
    hist_general[5]->Draw("hist");
    f->Close();
    return c;
}


/**********************************************************************************************************************************/
//PrintTrigTime()///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Prints the timestamp of all the triggers recorded by a given board////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::PrintTrigTime(uint64_t board){

    // Open the data root file
    TString rootfilemid(m_file(0,m_file.Sizeof()-2));
    TString rootfile=getenv("ANALYSIS_PATH");
    rootfile+="/rootfiles/";
    rootfile+=rootfilemid;
    rootfile+=".root";

    TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(rootfile_tree);
    if (!f || !f->IsOpen()) {
        f = new TFile(rootfile_tree);
    }
    
    TTree *fChain = (TTree*) f->Get("data");   //!pointer to the analyzed TTree or TChain
    
    Long64_t        nentries = fChain->GetEntriesFast();
    
    // End of openning file    
    int numberEv=nentries;
    for(int i=0;i<numberEv;i++){
        Events.clear();
        LoadtheEntry(i  , fChain);
        Event ev=Events.at(0);
        std::vector<double> times =ev.Gettimestamp();
        std::cout<<times.at(board-1)<<std::endl;
    }
    f->Close();
}

/**********************************************************************************************************************************/
//Position(int ch_layer,int layer,double &x,double &y,double &z)////////////////////////////////////////////////////////////////////
//Takes in input the ch and layer number, returns x,y,z coordinates/////////////////////////////////////////////////////////////////
/**********************************************************************************************************************************/

void Plotter::Position(int ch_layer,int layer,double &x,double &y,double &z){
    
    x=-10000;
    y=-10000;
    z=-10000;
    
    //angle in radiants for layers U and V (5 deg)
    
    //compute half layer width to be entered in the middle of the plane
    double half_layer=1536/2*fibre_thickness+6*gap_die+5.5*gap_SiPM;
    
    //z positions inside a station
    
    //layer inside a station
    int el=layer%4;
    
    //z position defined as:
    z=zpos[el]+int(layer/4)*z_offset;
    
    //how many half dies
    int mult=int(ch_layer/64);
    
    //shift to add to the position due to dead regions
    double shift=int(mult/2)*gap_die+int(mult/2)*gap_SiPM+(mult%2)*gap_die;
    
    //computation of the angle for the considered layer
    //double angle=radiants;
    
    //first station only x defined for X and U planes
    if(layer==0)x=-(ch_layer*fibre_thickness+shift-half_layer);
    if(layer==1)x=-(ch_layer*fibre_thickness+shift-half_layer);//-200*sin(angle);
    
    //first station only y defined for Y and V planes
    if(layer==2)y=-(ch_layer*fibre_thickness+shift-half_layer);//+200*sin(angle);
    if(layer==3)y=-(ch_layer*fibre_thickness+shift-half_layer);
    
    //second station only y defined for Y and V planes
    if(layer==4)y=-(ch_layer*fibre_thickness+shift-half_layer);
    if(layer==5)y=-(ch_layer*fibre_thickness+shift-half_layer);//-200*sin(angle);
    
    //second station only x defined for X and U planes
    if(layer==6)x=(ch_layer*fibre_thickness+shift-half_layer);//+200*sin(angle);
    if(layer==7)x=(ch_layer*fibre_thickness+shift-half_layer);
    
    
}



