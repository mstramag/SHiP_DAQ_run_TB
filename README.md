# SHiP_DAQ_run_TB

Data analysis program for time measurement analysis. This is developed for SciFi testbeam which will be held at DESY in October 2019. 

The codes are based on TBOct2018 by Maria Stramaglia
Rewritten by Surapat Ek-In

## Feature
- [DONE] Decoder - from FPGA SPI data convention.
- [DONE] Reader - contain an information for each hit, match with trigger counter in the next stage. 
- [DONE] Cluster constructor - with root file. 
- [DONE] Event Reconstructor - each event is separated by trigger line. 
- [DONE] Plotter - Visualisation program.
- [DONE] Alignment -
- [DONE] Track reconstruction - 

The program must be interactive within ROOT6 CLI. 
The core program must be rewritten since the previous version use `cint` which is replaced by CLANG in ROOT6. 

## Testing

The benchmark will be done on RUN_8006

- Interactive program
- memory management
- Time order


## Version

Example
```
    git checkout -b v1.0 v1.0
```

Stage | Things to add | Comment 
------|---------------|---------
v1.2  | Decoder and Reader          | Finish with decoder - start doing reader
v1.3  | Must have root output       | Finish
v1.9  | [Finish] Decoder and Reader | Comments are welcome
v2.0  | [Plan] Combine v1.9 and v1.0 | Comments are welcome 
v2.3.1 | with the Oct. 2018  decoder | Ready to be used 
v2.4.6 | with 2019  decoder | Ready to be used 
v2.5   | 2019 Decoder with HitDelayTime Correctim | Ready to be used
v2.6.1 | Compilable version - fixed warnings | Ready to used 
v2.6.2 | Add linear track reconstruction | Ready to used - Alignment is ongoing 
v2.6.3 | Fix bugs to compute efficiency and index convention  | Ready to used - Alignment is ongoing 
v2.6.4 | Add how to compile millepede  | Ready to used - Alignment is ongoing 
v2.6.5 | Update time calibration feature  | Ready to used - Alignment is ongoing 
v2.6.6 | Update Trigger variable - backend  | Ready to used - Alignment is ongoing 
v2.7     | Add alignment class  | need more test
v2.7.1   | Add function for VCSEL injection system  | alignment need tuning 
v2.7.2   | fix bugs in alignment class   | Ready to be used - alpha test
v2.7.3   | Add classes for event display and add README | Ready to be used - alpha test
v2.7.4   | Change alignment process | Ready to be used - alpha test - debugging cosmic
v2.7.5   | dead channels treatment changes and minor updates - Update run.C | Ready to be used - beta test
v2.7.6   | Change Configuration file to only accept mm and ns constants | Ready to be used - beta test
v2.7.7   | Modifications on timing functions and small fixes. Updated BoardOffsets at Configutation file. | Ready to be used - beta test
v2.7.8   | Optimise EventDisplay functions | Ready to be used - beta test
v2.8     | Update run.C | Ready to be used - beta test
v2.8.1   | Merge bugs fixing | Ready to be used - beta test
v2.8.2   | Improve backend on how to use ChOffSet | Ready to be used - beta test
v2.8.3   | Change millepede path | Ready to be used - beta test
v2.8.4   | Add limit header | Ready to be used - beta test
v2.8.5   | Update testbeam functions | Ready to be used 
v2.8.6   | Update README and clean hardcoded paths | Ready to be used 
v2.8.7   | Debugging message for Bad calibration boards | Ready to be used 
v2.8.8   | Deleted unused functions | Ready to be used
v2.8.9   | Update Efficiency and Residual functions | Ready to be used

Version 2.2 has a decoder.hpp bug. For CTR, please use the version v2.4.4. 

#################
## ORGANISATION
The package is organised following its directory structure:
```
    - analysis        Target initialisation
    - data			  Tested data - this data is for testing the code
    - etc 			  Old code and python old codes 
    - include         Headers 
    - src             Classes 
    - plot_results    Result should be saved here
    - rootfiles       ROOT file - contain output files
```
#################

## Set up program
1.  Clone this repository to your local directory as, SSH protocal,   
```
    git clone ssh://git@gitlab.cern.ch:7999/mstramag/SHiP_DAQ_run_TB.git
```

or HTTPS

```
    git clone https://gitlab.cern.ch/mstramag/SHiP_DAQ_run_TB.git
```

2. Redirect to the version you would like to work with. For example,

```
    cd SHiP_DAQ_run_TB
    git checkout -b <new_branch_name> v1.9 
```
This will create a branch name "new_branch" on top of branch version v1.9

3. Setup your work space  [rely on bash shell].  

```
    source Setup.sh
```
By default, the DATAPATH will look for the data in ```${HOME}/DESY_testbeam/rawdata/Calibration/```. If you want to change to the data in ```./data```, please do 

```
    sudo chmod a+x Setup.sh
    source Setup.sh local
```


4. Compile Millepede 

```
    cd millepede
    qmake
    make
```

This will create library files. It has already setup in Plotter.cpp 

5. To make this program interactive in ROOT interactive session, open ROOT session and run 

```
    gROOT->ProcessLine(".L analysis/main.C+")
```
This will compile the code in `main.C`. However, the program will crash if you call `main()`. To work inside this framework, you have to create your own classes or modify the existed classes. This is the only drawback for the version after v1.1. 

## Quick start with Plotter

0. Setting up

```
    source Setup.sh 
```

1. Open the ROOT session

```
    root -l
```

2. Load the program

```
    gROOT->ProcessLine(".L analysis/main.C+")
```

3. Define Plotter and set to run number

```
    Plotter p;
    p.SetFileIn("NUMBER", N_entries, Time_window_flag, Reject_the_sat_hit, Force_create, Simulation, ChOffsetBoard, ChFile, ShiftFile)
```

The NUMBER is the number after RUN. N_entries is the number of events. Time_window_flag is the flag to select time window defined in `Configuration.txt` for each board. Reject_the_sat_hit is the flag to reject a hit if it is saturated. `Force_create` will recreate the ROOT file. By default, the ROOT file will be automatically created and will be read once it is already created. `Simulation` specifies if the sample is from GEANT4 simulation sample. `ChOffsetBoard` is the BoardID in which its ChOffset will be read or recreated depending on `ChFile`. `ShiftFile` is the name of a file for shift parameter stored in the `./AlignedShift` directory. 

The example below is to create a root file with `no time window` and reject the saturated hit for `RUN_8006` (`real data`). The ChOffset will be recomputed (RECREATE flag) for all board ID (value 0 means for all boards). The third last flag is to force creating file. The last argument is optional. It is a shift file in `./AlignedShift` directory. In this case, it reads `./AlignedShift/shift_8006.txt`.

```
    Plotter p;
    p.SetFileIn("8006", 100000, 0, 1, 1, 0, 0, "RECREATE", "shift_8006.txt")
```

## Create macro in ```run.C```

1. To start the progrom, the previous instruction is done and embeded in ```run.C```, please open the file and read


2. Before running the program, please set board number, constants, etc. in ```Configuration.txt``` correctly. 


## Alignment 

1. After compiling the program and add the data. Start the alignment by using 

```
    p.AlignmentStart("shift_9014.txt")
```

This will create ```shift_9014.txt``` in ```./AlignedShift```. 

2. To load the shift you can do 

```
    p.reader.ReadShiftFromFile("./AlignedShift/shift_5031.txt")
```

3. Print the shift parameters 

```
    p.reader.layer_shift
```

The output will be vector correcponding to [layer_type][layer_number]


## Testing and understand the framework


1. Decoder class. Class for decoding STiC data. Example after load main.C

[Inside ROOT session]
```
    Decoder decoder;
    decoder.set_Input(0x7230500000e2f73d)
    decoder.STiC_ID()
```

2. Reader class. The Reader is a universal reader in this framework. 

 * Reader reads a STiC data and convert into .root file which has an entry corresponding to a hit. For example, if `arm_02_phys_08_ip` has 19222 hits, the output ROOT file will have 19222 entries. This class is built for robustness because Data has to be checked quickly when we go to a TestBeam. 

Init. by
```
    Reader reader;
```

If you want to loop over all hits data in `RUN_8006`, do [Note that please create a directory `RUN_8006` inside ./rootfiles/]

```
    reader.CreateHitAllBoard("8006")
```

You may experience `Trigger mismatch found! Run with verbose_trigger = 1`. Try to run

```
    reader.CreateHitRoot(filename, nEvent, verbose_trigger)
    reader.CreateHitRoot("/Users/surapat/EPFLPhD/Hardware_SHiP/SHiP_DAQ_run_TB/data/RUN_8006/arm_05_phys_25_ip", -1, 2)
```

If you run with `verbose_trigger = 1`, the output will tell you where the first trigger mismatch is. To debug further, all you have to do is to execute a program until that event with `verbose_trigger = 2`. `verbose_trigger = 2` will print an actual trigger counter and decoded trigger with other information. 

* The Reader also collect detector configuration from `Configuration.txt`. The reader will read this file once it is decleared. To call a constant

```
    reader.board_frequency
```
will tell you about the board_frequency from the file. The special thing is that the first table in the `Configuration.txt` is encoded using `<unordered_map>` which is in C++ STL. This library is almost equivalent to `python-dictionary`. For example, to call an Offset from BoardID = 2
```
    reader.BoardID_config[2][1]
```
The second index correspond to the column in `Configuration.txt` starting with `0` column `IP`.


## In case of problem

1. Permission problems. In the server, ```git``` comman is aliased as ```sudo git```. Some directories are affected when cloning the repository. Please use the command.  

```
sudo chown -R <GROUPNAME>:<USERNAME> ./ParentsDirectory
sudo chown -R lphe:lphe *
``` 

2. If there is a problem with ```millepede```, please make sure that you have ```make``` the software.  

3. In the case of depression, don't know what to do, contact S. Ek-in. You know where I am. 


## Future extensions


[] Debugging message with logging https://www.scalyr.com/blog/getting-started-quickly-c++-logging


## Conventions

This framework must use only C++ with STL if possible. Extra or fancy libs are not recommended. Your own functions which are useful might be put in `include/UtilFunc.cpp`

[This part below is underconstruction]

Code | Meaning
-----|--------
-----|--------



## Testbeam Oct2018 version

Run 

```
    git checkout -b v1.0 v1.0
```

### Setup the first time
- Create a directory Builds/ and Builds/rootfiles/
- Setup ROOT6 environment: source Setup.sh
- Generate the Makefile using: qmake

### How to compile and execute
- Compile with: make
- Execute in Builds/ using: ./analysis n°run with_time   where n°run is the run number (f.ex. 8000) and with_time applies the time cuts if different than 0.

### How to run this version

```
    .x obj/loader.c(RUN_NUMBER, TIMEFlag)
```



